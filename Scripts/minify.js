const minify = require('minify');
const path = require('path');
const fs = require('fs');
const fse = require("fs-extra");

const minifyOptions = {
    html: {
        removeAttributeQuotes: false,
        removeOptionalTags: false,
    },
};

const targetPath = process.argv[2];
console.info(targetPath);

function CleanupHtml(html)
{
    let result = html.replace("<!doctype html>", '<!DOCTYPE html>');
    result = result.replace(/<th>/g, '<th scope="col">');
    result = result.replace(/<th style=/g, '<th scope="col" style=');
    result = result.replace(/<thead><th/g, '<thead><tr><th');
    result = result.replace(/<\/th><\/thead>/g, '</th></tr></thead>');
    return result;
}

async function Run(targetPath, minifyOptions)
{
    let files  = [];
    
    function IterateDirectory(directory)
    {
        fs.readdirSync(directory).forEach(file => {
            const absolutePath = path.join(directory, file);
            if (fs.statSync(absolutePath).isDirectory())
            {
                return IterateDirectory(absolutePath);
            }
            else return files.push(absolutePath);
        });
    }
    
    IterateDirectory(targetPath);

    for (const filepath of files)
    {
        const extension = filepath.split(".").pop();
        switch (extension)
        {
            case "html":
            case "css":
            case "js":
                try
                {
                    console.info(`minifying ${filepath}...`);
                    let minified = await minify(filepath, minifyOptions);
                    minified = CleanupHtml(minified);
                    fse.writeFile(filepath, minified, "utf8", function (error)
                    {
                        if (error)
                        {
                            console.error(error);
                            return 1;
                        }
                    });
                } catch (error)
                {
                    console.error(error);
                    return 1;
                }
                break;
        }
    }

    return 0;
}

return Run(targetPath, minifyOptions);
