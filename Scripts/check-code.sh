#!/bin/bash

rm -rf ./Implementation/dist/*

pushd Implementation
    echo "Checking Codedoc..."
    codedoc check
    echo "Building Codedoc..."
    codedoc build
popd

pushd Scripts
    echo "Minifying..."
    node minify.js ../Implementation/dist
popd

pushd Implementation
    echo "Checking html..."
    for filename in $(find dist -name '*.html'); do
        echo "Checking $filename..."
        #html-validate "$filename"
        FileSize=$(wc -c < "$filename")
        if [ $FileSize -le 100 ]; then printf "\\033[0;31mFile is smaller than 100 characters.\n\\033[0m"; fi
    done
popd