#!/bin/bash

rm -rf ./Implementation/dist/*
cd Implementation
codedoc build

cd dist
http-server . >/dev/null 2>&1 &
blc http://localhost:8080/ --ordered --recursive "$@"
