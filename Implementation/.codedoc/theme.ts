import { createTheme } from '@codedoc/core/transport';


export const theme = /*#__PURE__*/createTheme({
  light: {
    primary: '#008300'
  },
  dark: {
    primary: '#00adef'
  }
});
