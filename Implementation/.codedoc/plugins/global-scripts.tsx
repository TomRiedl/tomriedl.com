import { StaticRenderer } from '@connectv/sdh';
import register from 'jsdom-global';
import { ConfigOverride } from '@codedoc/core';

const renderer = new StaticRenderer();
register();


export function GlobalScripts()
{
    return function(): ConfigOverride {
        return {
            page:
            {
                scripts: [
                    <script>{`
                    `}</script>
                ]
            }
        }
    };
}