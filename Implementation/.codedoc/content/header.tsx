import { CodedocConfig } from '@codedoc/core';
import { Header as _Header, DarkModeSwitch$, ToCToggle$ } from '@codedoc/core/components';


export function Header(config: CodedocConfig, renderer: any) {
    return (
        <_Header>
            <ToCToggle$ />
            <a href="/" class="title"><div class="logo"></div>Tom Riedl</a>
            <div style="float:right"><DarkModeSwitch$ /></div>
        </_Header>
    )
}
