
import { configuration } from "@codedoc/core";
import { GlobalScripts } from "./plugins/global-scripts";
import { theme } from "./theme";

const fse = require("fs-extra");
const mergeFiles = require('merge-files');

export const config = /*#__PURE__*/configuration({
    theme,

    page: {
        title: {
            base: "Tom Riedl",
            connector: " | "
        },
        meta: {
            subject: 'Tom Riedl',
            description: 'Business, Trading, Technik, Programmieren, Software, Kryptowährungen',
            keywords: ['tom', 'riedl', 'tomriedl', 'binance', 'bitfinex', 'crypto', 'krypto', 'referral', 'affiliate', 'system', 'code', 'partner', 'kryptowährungen', 'apps', 'telegram', 'android', 'ios'],
            themeColor: "#212121",
            appleMobileWebStatusBarStyle: "black-translucent"
        },
        favicon: "/favicon.ico",
        stylesheets: [ ],
        fonts: {
            text: {
                name: "Hind",
                url: "",
                fallback: "sans-serif"
            },
            code: {
                name: "Source Code Pro",
                url: "",
                fallback: `'Courier New', Courier, monospace`
            },
            icon: {
                name: "Material Icons",
                url: "",
                outline: "Material Icons Outlined"
            },
        }
    },

    src: {
        base: 'docs/md',
        toc: '_toc.md',
        not_found: '404.md',
        pick: /\.md$/,
    },

    dest: {
        assets: "dist",
        bundle: "bundle",
        styles: "styles",
        html: "dist"
    },

    plugins: [
        GlobalScripts()
    ],

    afterBuild: [
        async function Minify(): Promise<void>
        {
            const source: string = "docs/assets/styles.css";
            const destination: string = "dist/styles/codedoc-styles.css";
            const merged: string = "dist/styles/styles-merged.css";
            await mergeFiles([destination, source], merged);
            await fse.move(merged, destination, { overwrite: true }, (error: any) => { if (error) { console.error(error); } });
        },
        async function CopyAssets(): Promise<void>
        {
            await fse.copy("docs/assets", "dist/assets", { overwrite: true }, (error: any) => { if (error) { console.error(error); } });
            await fse.copy("docs/fonts", "dist/fonts", { overwrite: true }, (error: any) => { if (error) { console.error(error); } });
            await fse.copy("docs/root", "dist", { overwrite: true }, (error: any) => { if (error) { console.error(error); } });
        },
        async function CopyVersion1Content(): Promise<void>
        {
            await fse.copy("docs/wp-content", "dist/wp-content", { overwrite: true }, (error: any) => { if (error) { console.error(error); } });
        },
        async function FileOperations(): Promise<void>
        {
            const { resolve } = require('path');
            const { readdir } = require('fs').promises;
            async function* getFiles(path: string): any
            {
                const directories = await readdir(path, { withFileTypes: true });
                for (const directory of directories)
                {
                    const res = resolve(path, directory.name);
                    if (directory.isDirectory())
                    {
                        yield* getFiles(res);
                    }
                    else
                    {
                        yield res;
                    }
                }
            }

            async function PostProcessHtml(filepath: string): Promise<void>
            {
                function applySeoAttributes(html: string, tags: Array<string>): string
                {
                    let result: string = html;

                    // Tags
                    const keywordsTag: RegExpMatchArray | null = html.match(/<meta name="keywords" content="(.*?)">/gm);
                    if (tags.length && keywordsTag && keywordsTag.length)
                    {
                        result = result.replace(keywordsTag[0], `<meta name="keywords" content="${tags.join(",")}">`);
                    }

                    const titleCode: RegExpMatchArray | null = result.match(/<title>(.*?)<\/title>/);
                    if (titleCode && titleCode.length)
                    {
                        const title: string = titleCode[1].trim().replace(`${config.page.title.connector}${config.page.title.base}`, "");
                        result = result.replace('<link ', `<meta property="og:title" content="${title}"><link `);
                    }

                    const typeCode: RegExpMatchArray | null = result.match(/<code>##Type##:(.*?)<\/code>/);
                    if (typeCode && typeCode.length)
                    {
                        const type: string = typeCode[1].trim();
                        result = result.replace('<link ', `<meta property="og:type" content="${type}"><link `);
                        result = result.replace(typeCode[0], "");
                    }

                    const localeCode: RegExpMatchArray | null = result.match(/<code>##Locale##:(.*?)<\/code>/);
                    if (localeCode && localeCode.length >= 2)
                    {
                        const locale: string = localeCode[1].trim();
                        result = result.replace(` lang="de"`, ` lang="${locale.substr(0, 2)}"`);
                        result = result.replace(localeCode[0], "");
                        result = result.replace('<link ', `<meta property="og:locale" content="${locale.replace("-", "_")}"><link `);
                    }

                    const descriptionCode: RegExpMatchArray | null = result.match(/<code>##Description##:(.*?)<\/code>/);
                    const descriptionTag: RegExpMatchArray | null = html.match(/<meta name="description" content="(.*?)">/);
                    if (descriptionCode && descriptionCode.length >= 2 && descriptionTag && descriptionTag.length)
                    {
                        const description: string = descriptionCode[1].trim();
                        result = result.replace(descriptionTag[0], `<meta name="description" content="${description}">`);
                        result = result.replace(descriptionCode[0], "");
                        result = result.replace('<link ', `<meta property="og:description" content="${description}"><link `);
                    }

                    result = result.replace('<link ', `<meta property="og:site_name" content="${config.page.title.base}"><link `);

                    return result;
                }

                fse.readFile(filepath, "utf8", async function (error: any, data: any)
                {
                    if (error)
                    {
                        return console.error("Unable to read file in AddLangToHtml.", error);
                    }
                    let result: string = data.replace(/<html/g, '<html lang="de" prefix="og: https://ogp.me/ns#"');
                    result = result.replace(/, user-scalable=0/g, '');
                    result = result.replace(/maximum-scale=1/g, 'maximum-scale=5');
                    result = result.replace(/<a target="_blank">See/g, '<a href="javascript:void(0);" target="_blank" rel="nofollow">See');
                    result = result.replace(/target="_blank"/g, 'target="_blank" rel="noopener"');
                    result = result.replace(/class="header-0-0/g, 'class="header-0-0-x header-0-0');
                    result = result.replace(/class="toc-0-0/g, 'class="toc-0-0-x toc-0-0');
                    result = result.replace(/class="heading-0-0/g, 'class="heading-0-0-x heading-0-0');
                    result = result.replace(/class="anchor-0-0/g, 'class="anchor-0-0-x anchor-0-0');
                    result = result.replace(/class="contentnav-0-0/g, 'class="contentnav-0-0-x contentnav-0-0');

                    result = result.replace(/BINANCE_CODE/g, 'T5UG2173');
                    result = result.replace(/BINANCE_URL/g, 'https://accounts.binance.com/de/register?ref=T5UG2173');
                    result = result.replace(/BINANCE_REF_PAGE_CODE/g, 'PL8RR6H9');
                    result = result.replace(/BINANCE_REF_PAGE_URL/g, 'https://accounts.binance.com/en/register?ref=PL8RR6H9');
                    result = result.replace(/BINANCE_REF_PAGE_50_CODE/g, 'VR588DDW');
                    result = result.replace(/BINANCE_REF_PAGE_50_URL/g, 'https://accounts.binance.com/en/register?ref=VR588DDW');
                    result = result.replace(/BINANCE_EN_URL/g, 'https://accounts.binance.com/en/register?ref=D0Z7YAV8');
                    result = result.replace(/BINANCE_ZH_URL/g, 'https://accounts.binance.com/en/register?ref=UQITV7TT');
                    result = result.replace(/BITFINEX_URL/g, 'https://www.bitfinex.com/?refcode=yiZ39X8ke');
                    result = result.replace(/POLONIEX_URL/g, 'https://poloniex.com/signup?c=P3DCTAWA');
                    result = result.replace(/CRYPTOCOM_APP_URL/g, 'https://crypto.com/app/xbj8ksdk9f');
                    result = result.replace(/CRYPTOCOM_EXCHANGE_URL/g, 'https://crypto.com/exch/xbj8ksdk9f');
                    result = result.replace(/LIQUID_URL/g, 'https://liquid.com/sign-up/?affiliate=ijEWkDQ2102446');
                    result = result.replace(/CELSIUS_URL/g, 'https://celsiusnetwork.app.link/157400ccde');
                    result = result.replace(/NEXO_URL/g, 'https://nexo.io/?u=5f07f47cb95344555c2d7869');
                    result = result.replace(/COINLEND_URL/g, 'https://www.coinlend.org/?referral=425326');
                    result = result.replace(/COINSPOT_URL/g, 'https://www.coinspot.com.au?affiliate=3RQEC');
                    result = result.replace(/OKEX_URL/g, 'https://www.okex.com/join/1/3066896');
                    result = result.replace(/ADVCASH_URL/g, 'https://wallet.advcash.com/referral/47ccccc2-1eb1-4db5-b39c-9666811cc920');
                    result = result.replace(/SERVERPROFIS_URL/g, 'https://service.serverprofis.net/aff.php?aff=846');
                    result = result.replace(/CURVE_URL/g, 'https://www.curve.com/join#D4J982JN');
                    result = result.replace(/TRADINGVIEW_URL/g, 'https://de.tradingview.com/gopro/?share_your_love=tomriedlcom');

                    // Free title tag to read it easily from shell script
                    result = result.replace("<title>", "\n<title>");
                    result = result.replace("</title>", "</title>\n");

                    // Touch Icon
                    result = result.replace('<meta name="apple', `<link rel="apple-touch-icon" href="/apple-touch-icon.png"><meta name="apple`);

                    // Create localization tags
                    let tags: RegExpMatchArray | null = result.match(/<code>##Translation##:(.*?):(.*?)<\/code>/gm);
                    for (const tag of tags || [])
                    {
                        const matches: RegExpMatchArray | null = tag.match(/<code>##Translation##:(.*?):(.*?)<\/code>/);
                        if (matches?.length === 3)
                        {
                            const linkCode: string = `<link rel="alternate" hreflang="${matches[1]}" href="${matches[2]}" />`;
                            result = result.replace('<link', `${linkCode}<link`);
                            result = result.replace(tag, "");
                        }
                    }

                    // Icons
                    tags = result.match(/<code>##Icon##:(.*?)<\/code>/gm);
                    for (const tag of tags || [])
                    {
                        const matches: RegExpMatchArray | null = tag.match(/<code>##Icon##:(.*?)<\/code>/);
                        if (matches?.length === 2)
                        {
                            const icon: string = matches[1].trim();
                            result = result.replace(tag, `\n<!-- Icon:${icon} -->\n`);
                        }
                    }

                    // Include other pages
                    tags = result.match(/<code>##Include##:(.*?)<\/code>/gm);
                    for (const tag of tags || [])
                    {
                        const matches: RegExpMatchArray | null = tag.match(/<code>##Include##:(.*?)<\/code>/);
                        if (matches?.length === 2)
                        {
                            const filename: string = `docs/md/${matches[1].trim()}.html`;
                            // Let the exception bubble to top if the file does not exist
                            fse.ensureFileSync(filename);
                            const content: string = fse.readFileSync(filename, 'utf8');
                            result = result.replace(tag, content);
                        }
                    }


                    // Move style tags to head
                    tags = result.match(/<code>##Style##:(.*?)<\/code>/gm);
                    let styleContent: string = "";
                    for (const tag of tags || [])
                    {
                        const matches: RegExpMatchArray | null = tag.match(/<code>##Style##:(.*?)<\/code>/);
                        if (matches?.length === 2)
                        {
                            styleContent += `${matches[1]}\n`;
                            result = result.replace(tag, "");
                        }
                    }
                    result = result.replace('</style>', `\n${styleContent}</style>`);

                    // Build image/picture tags
                    tags = result.match(/<code>##Picture##:(.*?):(.*?):(.*?):(.*?):(.*?)<\/code>/gm);
                    for (const tag of tags || [])
                    {
                        const matches: RegExpMatchArray | null = tag.match(/<code>##Picture##:(.*?):(.*?):(.*?):(.*?):(.*?)<\/code>/);
                        if (matches?.length === 6)
                        {
                            const width: string = matches[1];
                            const height: string = matches[2];
                            const filename: string = matches[3];
                            const extensions: Array<string> = matches[4].split(",");
                            const altText: string = matches[5];
                            if (extensions.length == 1)
                            {
                                result = width != "0" && height != "0"
                                    ? result.replace(tag, `<img src="${filename}.${extensions[0]}" width="${width}" height="${height}" alt="${altText}" />`)
                                    : result.replace(tag, `<img src="${filename}.${extensions[0]}" alt="${altText}" />`);
                            }
                            else
                            {
                                let pictureTag: string = "<picture>";
                                for (const extension of extensions)
                                {
                                    pictureTag += `<source srcset="${filename}.${extension}" type="image/${extension.replace("jpg", "jpeg")}">`;
                                }
                                pictureTag += width != "0" && height != "0"
                                    ? `<img src="${filename}.${extensions.pop()}" width="${width}" height="${height}" alt="${altText}" /></picture>`
                                    : `<img src="${filename}.${extensions.pop()}" alt="${altText}" /></picture>`;
                                result = result.replace(tag, pictureTag);
                            }
                        }
                    }

                    // Open all external links in new tabs
                    const linkTags: RegExpMatchArray | null = result.match(/<a [^>]*href="[^"]*"[^>]*>/gm);
                    for (const tag of linkTags || [])
                    {
                        const uri: RegExpMatchArray | null = tag.match(/.*href="([^"]*)".*/);
                        if (uri && uri.length >= 2)
                        {
                            const href: string = uri[1];
                            if (href && href[0] != "/" && href[0] != "#" && href.indexOf("https://tomriedl.com") < 0
                                && tag.lastIndexOf("\">") == tag.length - 2)
                            {
                                const search: string = tag;
                                const searchNoEnd: string = tag.substring(0, tag.length - 2);
                                const replace: string = `${searchNoEnd}" rel="nofollow" onclick="window.open(this.href);return false">`;
                                result = result.replace(search, replace);
                            }
                        }
                    }

                    // Remove "link" text from h-tags
                    result = result.replace(/>link<\/span>/g, '></span>');

                    // Replace tags
                    const tagsCode: RegExpMatchArray | null = result.match(/<code>##Tags##:(.*?)<\/code>/gm);
                    let tagList: Array<string> = [];
                    for (const codeBlock of tagsCode || [])
                    {
                        const tags: string = codeBlock.replace("<code>##Tags##:", "").replace("</code>", "").replace(",", " ").trim();
                        tagList = tags.toLowerCase().split(" ");
                        const replacementTags: Array<string> = [];
                        for (const rawTag of tagList)
                        {
                            const tag = rawTag.trim();
                            replacementTags.push(`<a href="/tag/${tag}" class="TagLink">${tag}</a>`);
                        }
                        const replacement: string = `<hr /><br />Tags: \n${replacementTags.join(" &middot; \n")}\n`;
                        result = result.replace(codeBlock, replacement);
                    }

                    result = applySeoAttributes(result, tagList);

                    // Extract first image to add it to og:image
                    const imageUrls: Array<string> | undefined = result.match(/<img [^>]*src="[^"]*"[^>]*>/gm)?.map((x: string) => x.replace(/.*src="([^"]*)".*/, '$1'));
                    const coverImage: string = imageUrls && imageUrls.length ? imageUrls[0] : "/logo-200.png";
                    result = result.replace('<link ', `<meta property="og:image" content="${coverImage}"><link `);

                    // Remove empty tags
                    result = result.replace(/<p><\/p>/g, "");
                    result = result.replace(/<p>\n<\/p>/g, "");
                    result = result.replace(/<p>\n\n<\/p>/g, "");
                    result = result.replace(/<link href="" rel="stylesheet">/g, "");
                    // Remove marker tags
                    result = result.replace(/<marker>/g, "");
                    result = result.replace(/<\/marker>/g, "");

                    fse.writeFile(filepath, result, "utf8", function (error: any)
                    {
                        if (error) return console.error(error);
                    });
                });
            }

            for await (const filepath of getFiles("docs/md"))
            {
                const extension: string = filepath.split(".").pop();
                
                switch (extension)
                {
                    case "webp":
                    case "png":
                    case "jpg":
                    case "svg":
                    case "pdf":
                    case "json":
                        await fse.copy(filepath, filepath.replace("/docs/md/", "/dist/"), { overwrite: true }, (error: any) => { if (error) { console.error(error); } });
                        break;
                }
            }

            for await (const filepath of getFiles("dist"))
            {
                const extension: string = filepath.split(".").pop();
                
                switch (extension)
                {
                    case "html":
                        await PostProcessHtml(filepath);
                        break;
                }
            }
        }
    ],
    
});
