 #!/bin/bash

Path=$1
if [ "$Path" == "" ]; then echo "Please specify path."; exit 1; fi

echo "Converting files in /wd/$Path..."
cd /wd/$Path
pwd

for inputFile in $(find /wd/$Path -name '*.webp'); do
    echo "Converting $inputFile..."
    outputFile=${inputFile/.webp/.png}
    convert "$inputFile" -strip "$outputFile"
    rm "$inputFile"
done

