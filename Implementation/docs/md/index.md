# Tom Riedl - Startseite

`##Description##: Beiträge und Anleitungen zu Kryptowährungen, Trading, Technik, Programmieren und anderen Themen`
`##Type##: website`
`##Locale##: de-DE`

<img src="/logo-200.png" style="float:left;margin-right:1em;width:100px;height:100px;" alt="Tom Riedl Logo klein" />

Willkommen bei tomriedl.com - Beiträge und Anleitungen zu Kryptowährungen, Trading, Technik, Programmieren und anderen Themen.

[Über Tom Riedl](/uber/) - [Tags](/tag/) - [Changelog](/changelog/) - [Impressum](/impressum/)

<br style="clear:both;" />

<div class="categoryContainer">
<a href="/tag/binance" class="item binance"><img src="images/binance.svg" alt="Binance" /><span class="categoryTitle">Binance</span></a>
<a href="/tag/krypto" class="item crypto"><img src="images/crypto.svg" alt="Kryptowährungen" /><span class="categoryTitle">Kryptowährungen</span></a>
<a href="/tag/sicherheit" class="item security"><img src="images/security.svg" alt="Sicherheit" /><span class="categoryTitle">Sicherheit</span></a>
<a href="/tag/telegram" class="item telegram"><img src="images/telegram.svg" alt="Telegram" /><span class="categoryTitle">Telegram</span></a>
<a href="/tag/iota" class="item iota"><img src="images/iota.svg" alt="IOTA" /><span class="categoryTitle">IOTA</span></a>
<a href="/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/" class="item definitions"><img src="images/knowledge.svg" alt="Definitionen Abkürzungen" /><span class="categoryTitle">Definitionen</span></a>
</div>

## Neueste Beiträge
- `2024-01-07` [Telegram Premium - Funktionen, Vorteile und Bestellung in Deutschland](/telegram/premium/)
- `2023-01-22` [Profit Scout Vorstellung – unglaublich präzises Werkzeug für Trades](/krypto/profit-scout/)
- `2021-08-08` [BIMI, DMARC, DKIM und SPF für E-Mails - Anleitung zum Einrichten und Testen](/email/bimi-dmarc-dkim-spf/)
- `2021-07-11` [Alternative Einzahlmöglichkeiten für Binance in Euro](/krypto/binance/alternative-einzahlmoeglichkeiten/)
- `2021-06-27` [Binance Referral-ID BINANCE_REF_PAGE_CODE - Vorteile und Bonus](/krypto/binance/referral-id/)
- `2021-04-12` [Aktien auf der Binance-Plattform handeln](/krypto/binance/aktien-auf-binance-handeln/)
- `2021-04-11` [Strategie für neue Coins/Tokens auf Binance](/krypto/binance/strategie-fuer-neue-coins-tokens-auf-binance/)
- `2021-02-21` [Binance Account registrieren - Konto erstellen](/krypto/binance/account-erstellen/)
- `2021-02-06` [Binance-Guthaben über Bankkonto aufladen (SEPA-Überweisung)](/krypto/binance/guthaben-ueber-konto-aufladen/)
- `2021-01-30` [Binance BNB Vault - BNB Staking](/krypto/binance/binance-bnb-vault-bnb-staking/)
- `2021-01-24` [Stablecoins tauschen: Spot-Trading BUSD/USDT in der Binance-App](/krypto/binance/binance-stablecoins-tauschen-spot-trading-usdt-busd/)
- `2021-01-17` [Binance Krypto-Kreditkarte - Anleitung, Vorstellung, Test und Erfahrungen](/krypto/binance/binance-kreditkarte-anleitung-vorstellung-und-test/)
- `2021-01-10` [Auszahlen/Überweisen von Binance auf eine andere Plattform oder Wallet (Withdraw)](/krypto/binance/auszahlen-ueberweisen-von-binance-auf-andere-plattform-oder-wallet-withdraw/)

## Sicherheit, E-Mails
- [BIMI, DMARC, DKIM und SPF für E-Mails - Anleitung zum Einrichten und Testen](/email/bimi-dmarc-dkim-spf/)
- [Two-Factor Authentication / Multi-Factor Authentication (TFA, 2FA, MFA, TOTP)](/version1/two-factor-authentication-multi-factor-authentication-tfa-2fa-mfa-totp/)
- [Spamreduzierung durch Catch-All E-Mails für alle Mitarbeiter](/version1/spamreduzierung-durch-catch-all-e-mails-fur-alle-mitarbeiter/)

## Kryptowährungen
Anleitungen und Informationen zu Kryptowährungen, Wallets und Krypto-Börsen.

### Allgemein
- [Risiken beim Spekulieren mit Kryptowährungen](/version1/risiken-beim-spekulieren-mit-kryptowaehrungen/)
- [Abkürzungen und Begriffe der Kryptowelt](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/)
- [Bitcoin Expresskauf mit Fidor und Bitcoin.de](/version1/bitcoin-expresskauf-mit-fidor-und-bitcoin-de)

### Binance
- [Binance Account registrieren - Konto erstellen](/krypto/binance/account-erstellen/)
- [Binance Referral-ID BINANCE_REF_PAGE_CODE - Vorteile und Bonus](/krypto/binance/referral-id/)
- [Binance-Guthaben über Bankkonto aufladen (SEPA-Überweisung)](/krypto/binance/guthaben-ueber-konto-aufladen/)
- [Alternative Einzahlmöglichkeiten für Binance in Euro](/krypto/binance/alternative-einzahlmoeglichkeiten/)
- [Binance BNB Vault - BNB Staking](/krypto/binance/binance-bnb-vault-bnb-staking/)
- [Binance Krypto-Kreditkarte - Anleitung, Vorstellung, Test und Erfahrungen](/krypto/binance/binance-kreditkarte-anleitung-vorstellung-und-test/)
- [Binance Referral Code – Affiliate Link, wo finde ich ihn?](/version1/binance-referral-code-affiliate-link-wo-finde-ich-ihn/)
- [Strategie für neue Coins/Tokens auf Binance](/krypto/binance/strategie-fuer-neue-coins-tokens-auf-binance/)
- [Stablecoins tauschen: Spot-Trading BUSD/USDT in der Binance-App](/krypto/binance/binance-stablecoins-tauschen-spot-trading-usdt-busd/)
- [Auszahlen/Überweisen von Binance auf eine andere Plattform oder Wallet (Withdraw)](/krypto/binance/auszahlen-ueberweisen-von-binance-auf-andere-plattform-oder-wallet-withdraw/)
- [Aktien auf der Binance-Plattform handeln](/krypto/binance/aktien-auf-binance-handeln/)
- [Zwischen Binance Lite und Binance Professional wechseln](/krypto/binance/zwischen-binance-lite-binance-professional-wechseln/)

### Bitfinex / EOSfinex
- [Anleitung für neues Bitfinex Affiliate/Referral-System](/version1/anleitung-fuer-neues-bitfinex-affiliate-referral-system/)
- [Bitfinex Invitation Code (Einladungscode)](/version1/bitfinex-invitation-code-einladungscode/)
- [EOSfinex: Neue dezentrale Börse von Bitfinex](/version1/eosfinex-neue-dezentrale-boerse-von-bitfinex/)

### Liquid / Quoinex
- [Liquid Earn - Lending Plattform](/krypto/liquid/liquid-earn-lending-plattform/)

### Lending
- [Coinlend-App](/version1/coinlend-app/)
- [Kryptowährungen gegen Zinsen verleihen – Coinlend Anleitung/Tutorial](/version1/kryptowaehrungen-gegen-zinsen-verleihen-coinlend-anleitungtutorial/)


### ICOs
- [ICO-Teilnahme am Beispiel von DragonChain](/version1/ico-teilnahme-am-beispiel-von-dragonchain/)

### IOTA / MIOTA
- [IOTA Trading auf Bitfinex](/version1/iota-trading-auf-bitfinex/)
- [IOTA Wallet „No Connection / Verbindung verweigert / Keine Verbindung“](/version1/iota-wallet-no-connection/)
- [IOTA Snapshots](/version1/iota-snapshots/)
- [IOTA – Erste Hilfe, Fragen und Antworten (FAQ)](/version1/iota-erste-hilfe-fragen-und-antworten-faq/)
- [IOTA Seed erzeugen / IOTA-Seed-Generator](/version1/iota-seed-generator/)

## Telegram
- [Telegram Premium](/telegram/premium/)
- [Telegram Tipps und Tricks](/version1/telegram-tipps-und-tricks/)
- [Telegram-Account mit zweistufiger Bestätigung absichern (Two-Step-Verification)](/version1/telegram-account-mit-zweistufiger-bestaetigung-absichern-two-step-verification/)

## Websites und Homepages
- [Neue Website - Warum und wie](/website-homepage/neue-website-warum-und-wie/)