# Über Tom Riedl

`##Description##: Informationen über Tom Riedl`
`##Type##: article`
`##Locale##: de-DE`

![Tom Riedl](tom-riedl.png)

🏠 Rosenheim<br />
🏢 [CyanCor GmbH](https://cyancor.com/)<br />
🤝 [Xing](https://www.xing.com/profile/Thomas_Riedl27), [LinkedIn](https://www.linkedin.com/in/tomriedl/)<br />
💻 [GitLab](https://gitlab.com/TomRiedl), [GitHub](https://github.com/TomRiedl), [stackshare](https://stackshare.io/TomRiedl)
<div style="clear:both;"></div>

[Impressum](/impressum/)<br />
[Datenschutzerklärung](/datenschutzerklaerung/)