# Aktien auf der Binance-Plattform handeln

`##Description##: Anleitung zum Handeln von Aktien auf Binance`
`##Type##: article`
`##Locale##: de-DE`

`##Picture##:1080:552:cover-aktien-auf-binance-handeln:webp,png:Strategie für neue Coins/Tokens auf Binance`

<h3>Der Aktienhandel auf Binance wird aufgrund gesetzlicher Bestimmungen 2021-10-14 beendet.</h3>

Seit 2021-04-12 können auf [Binance](BINANCE_URL) Aktien-Token gehandelt werden. Die erste zu handelnde Aktie ist Tesla Inc. (TSLA) zum Handelspaar [BUSD](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#busd). Das besondere daran ist, dass nicht eine vollständige Aktie erworben werden muss, sondern auch ein Bruchteil einer Aktie erworben werden kann.

Den Token zugrunde liegenden Aktien sind vollständig durch die deutsche CM-Equity AG gesichert und können kommissionsfrei gehandelt werden. Momentan ist der Handel nur über die [Binance-Website](BINANCE_URL) möglich, in der App funktioniert es noch nicht (Stand 2021-04-12).

Solltest du noch keinen Binance-Account haben, [kannst du dich hier registrieren](BINANCE_URL) - Bei der Registrierung über diesen Link erhältst du Zugang zur VIP-Gruppe mit zusätzlichem Support. [Mehr Infos hier](/support/vip-gruppe/).

- [Voraussetzungen](#voraussetzungen)
- [Anleitung](#anleitung)
- [Verkaufen](#verkaufen)

## Voraussetzungen
1. [Binance-Account](BINANCE_URL)
2. Abgeschlossenes Level 3 [KYC](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#kyc) für deutsche Bürger:innen, KYC Level 2 für alle anderen.

## Anleitung
1. Auf der [Binance-Website einloggen](BINANCE_URL)
2. Über den Menüpunkt **Traden** und **Aktien-Token** den Handelsbereich öffnen.<br />`##Picture##:513:422:menue-aktien-token:webp:Über das Menü den Aktien handel öffnen`
3. Die Servicevereinbarung lesen, verstehen und nur wenn es für dich in Ordnung ist akzeptieren.<br />`##Picture##:464:516:servicevereinbarung:webp:Servicevereinbarung des Aktien-Handels`
4. Den gewünschten Aktien-Token wählen und mit einem Klick auf **Trade** fortfahren.<br />`##Picture##:766:345:aktien-token-waehlen:webp:Aktien-Token wählen`
5. Gewünschte Anzahl der Aktien-Token eingeben und auf Kaufen klicken. Bruchteile einer Aktie, z.B. `0.5` können eingegeben werden. <br />`##Picture##:810:619:tesla-aktien-token-anzahl:webp:Anzahl Tesla Aktien Token`
6. Order überprüfen und bestätigen.<br />`##Picture##:355:255:bestaetigungsdialog:webp:Bestätigungsdialog der Order`
7. Order wird ausgeführt.<br />`##Picture##:304:87:order-ausgefuehrt:webp:Order ausgeführt`
8. Links unten unter **Assets** bzw. **Historie** kannst du die abgeschlossene Order überprüfen.`##Picture##:516:128:order-status-assets:webp:Order Assets`

## Verkaufen
Der Verkauf funktioniert genauso wie [oben beschrieben](#anleitung), nur musst du hier auf **Verkaufen** klicken.<br />`##Picture##:273:148:aktien-verkaufen:webp:Aktien verkaufen`


[➡️ Weitere Artikel zu Binance](/tag/binance/)

`##Include##: _crypto-disclaimer-de`

`##Tags##: binance aktien krypto`
