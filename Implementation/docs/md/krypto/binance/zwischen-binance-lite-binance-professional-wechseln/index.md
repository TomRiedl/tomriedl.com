# Zwischen Binance Lite und Binance Professional wechseln

`##Description##: Tutorial zum Wechseln zwischen Binance Lite und Binance Professional`
`##Type##: article`
`##Locale##: de-DE`

![Zwischen Binance Lite und Binance Professional wechseln](binance-professional.png)

Die [Binance](BINANCE_URL)-App bietet zwei verschiedene Modi, den Lite-Modus und den Professional-Modus.<br />
Der Lite-Modus bietet nur eine eingeschränkte Funktionalität. Ich empfehle, gleich zu Beginn den Professional-Modus zu aktivieren, damit man die volle Funktionalität von Binance nutzen kann.

Solltest du noch keinen Binance-Account haben, [kannst du dich hier registrieren](BINANCE_URL) - Bei der Registrierung über diesen Link erhältst du Zugang zur VIP-Gruppe mit zusätzlichem Support. [Mehr Infos hier](/support/vip-gruppe/).

## Anleitung

1. Klicke auf das Account-Menü-Symbol (zeigt eine Person) links oben.<br />![Binance Profil-Menü](binance-profil-menue-oeffnen.png)
2. Deaktiviere Binance Lite<br />![Binance Lite deaktivieren](binance-lite-modus-deaktivieren.png)
3. Die App wechselt nun in den Professional-Modus und du kannst alle Funktionen nutzen.

[➡️ Weitere Artikel zu Binance](/tag/binance/)

`##Include##: _crypto-disclaimer-de`

`##Tags##: binance lite professional krypto`
