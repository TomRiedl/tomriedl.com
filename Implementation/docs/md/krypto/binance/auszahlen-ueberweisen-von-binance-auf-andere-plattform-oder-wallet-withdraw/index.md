# Auszahlen/Überweisen von Binance auf eine andere Plattform oder Wallet (Withdraw)

`##Description##: Anleitung zur fehlerfreien Überweisung bzw. Auszahlung von Binance`
`##Type##: article`
`##Locale##: de-DE`

![Auszahlen Überweisen von Binance auf eine andere Plattform oder Wallet (Withdraw)](binance-withdraw-cover.png)

Das Auszahlen von [Binance](BINANCE_URL) ist nicht schwierig, jedoch muss man genau aufpassen, dass man die Adresse und auch die richtige Blockchain (Netzwerk) angibt.

Das folgende Tutorial zeigt dir Schritt für Schritt, wie du von [Binance](BINANCE_URL) deine Kryptowährungen auf eine andere Plattform oder auf deine Wallet transferierst.

Solltest du noch keinen Binance-Account haben, [kannst du dich hier registrieren](BINANCE_URL) - Bei der Registrierung über diesen Link erhältst du Zugang zur VIP-Gruppe mit zusätzlichem Support. [Mehr Infos hier](/support/vip-gruppe/).

- [Anleitung für App](#anleitung-für-app)
- [Anleitung für Browser](#anleitung-für-browser)

## Anleitung für App
1. [Binance](BINANCE_URL)-App starten und ggf. einloggen.
2. Sollte in der App "Binance Lite" (der einfache Modus) aktiviert sein, deaktiviere diesen zuvor. [Zur Anleitung](/krypto/binance/zwischen-binance-lite-binance-professional-wechseln/)
3. Wähle in der unteren Tableiste den Punkt **Wallets**.<br />![Binance Wallet auswählen](binance-wallet-auswaehlen.png)
4. Klicke oben auf **Auszahlungen** (im Englischen Withdraw).<br />![Auszahlung in Binance](binance-auszahlung-auswaehlen.png)
5. Wähle in der Liste die Währung aus, die du auszahlen möchtest (im Beispiel hier wähle ich USDT).<br />![Kryptoasset aus Liste auswählen in der Binance-App auswählen](kryptoasset-auswaehlen-usdt-binance.png)
6. Nun müssen folgende drei Informationen angegeben werden (kann bei anderen Währungen leicht variieren, ist aber meist sehr ähnlich)
   - **Adresse**: Die Adresse ist das Zieladresse, auf die die Coins/Tokens transferiert werden sollen. Diese erhältst du in der jeweiligen Wallet bzw. auf der anderen Plattform unter Einzahlung/Deposit. Am besten kopierst du die Adresse und schreibst sie nicht ab. Ein anderes Zeichen reicht aus, um die Kryptos komplett zu verlieren, passe also hier ganz genau auf.
   - **Netzwerk**: Manche Kryptowährungen sind in verschiedenen Blockchains vorhanden, [Binance](BINANCE_URL) unterstützt viele dieser Netzwerke. Es ist sehr wichtig, dass du hier das richtige Netzwerk auswählst, sonst sind die Kryptowährungen verloren. Um welches Netzwerk es sich handelt, wird dir bei der Zielplattform angezeigt, bzw. solltest du wissen, wenn du es auf deine eigene Wallet transferierst. [ERC20](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#erc20) ist das Ethereum-Netzwerk, [BEP2](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#bep2) das [Binance](BINANCE_URL)-Netzwerk, [TRC20](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#trc20) das Tron-Netzwerk; es gibt allerdings noch viele mehr.
   - **Betrag**: Die Zahl der Coins/Tokens, die du transferieren möchtest. Beachte, dass beim Auszahlen Gebühren entstehen, diese werden dir aber genau angezeigt.
   <br />![Alle benötigten Daten für die Auszahlung](daten-auszahlung-binance-app.png)
7. Klicke unten auf **Auszahlung**. Solltest du den Button nicht sehen ist dieser womöglich von der Tastatur verdeckt, schließe dann vorher die Tastatur.<br />![Button Auszahlung](button-auszahlung.png)
8. Prüfe nun in der Zusammenfassung, ob alle Daten richtig sind. Wenn ja, starte die Transaktion mit einem Klick auf **Bestätigen**<br />![Binance-Order bestätigen](binance-order-bestaetigen.png)
9. Die Transaktion wird in absehbarer Zeit von [Binance](BINANCE_URL) gestartet. Überprüfen kannst du den Status, wenn du auf die Wallet-Seite wechselst (siehe Schritt 3) und auf das Protokoll-Symbol klickst.<br />![Protokoll der Binance-Transaktionen](protokoll-transaktionen.png)<br />![Eintrag Transaktion in Binance-Protokoll](binance-protokoll-transaktion-eintrag-usdt.png)
10. Je nach Netzwerk kann es Sekunden bis mehrere Stunden dauern, bis die Transaktion abgeschlossen ist. Die hier gezeigte Transaktion war 35 Minuten später bei [Celsius](CELSIUS_URL) (USDT über ERC20).<br />![Eingehende Transaktion bei Celsius](transaktion-zu-celsius.png)

## Anleitung für Browser
1. Bei [Binance](BINANCE_URL) einloggen.
2. Wähle im Menü rechts oben den Eintrag **Wallet** und anschließend **Übersicht** oder **Spot**.<br />![Binance-Web Menü](binance-web-wallet-uebersicht.png)
3. Klicke rechts oben auf **Auszahlung**.<br />![Button Auszahlung in Binance Browser](auszahlung-button-binance-browser.png)
4. Wechsle auf **Krypto**.<br />![Krypto auswählen](krypto-auswaehlen.png)
5. Wähle unter **Coin** deine gewünschte Kryptowährung.<br />![Coin oder Token auswählen](binance-web-coin-token-selektieren.png)
6. Fülle alle benötigten Informationen, primär sind das:
   - **Adresse**: Die Adresse ist das Zieladresse, auf die die Coins/Tokens transferiert werden sollen. Kopiere die Adresse und schreibe sie nicht ab, da beim Abschreiben Fehler entstehen können. Ist die Adresse falsch, gehen womöglich deine Coins/Tokens verloren.
   - **Netzwerk**: Wähle hier das korrekte Netzwerk. Welches Netzwerk das richtige ist, erfährst du auf der Plattform unter Einzahlung/Transaktion/Deposit oder du weißt es, wenn du eine eigenen Wallet hast.
   - **Betrag**: Die Zahl der Coins/Tokens, die du transferieren möchtest. Beachte, dass beim Auszahlen Gebühren entstehen, diese werden dir aber genau angezeigt.
   <br />![Daten der Auszahlung im Binance-Portal](transaktionsdaten-fuer-ueberweisung.png)
7. Klicke auf **Einreichen**<br />![Transaktion im Binance-Portal einreichen](transaktion-einreichen-binance-portal.png)
8. Bestätige die Sicherheitsprüfung mit den geforderten Codes und klicke auf **Senden**. [Mehr Informationen zur Two-Factor-Authentication](/version1/two-factor-authentication-multi-factor-authentication-tfa-2fa-mfa-totp/)<br />![Binance Sicherheitsprüfung beim Auszahlen](binance-sicherheitspruefung-withdraw.png)
9. Die Transaktion wird anschließend von [Binance](BINANCE_URL) gestartet.
10. Um den Status der Transaktion zu sehen, klicke links auf **Übersicht** und anschließend auf **Transaktionshistorie** rechts.<br />![Navigationshistorie über Binance-Menü öffnen](binance-portal-navigation-zu-transaktionshistorie.png)
11. Wähle unter **Transfer** "Auszahlung" aus. Nun siehst du den aktuellen Stand der ausgeführten Transaktion.<br />![Erfolgreiche Transaktion](erfolgreiche-transaktion.png)

[➡️ Weitere Artikel zu Binance](/tag/binance/)

`##Include##: _crypto-disclaimer-de`

`##Tags##: binance transaktion wallet deposit withdraw einzahlung auszahlung krypto`
