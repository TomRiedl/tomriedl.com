# Binance Krypto-Kreditkarte - Anleitung, Vorstellung, Test und Erfahrungen

`##Description##: Testbericht und Erfahrungen, sowie eine Anleitung zur Bestellung der Binance Kreditkarte`
`##Type##: article`
`##Locale##: de-DE`

`##Picture##:1024:497:cover-kreditkarte-binance:jpg:Binance Krypto-Kreditkarte - Anleitung, Vorstellung, Test und Erfahrungen`

- [Vorstellung](#vorstellung)
  - [Allgemeine Informationen](#allgemeine-informationen)
  - [Gebühren und Kosten der Kreditkarte](#gebühren-und-kosten-der-kreditkarte)
  - [Cashback](#cashback)
- [Kreditkarte bestellen - Anleitung für den Browser](#kreditkarte-bestellen---anleitung-für-den-browser)
- [Kreditkarte bestellen - Anleitung für die App](#kreditkarte-bestellen---anleitung-für-die-app)
- [Kreditkarte aktivieren](#kreditkarte-aktivieren)
- [PIN der Kreditkarte abrufen](#pin-der-kreditkarte-abrufen)
- [Test der Kreditkarte](#test-der-kreditkarte)
  - [Erster Test: Zahlung mit mehreren Unterkonten](#erster-test-zahlung-mit-mehreren-unterkonten)
  - [Zweiter Test: Weitere Terminals](#zweiter-test-weitere-terminals)
  - [Dritter Test: Online-Zahlung](#dritter-test-online-zahlung)
  - [Vierter Test: Rückerstattungen](#vierter-test-rückerstattungen)
- [Fragen und Antworten](#fragen-und-antworten)
  - [Wie lange dauert es, bis die Kreditkarte ankommt?](#wie-lange-dauert-es-bis-die-kreditkarte-ankommt)
  - [Muss ich steuerlich etwas beachten?](#muss-ich-steuerlich-etwas-beachten)
  - [Kann ich aus Versehen mit Kryptowährungen bezahlen, die ich nicht für die Karte vorgesehen habe?](#kann-ich-aus-versehen-mit-kryptowährungen-bezahlen-die-ich-nicht-für-die-karte-vorgesehen-habe)
  - [Kann ich die Priorität der Kryptowährungen bestimmen?](#kann-ich-die-priorität-der-kryptowährungen-bestimmen)
  - [Kann ich die PIN der Kreditkarte ändern?](#kann-ich-die-pin-der-kreditkarte-ändern)
  - [Funktionieren Rückerstattungen?](#funktionieren-rückerstattungen)
- [Weitere Ressourcen](#weitere-ressourcen)

## Vorstellung

### Allgemeine Informationen

Die Kreditkarte (eine VISA-Debitkarte) von [Binance](BINANCE_URL) erlaubt es dir, Zahlungen über dein Guthaben (Euro und Kryptowährungen) bei Binance durchzuführen. Mit jeder durchgeführten Zahlung erhältst du Cashback in Form von Binance-Coins (BNB) zurück.

Die Kreditkarte verfügt über eine eigene Wallet, so kannst du selbst bestimmen, welche Währungen und wieviele für die Zahlungen zur Verfügung stehen sollen. Die Reihenfolge bestimmt dabei, welche Währung zuerst verwendet werden soll (die Reihenfolge kannst du frei bestimmen).<br />
`##Picture##:706:825:binance-kreditkarte-wallet:webp:Wallet Binance Kreditkarte`

Solltest du noch keinen Binance-Account haben, [kannst du dich hier registrieren](BINANCE_URL) - Bei der Registrierung über diesen Link erhältst du Zugang zur VIP-Gruppe mit zusätzlichem Support. [Mehr Infos hier](/support/vip-gruppe/).

### Gebühren und Kosten der Kreditkarte
Die Bestellung der Kreditkarte ist kostenlos. Zahlungen in Euro sind kostenfrei (Stand 2021-04-11).

### Cashback
Das Cashback, eine Art Geld-zurück-System belohnt dich für die Benutzung der Kreditkarte mit einer Rückzahlung in Form von Binance Coins (BNB).

Die folgende Tabelle zeigt die Rückvergütung anhand deines BNB-Guthabens (berücksichtigt wird der Durchschnitt der letzten 30 Tage). Die BNB müssen nicht im Kreditkartenkonto sein, sondern können in der Spot-Wallet oder auch im Savings-Account liegen.

| Guthaben<br />(größer oder gleich) | Belohnung bei Käufen | Max. Cashback im Wert von<br />(Stand 2021-04-23) |
| -----------: | ---: | ----: |
|   0 BNB |   0.1% |  1.80 € |
|   1 BNB |   2.0% |  36.00 € |
|  10 BNB |   3.0% |  54.00 € |
|  40 BNB |   4.0% |  72.00 € |
| 100 BNB |   5.0% |  90.00 € |
| 250 BNB |   6.0% | 108.00 € |
| 600 BNB |   8.0% | 144.00 € |

Die Rückvergütung erfolgt automatisch einige Tage später auf das BNB-Konto deiner Karte.

Der maximale berücksichtigte Betrag für die Rückvergütungen liegt bei 1,800 € pro Monat, alles darüber hinaus wird nicht beachtet.

`##Picture##:634:141:cashback-zusammenfassung:webp:Cashback Zusammenfassung`

Derzeit bekannte Einschränkungen (Stand 2021-04-11):
- Amazon Marketplace wird nicht beachtet (Amazon selbst dagegen schon)


## Kreditkarte bestellen - Anleitung für den Browser
1. Account bei [Binance](BINANCE_URL) registrieren und anschließend einloggen.
2. Navigiere über das Menü rechts oben auf **Wallet > Karte**.<br >`##Picture##:457:492:binance-browser-menue-kreditkarte-wallet:webp:Menü Wallet Kreditkarte`
3. Finde den hervorgehobenen Button **Loslegen** und klicke darauf.<br />`##Picture##:605:258:loslegen-kreditkarte-bestellen:webp:Bestellung Binance Kreditkarte starten]`
4. Hast du deine Identität bereits bestätigt, fülle das Formular aus und bestelle die Kreditkarte. Solltest du die Identitätsprüfung noch nicht durchgeführt haben, musst du das zuerst abschließen.
5. Fertig, Kreditkarte ist bestellt und sollte demnächst per Post geliefert werden (bei mir hat es von Bestellung bis Lieferung 15 Tage gedauert).

## Kreditkarte bestellen - Anleitung für die App
1. Account bei [Binance](BINANCE_URL) registrieren und anschließend App herunterladen und darin einloggen.
2. Stelle sicher, dass du die [App auf Professional umgestellt hast](/krypto/binance/zwischen-binance-lite-binance-professional-wechseln/).
3. Klicke auf der Startseite auf **Mehr**.<br />`##Picture##:701:435:binance-app-startseite-mehr-1:webp:Binance App Startseite Mehr Funktionen Ansicht 1` `##Picture##:708:369:binance-app-startseite-mehr-2:webp:Binance App Startseite Mehr Funktionen Ansicht 2`
4. Klicke unter **Finanzen** auf **Karte**.<br />`##Picture##:703:623:binance-app-finanzen-karte:webp:Karte über App bestellen`
3. Mit einem Klick auf den Button **Loslegen** startest du den Bestellprozess.<br />`##Picture##:605:258:loslegen-kreditkarte-bestellen:webp:Bestellung Binance Kreditkarte starten`
4. Fülle das nachfolgende Formular aus und bestätige die Bestellung.
5. Nach ein paar Tagen sollte die Kreditkarte zu dir nach Hause geliefert werden (bei mir hat es 15 Tage gedauert).<br />`##Picture##:905:812:binance-kreditkarte-brief:jpg:Karte geliefert`

## Kreditkarte aktivieren
Sobald du die Kreditkarte erhalten hast, musst du diese aktivieren.

1. Bei [Binance](BINANCE_URL) einloggen oder die App öffnen.
2. Zur Karten-Wallet navigieren. Siehe oben Schritt 2 für Browser oder die Schritte 3-4 für die App.
3. Scrolle etwas nach unten, dort findest du den Banner zum Aktivieren.<br />`##Picture##:675:332:physische-binance-karte-aktivieren:webp:Physische Binance-Karte aktivieren`
4. Trage den dreistelligen CVV-Code deiner Kreditkarte ein. Diesen findest du auf der Rückseite beim Unterschriftenfeld.<br />`##Picture##:665:1000:kreditkarte-binance-cvv-code:webp:CVV Code Eingabe Freischaltung`
5. Die Karte ist aktiviert und kann ab sofort verwendet werden.<br />`##Picture##:641:529:kreditkarte-aktiviert:webp:Kreditkarte erfolgreich aktiviert`
6. Hattest du zuvor eine virtuelle Kreditkarte, transferiere das Guthaben von der virtuellen auf die physische Karte, da die virtuelle Kreditkarte deaktiviert wird.
7. Um nun mit der Karte zahlen zu können, musst du noch den [PIN der Kreditkarte abrufen](#pin-der-kreditkarte-abrufen).


## PIN der Kreditkarte abrufen
Die PIN der Kreditkarte kannst du jederzeit in der Karten-Wallet anzeigen lassen.

1. Bei [Binance](BINANCE_URL) einloggen oder die App öffnen.
2. Navigiere zur Karten-Wallet (siehe oben).
3. Über der Transaktionshistorie befindet sich der Button **PIN anzeigen**.<br />`##Picture##:693:223:kreditkarten-pin-anzeigen-binance-debitkarte:webp:PIN Debitkarte anzeigen`
4. Führe die Sicherheitsabfrage durch (hier wird ein sechsstelliger Code in einer SMS gesendet).<br />`##Picture##:685:431:sicherheitspruefung-pin-abfrage:webp:Sicherheitsprüfung PIN-Abfrage`
5. Die PIN wird angezeigt. Lass dich von dem Text "Deine PIN läuft ab in" nicht irritieren, hier handelt es sich um einen Übersetzungsfehler. Die PIN läuft nicht ab, sondern der Dialog schließt sich automatisch nach 10 Sekunden.<br />`##Picture##:671:495:anzeige-pin-binance-karte:webp:Anzeige PIN der Binance Kreditkarte`


## Test der Kreditkarte
Nachdem ich die Karte erhalten habe, habe ich sie natürlich gleich getestet. Kurzzusammenfassung aller Tests: Hat alles wie erwartet funktioniert.

### Erster Test: Zahlung mit mehreren Unterkonten
Für den ersten Test habe ich die Kreditkarte mit einem Euro und 50 BUSD aufgefüllt, um zu Testen, wie gut eine Zahlung über mehrere Unterkonten funktioniert.<br />`##Picture##:703:597:binance-wallet-debitkarte-euro-busd:webp:Debitkarte Guthaben in Euro und BUSD`

Anschließend kaufte ich ein paar Dinge bei REWE. Das Bezahlen über NFC (kontaktloses Zahlen) hat hier nicht funktioniert, jedoch über das Einstecken der Karte. Bei Lidl und Aldi konnte ich über NFC bezahlen ([siehe zweiter Test](#zweiter-test-weiteren-terminals)).<br />`##Picture##:616:299:beleg-zahlung-rewe-kreditkarte:webp:Kaufbeleg REWE Zahlung Kreditkarte`

Kurz nach der Zahlung wurde eine Notification angezeigt und die Zahlung war anschließend in der Binance-App ersichtlich.<br />`##Picture##:697:207:binance-notification:webp:Notification der Binance-App`<br />`##Picture##:707:790:zahlungsdetails-binance-transaktion:webp:Zahlung bei REWE in der App`

### Zweiter Test: Weitere Terminals
Nach dem ersten Test habe ich weitere Zahlungen durchgeführt, um zu sehen, ob die Karte überall akzeptiert wird.

| Verkaufsstelle | Erfolgreich | Zahlung über NFC |
| :--- | ---: | ---: |
| REWE | ja | ja |
| Lidl | ja | ja |
| Aldi | ja | ja |
| Edeka | ja | ja |
| Aral | ja | ja |
| Shell | ja | ja |
| OBI | ja | ja |
| McDonald's | ja | ja |
| dm | ja | ja |


### Dritter Test: Online-Zahlung
Habe das Netflix-Abo so umgestellt, dass es ab jetzt von der Binance-Karte abbucht. Weiterhin habe ich die Karte als Zahlungsmittel bei PayPal angegeben. All das hat ohne Probleme funktioniert.<br />`##Picture##:408:93:netflix-zahlung-ueber-binance:webp:Netflix-Zahlung über Binance`

Weitere getestete Services:

| Onlineshop/Service | Erfolgreich |
| :--- | ---: |
| PayPal | ja |
| Netflix | ja |
| PlayStation Network | ja |
| Amazon | ja |
| Lieferando | ja |
| Fiverr | ja |


### Vierter Test: Rückerstattungen
Wird eine Bestellung storniert, wird der Betrag für gewöhnlich auf das Ursprungszahlungmittel zurücküberwiesen. Seit März 2021 ist dies nun auch bei Binance möglich. Dein Geld geht somit nicht verloren.<br />`##Picture##:590:438:rueckerstattung-von-obi:webp:Rückerstattung von OBI`<br />`##Picture##:382:466:rueckerstattung-uebersicht:webp:Übersicht Rückerstattung`

## Fragen und Antworten

### Wie lange dauert es, bis die Kreditkarte ankommt?
Von Bestellung bis Lieferung hat es bei mir 15 Tage gedauert.

### Muss ich steuerlich etwas beachten?
Wenn du einen Kauf tätigst und es werden Kryptowährungen (außer Euro) hierfür verwendet, wechselt Binance diese Kryptowährungen zum aktuellen Kurs. Ob und in welchem Umfang [§23 EStG](https://www.gesetze-im-internet.de/estg/__23.html) oder andere Gesetze Anwendung finden kläre bitte mit deinem Steuerberater, da ich hierüber keine Auskunft geben darf.

### Kann ich aus Versehen mit Kryptowährungen bezahlen, die ich nicht für die Karte vorgesehen habe?
Das kann passieren, wenn du die Option **Automatische Aufladung** aktiviert hast. Wenn du das nicht willst, deaktiviere diese Option. Damit bestimmt du selbst, wann und wie du deine Karte auflädst.<br />`##Picture##:688:327:automatische-aufladung:webp:Automatische Aufladung`

### Kann ich die Priorität der Kryptowährungen bestimmen?
Ja, du kannst die Reihenfolge ganz einfach durch Verschieben der einzelnen Unterkonten verändern. Klicke/Tippe dazu auf das linke Symbol mit den Punkten und halte die Maus/den Finger gedrückt. Verschiebe nun das Konto an die gewünschte Position.<br />`##Picture##:444:236:wallet-reihenfolge-aendern:webp:Reihenfolge der Wallets ändern`

### Kann ich die PIN der Kreditkarte ändern?
Das ist über den Browser bzw. der App nicht vorgesehen. Kontaktiere hierzu den Binance-Support.

### Funktionieren Rückerstattungen?
Ja, siehe [Vierter Test: Rückerstattungen](#vierter-test-rückerstattungen).


## Weitere Ressourcen
- [Binance Card Cashback](https://www.binance.com/en/support/faq/c93fe535bcf4431aa32623ae0a49d4f2)
- [Notice on Binance Card Cashback Level Adjustment](https://www.binance.com/en/support/announcement/a3180a76432b49d9bcd5dfc7559eadeb)

`##Include##: _crypto-disclaimer-de`

`##Tags##: binance kreditkarte debitkarte krypto`
