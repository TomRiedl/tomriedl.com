# Stablecoins tauschen: Spot-Trading BUSD/USDT in der Binance-App

`##Description##: Binance Stablecoin Spot-Trading am Beispiel von BUSD und USDT`
`##Type##: article`
`##Locale##: de-DE`

![Stablecoins tauschen: Spot-Trading BUSD/USDT in der Binance-App](cover-binance-stablecoins-tauschen-spot-trading-usdt-busd.png)

Das Tutorial zeigt Schritt für Schritt, wie man bei [Binance](BINANCE_URL) USDT gegen BUSD tauscht/handelt. BUSD und USDT können natürlich gegen jedes andere Handelspaar ausgetauscht werden, der Vorgang ist immer der gleiche.

Solltest du noch keinen Binance-Account haben, [kannst du dich hier registrieren](BINANCE_URL) - Bei der Registrierung über diesen Link erhältst du Zugang zur VIP-Gruppe mit zusätzlichem Support. [Mehr Infos hier](/support/vip-gruppe/).

## Anleitung
1. Die [Binance](BINANCE_URL)-App öffnen.
2. Stelle sicher, dass du die [App auf Professional umgestellt hast](/krypto/binance/zwischen-binance-lite-binance-professional-wechseln/).
3. In der Tab-Leiste **Trades** auswählen.<br />![Trades-Bereich der Binance-App](binance-app-bereich-trades.webp)
4. Ist das gewünschte Handelspaar nicht ausgewählt, auf das aktuelle Paar tippen.<br />![Handelspaar wechseln](handelspaar-wechseln.webp)
5. Nun das gewünschte Trading-Paar suchen und auswählen. Für dieses Tutorial wurde BUSD/USDT gewählt.<br />![Trading-Pair suchen](binance-suche-trading-pair.webp)
6. **Kaufen** oder **Verkaufen** wählen. Die Richtung ist abhängig vom gewählten Paar. Nehmen wir die fiktiven Paare AAA/BBB. Will man AAA erwerben, so wählt man **Kaufen** aus, will man BBB erwerben **Verkaufen**. Ob man die richtige Richtung gewählt hat, kann leicht durch die Zeile **Verfügbar** weiter unten erkennen. Hier sollte das stehen, das man selbst hat und tauschen will.<br />![Kaufen oder Verkaufen wählen](kaufen-verkaufen-waehlen.webp)
7. **Limit** ist vorausgefüllt, wenn nicht, wähle **Limit** aus. Hast du mit dem Traden mehr Erfahrung, kannst du natürlich auch andere Mechaniken wählen.<br />![Limit Trade wählen](limit-trade.webp)
8. Im nächsten Schritt wählst du den Preis deines Kaufes/Verkaufes für eine Einheit. In unserem Beispiel ist das 1.0000 (Eins-Komma-Null), da ein BUSD, genauso wie ein TUSD einem Dollar entspricht. Hast du das Paar BTC/BUSD, dann wäre das je nach Kurs z.B. bei 35,000 (fünfunddreißigtausend). Den Wert kannst du frei bestimmen; ob die Order sofort ausgelöst wird ([Taker](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#taker)) oder später ([Maker](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#maker)) ist abhängig davon, ob der Trade (Faktor * Volumen) durch das [Orderbuch](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#orderbuch) bedient werden kann.<br />![Preis für Kauf festlegen](preis-fuer-kauf-festlegen.webp)<br />Auf der rechten Seite siehst du einen zusammengefassten Ausschnitt des Orderbuches, sowie den aktuellen Marktwert, hier kannst du dich für die Wahl des Preises orientieren.<br />![Aktueller Marktpreis](marktpreis.webp)<br />Willst du den Kursverlauf sehen, kannst du auf das Kerzen-Symbol rechts oben klicken.<br />![Kerzen Kurs](candles-kerzen-kurs.webp)
9. Als nächstes wählst du Anzahl der Einheiten, die du tauschen möchtest. Unterhalb des Eingabefeldes findest du Schnellwahlknöpfe für 25, 50, 75 und 100 Prozent deines verfügbaren Guthabens.<br />![Anzahl wählen](anzahl-waehlen.webp)
10. Direkt darunter kannst du prüfen, wieviele Einheiten du vom anderen Handelspaar bekommst.<br />![Erwartete Anzahl](erwartete-anzahl.webp)
11. Mit Klick auf den Kaufen/Verkaufen-Button erstellst du die Order. Beachte hierbei, dass es keinen weiteren Bestätigungsdialog gibt. Mit einem Klick wird die Order sofort erstellt.<br />![Order erstellen](order-erstellen.webp)
12. Gibt es zu deiner Order ein passendes Angebot im [Orderbuch](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#orderbuch), wird der Trade sofort ausgeführt. Wenn nicht, wird dieser in das Orderbuch aufgenommen und du kannst den aktuellen Status in der Liste unten sehen.<br />![Offene Order](offene-order.webp)<br />Mit einem Klick auf den Eintrag erhältst du mehr Details und zu wievielen Prozent der Trade abgeschlossen ist.<br />![Zusammenfassung der Order](zusammenfassung-order-binance.webp)



[➡️ Weitere Artikel zu Binance](/tag/binance/)

`##Include##: _crypto-disclaimer-de`

`##Tags##: binance spot trading busd usdt stablecoin krypto`
