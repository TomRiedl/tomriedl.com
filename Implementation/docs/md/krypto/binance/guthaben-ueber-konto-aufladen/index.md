# Binance-Guthaben über Bankkonto aufladen (SEPA-Überweisung)

`##Description##: Anleitung zur Überweisung von deinem Konto an Binance`
`##Type##: article`
`##Locale##: de-DE`

`##Picture##:1080:552:cover-guthaben-ueber-konto-aufladen:webp,png:Binance-Guthaben über Bankkonto aufladen (SEPA-Überweisung)`

Bei [Binance](BINANCE_URL) ist es sehr einfach über das eigene Bankkonto Euro auf die Spot-Wallet zu überweisen. Weiterhin ist das Geld unter der Woche in weniger als 24 Stunden verfügbar. Hierbei erhältst du von [Binance](BINANCE_URL) ein SEPA-Empfängerkonto und einen bestimmten Verwendungszweck, damit die Überweisung richtig zugeordnet werden kann. Wie es genau funktioniert, kannst du in diesem Tutorial nachlesen.

Solltest du noch keinen Binance-Account haben, [kannst du dich hier registrieren](BINANCE_URL) - Bei der Registrierung über diesen Link erhältst du Zugang zur VIP-Gruppe mit zusätzlichem Support. [Mehr Infos hier](/support/vip-gruppe/).

Für alternative Einzahlmöglichkeiten siehe den Artikel [Alternative Einzahlmöglichkeiten für Binance in Euro](/krypto/binance/alternative-einzahlmoeglichkeiten/).

- [Anleitung für die App](#anleitung-für-die-app)
- [Anleitung für die Website (Browser)](#anleitung-für-die-website-browser)

## Anleitung für die App
1. Die [Binance](BINANCE_URL)-App öffnen.
2. Stelle sicher, dass du die [App auf Professional umgestellt hast](/krypto/binance/zwischen-binance-lite-binance-professional-wechseln/).
3. Klicke auf der Startseite auf **EUR einzahlen**.<br />`##Picture##:714:400:binance-eur-einzahlen:webp:Binance EUR einzahlen`
4. Wähle unter **Einzahlung mit** den Eintrag **Banküberweisung (SEPA)**. Die Einzahlung im Screenshot ist kostenlos; [Binance](BINANCE_URL) bietet in Aktionszeiträumen eine kostenlose Einzahlung an, ohne Aktion liegt die Gebühr bei 0.02% (Stand 2021-02-06).<br />`##Picture##:688:331:bankueberweisung-sepa:webp:Banküberweisung SEPA`
5. Solltest du die zusätzliche Verifizierung noch nicht abgeschlossen haben, musst du dies vor der ersten Überweisung tun. Hast du die Verifizierung bereits abgeschlossen kannst du diesen Schritt überspringen.<br />Für die zusätzliche Verifizierung klicke auf den Button **Jetzt verifizieren** und gebe die benötigten Informationen an.<br />`##Picture##:614:257:zusaetzliche-verifizierung:webp:Zusätzliche Verifizierung`<br />
6. Trage deinen gewünschten Betrag ein.<br />`##Picture##:705:222:betrag-eingeben:webp:Betrag eingeben`
7. Bestätige den Betrag mit einem Klick auf **Bestätigen**.<br />`##Picture##:695:686:betrag-bestaetigen:webp:Betrag bestätigen`
8. Sehe dir die Hinweise an und bestätige sie mit einem Klick auf **Ok**. Die Hinweise sagen folgendes:<br />&bull; Der Kontoname muss mit dem des [Binance-Kontos](BINANCE_URL) übereinstimmen, du kannst also nur von deinem Konto auf das von Binance überweisen. Geld auf das Binance-Konto eines Freundes kannst du aufgrund von Geldwäschegesetze nicht vornehmen.<br />&bull; Der Begünstigte (der Empfänger) muss **Binance** lauten.<br />`##Picture##:578:801:hinweise-ueberweisung:webp:Hinweise für die Überweisung`
9. In der folgenden Übersicht siehst du alle relevanten Informationen für die Banküberweisung. Für gewöhnlich benötigst du hierfür nur den Empfänger, die IBAN und den Verwendungszweck (Referenzcode). Wichtig ist, dass der Verwendungszweck übereinstimmt, gebe also keine zusätzlichen Informationen an, nur die angezeigte Zahl.<br />`##Picture##:688:990:ueberweisungsdaten:webp:Binance Überweisungsdaten über Clear Junction`
10. Überweise den zuvor gewählten Betrag an den angegebenen Empfänger.<br />`##Picture##:635:143:ueberweisung-fidor:webp:Überweisung Fidor`
11. An Bankarbeitstagen waren meine Überweisungen immer in unter 24 Stunden bei Binance, bei Überweisungen an Wochenenden kam die Überweisung stets am Montag.
12. Die Transaktionshistorie kannst du unter **Wallet** mit einem Klick auf das Transaktions-Symbol ansehen.<br />`##Picture##:709:382:transaktionshistorie-oeffnen:webp:Transaktionshistorie öffnen`<br />`##Picture##:638:704:angekommene-ueberweisung:webp:Überweisung angekommen`

## Anleitung für die Website (Browser)
1. [Auf der Binance-Homepage einloggen](BINANCE_URL).
2. Im Menü links oben **Kryptos kaufen** auswählen, sicherstellen, dass **EUR** ausgewählt ist und auf **Bankeinzahlung** klicken.<br />`##Picture##:376:274:binance-website-kryptos-kaufen:webp:Binance Website Kryptos kaufen`
3. **Banküberweisung (SEPA)** auswählen, den gewünschten Betrag eingeben und auf **Weiter** klicken.<br />`##Picture##:1419:593:homepage-betrag-eingeben:webp:Betrag eingeben`
4. Den angezeigten Hinweis lesen und bestätigen.<br />Der Hinweis sagt:<br />&bull; Du darfst nur von deinem Konto aus überweisen (die Namen vom Konto und Binance müssen übereinstimmen; Grund hierfür sind die Bestimmungen zur Geldwäsche).<br />&bull; Du musst als Empfänger **Binance** angeben.<br />`##Picture##:516:386:regeln-bestaetigen:webp:Regeln bestätigen`
5. Nun werden dir die Überweisungsdaten angezeigt. Für gewöhnlich benötigst du nur Empfänger, IBAN und Verwendungszweck (Referenzcode).<br />`##Picture##:619:527:ueberweisungsdaten-website:webp:Überweisungsdaten`
6. Führe die Überweisung durch.<br />`##Picture##:635:143:ueberweisung-fidor:webp:Überweisung Fidor`
7. An Werktagen sind die Überweisungen immer in unter 24 Stunden angekommen, bei Überweisungen an Wochenenden kam die Überweisung stets am Montag.
8. Die Transaktionshistorie siehst du auf der gleichen Seite unten unter **Fiat-Einzahlungsverlauf**.<br />`##Picture##:1002:269:fiat-einzahlungsverlauf:webp:Einzahlungsverlauf Fiat`


[➡️ Weitere Artikel zu Binance](/tag/binance/)

`##Include##: _crypto-disclaimer-de`

`##Tags##: binance konto sepa ueberweisen aufladen krypto`
