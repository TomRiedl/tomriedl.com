# Binance Account registrieren - Konto erstellen - Deutsche Anleitung

`##Description##: Tutorial zum Erstellen und Verifizieren eines Binance-Kontos`
`##Type##: article`
`##Locale##: de-DE`

`##Picture##:1080:552:cover-binance-account-registrieren-und-verifizieren:webp,png:Binance Account registrieren - Konto erstellen - Deutsche Anleitung`

[Binance](BINANCE_URL) ist eine europäische Kryptobörse mit Hauptsitz in Malta und der Binance Deutschland GmbH & Co. KG als verantwortliche Instanz der deutschen Plattform. Binance unterliegt somit der DSGVO.

Das Erstellen eines [Binance-Accounts](BINANCE_URL) ist sehr leicht (ca. 2 Minuten), lediglich die Verifizierung ist etwas aufwändiger (ca. 10 Minuten). Die Verifizierung ist für die Nutzung der Plattform nicht zwingend notwendig, jedoch sind ohne die Verifizierung Funktionen eingeschränkt.

Wenn du dich über meinen Affiliate-Link anmeldest erhältst du Zugang zur [VIP-Gruppe](/support/vip-gruppe/) und ich unterstütze dich persönlich, solltest du Schwierigkeiten auf der Binance-Plattform haben. Der Code lautet **BINANCE_CODE** und der zugehörige Link ist folgender: [BINANCE_URL](BINANCE_URL) - In der Anleitung weiter unten erhältst du noch mehr Informationen dazu.

Solltest du noch keine Erfahrung mit Kryptowährungen haben, wird dir diese Anleitung, sowie die [anderen Anleitungen zu Binance](/tag/binance/) eine gute Unterstützung sein. Das Spekulieren mit Kryptowährungen kann zu großen Gewinnen führen, ist jedoch auch mit Risiken verbunden. [Weitere Informationen zu den Risiken](/version1/risiken-beim-spekulieren-mit-kryptowaehrungen/).



- [Konto erstellen - Anleitung](#konto-erstellen---anleitung)
- [Account verifizieren - Anleitung](#account-verifizieren---anleitung)
- [Weitere Artikel zu Binance](#weitere-artikel-zu-binance)

## Konto erstellen - Anleitung
1. Öffne die Registrieren-Seite über folgenden Link: [BINANCE_URL](BINANCE_URL)
2. Du hast nun die Wahl, das Konto mit einer E-Mailadresse oder einer Handynummer zu registrieren. In der folgenden Anleitung wird der Weg über die E-Mail beschrieben, über die Handynummer ist es sehr ähnlich und die Anleitung funktioniert genau so.<br />`##Picture##:381:146:konto-mit-email-oder-handy:webp:Binance-Konto mit E-Mail oder Handynummer erstellen`
3. Gib deine **E-Mail**, dein **Passwort** und die Empfehlungs-ID **BINANCE_CODE** ein. Mit der Empfehlungs-ID erhältst du Zugang zu meiner VIP-Gruppe mit persönlicher Unterstützung.<br />`##Picture##:382:260:email-passwort-ref-id:webp:E-Mail, Passwort und Referral ID eingeben`
4. Lies dir die Nutzungsbedingungen durch, akzeptiere sie durch das Setzen des Hakens und klicke auf **Konto erstellen**.<br />`##Picture##:381:122:nutzungsbedingungen-akzeptieren:webp:Nutzungsbedingungen akzeptieren und Konto erstellen`
5. Um zu bestätigen, dass du ein echter Mensch bist, musst du du das [Captcha](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#captcha) lösen. Hierzu schiebst du mit der Maus das Puzzlestück an den dafür vorgesehenen Platz (hier nicht das Puzzlestück schieben, sondern mit dem Pfeil-Button unter den Bild).<br />`##Picture##:334:349:binance-captcha:webp:Binance CAPTCHA lösen`
6. Öffne dein E-Mail-Postfach, du solltest eine E-Mail mit einem 6-stelligen Code erhalten haben. Dieser Code wird im nächsten Schritt benötigt.<br />`##Picture##:599:329:email-code:webp:Code der Bestätigungsemail`
7. Gib den Code in die dafür vorgesehenen Felder ein.<br />`##Picture##:382:333:eingabe-aktivierungscode:webp:Eingabe des Aktivierungscodes`
8. Die Registrierung ist abgeschlossen. Klicke rechts oben auf **Zum Dashboard gehen**, um auf die Hauptseite zu gelangen.<br />`##Picture##:667:720:registrierung-abgeschlossen-zum-dashboard:webp:Weiter zum Dashboard`
9. Ab jetzt kannst du die Plattform nutzen, jedoch sind einige Funktionen gesperrt, da dein Konto noch nicht verifiziert ist. Im folgenden Kapitel findest du eine [Anleitung zur Konto-Verifizierung](#anleitung-account-verifizieren).<br />Wenn du dich über meinen Ref-Link bzw. Code angemeldet hast, kannst du dich für die VIP-Gruppe anmelden. [Zur Anleitung für die VIP-Gruppe](/support/vip-gruppe/)

## Account verifizieren - Anleitung
Bei jeder Kryptobörse ist es notwendig, dass man sich als Person verifiziert. Gründe hierfür sind gesetzliche Vorgaben, meist aufgrund zur Unterbindung von Geldwäsche. [Binance](BINANCE_URL) unterliegt mit ihrem Sitz in der EU, sowie der Tochter in Deutschland der DSGVO.

1. Öffne über das Profil-Menü rechts oben den Menüpunkt **Identitätsprüfung**<br />`##Picture##:442:459:identitaetspruefung-menu:webp:Menü Identitätsprüfung`
2. Klicke unter Persönliche Angaben auf **Verifizieren**<br />`##Picture##:824:254:identitaetspruefung-starten:webp:Verifizierung starten`
3. Wähle das Land, das auf deinem Personalausweis als Wohnsitz angegeben ist und klicke auf **Start**.<br />`##Picture##:791:632:land-auswaehlen:webp:Verifizierung starten`
4. Gib deinen Namen, Geburtsdatum und Wohnsitz an. Klicke anschließend auf **Einreichen und fortfahren**<br />`##Picture##:796:635:verifizierung-adresse:webp:Verifizierung starten`
5. Wähle ein Dokument, das du für die Verifizierung verwenden willst.<br />**ID Card** = Personalausweis<br />**Passport** = Reisepass<br />**Driver's License** = Führerschein<br />Klicke anschließend auf **Weiter**.<br />`##Picture##:792:634:wahl-ausweis-zur-verifikation:webp:Verifizierung starten`
6. Du kannst nun entweder ein Foto der Vorderseite und der Rückseite des Dokument hochladen oder direkt über die Kamera ein Foto machen.<br />`##Picture##:795:637:ausweis-vorderseite-rueckseite-hochladen:webp:Vorderseite und Rückseite des Ausweises hochladen`
7. Nachdem du die Vorder- und Rückseite des Ausweisdokuments hochageladen hast, kannst du mit einem Klick auf **Weiter** fortfahren.<br />`##Picture##:792:636:ausweis-fotos-hochgeladen:webp:Vorderseite und Rückseite des Ausweises hochladen`
8. Im nächsten Schritt lädst du ein Profilfoto hoch.<br />`##Picture##:796:635:foto-hochladen:webp:Profilfoto hochladen`
9. Im letzten Schritt wirst du automatisiert über die Webcam verifiziert. Hierzu klickst du auf **ID-Prüfung beginnen**. Du musst hier mit keinem Menschen telefonieren, nur eine Computerstimme gibt dir Anweisungen.<br />Hast du keine Webcam, kannst du die Verifizierung mit der Binance-App durchführen.<br />`##Picture##:796:638:erweiterte-verifizierung-starten:webp:Erweiterte verifizierung starten`
10. Erlaube dem Browser das Verwenden der Webcam, hier wird meist oben unter der Adresszeile ein kleines Fenster angezeigt.<br />`##Picture##:333:150:use-camera:webp:Kamera freigeben`
11. Folgend den Anweisungen der Computerstimme:<br />&bull; Open your mouth = Öffne deinen Mund<br />&bull; Nod your head = Nicke mit deinem Kopf<br />`##Picture##:794:627:verifizierung-gesicht:webp:Anweisungen Gesichtsverifizierung`
12. Die Registrierung ist abgeschlossen und du kannst ab jetzt dein [Binance-Konto über eine SEPA-Überweisung auffüllen](/krypto/binance/guthaben-ueber-konto-aufladen/) oder deine Gewinne auf dein Bankkonto auszahlen.

## Weitere Artikel zu Binance
Auf dieser Website findest du weitere Anleitungen zu [Binance](BINANCE_URL). Jeder Artikel hat Tags, worüber du ähnliche Artikel suchen kannst.

[➡️ Alle Artikel zu Binance](/tag/binance/)


`##Include##: _crypto-disclaimer-de`

`##Tags##: binance account konto registrieren krypto`
