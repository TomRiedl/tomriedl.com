# Binance BNB Vault - BNB Staking

`##Description##: Anleitung und Informationen zum BNB-Vault von Binance`
`##Type##: article`
`##Locale##: de-DE`

![Binance BNB Vault - BNB Staking](cover-bnb-staking.png)

Der [Binance](BINANCE_URL) BNB-Vault (eng. Tresor/Schatzkammer) wurde 2020-11-03 eröffnet und bietet Binance-Nutzern viele Vorteile. Im folgenden Artikel erkläre ich die Vorteile des Vaults, wie man BNB dort ablegt und kläre noch weitere häufige Fragen.

Solltest du noch keinen Binance-Account haben, [kannst du dich hier registrieren](BINANCE_URL) - Bei der Registrierung über diesen Link erhältst du Zugang zur VIP-Gruppe mit zusätzlichem Support. [Mehr Infos hier](/support/vip-gruppe/).

- [Kurze Erklärung](#kurze-erklärung)
- [Vorteile](#vorteile)
- [Anleitung](#anleitung)
  - [BNB in den Tresor legen (Tutorial für die App)](#bnb-in-den-tresor-legen-tutorial-für-die-app)
  - [BNB aus dem Tresor holen (Tutorial für die App)](#bnb-aus-dem-tresor-holen-tutorial-für-die-app)
- [Fragen und Antworten](#fragen-und-antworten)
  - [Kann ich die BNB jederzeit aus dem Vault holen?](#kann-ich-die-bnb-jederzeit-aus-dem-vault-holen)
  - [Sind die BNB im Vault andere als in der Earn-Wallet?](#sind-die-bnb-im-vault-andere-als-in-der-earn-wallet)
  - [Ab wann erhält man Einnahmen durch das Staking?](#ab-wann-erhält-man-einnahmen-durch-das-staking)
  - [Wohin gehen die Einnahmen?](#wohin-gehen-die-einnahmen)
  - [Kann ich die BNB automatisch im Tresor ablegen lassen?](#kann-ich-die-bnb-automatisch-im-tresor-ablegen-lassen)
  - [Was muss ich steuerlich beachten?](#was-muss-ich-steuerlich-beachten)

## Kurze Erklärung
BNB sind die nativen Token der [Binance](BINANCE_URL)-Chain und wurden 2017 durch ein [ICO](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#ico) erstmalig angeboten. Der Besitz von BNB bietet beim Traden Vorteile (25% Rabatt der Gebühren, wenn man mit BNB zahlt), sowie andere Boni, je nach genutzten Funktionen. Wenn du deine BNB länger halten und sie nicht für das Day-Trading verwenden willst, kannst du sie in die BNB-Vault legen. Dort erhälts Zinsen, sowie [weitere Boni](#vorteile).


## Vorteile
Das Halten deiner BNB im BNB-Vault hat folgende Vorteile:
- Du erhältst Einnahmen bzw. Zinsen auf deine eingelegten BNB (ca. 5% pro Jahr, Stand 2021-01-30).<br />![BNB Zinsen](bnb-zinsen.webp)
- Nutzt du die [Binance-Kreditkarte](/krypto/binance/binance-kreditkarte-anleitung-vorstellung-und-test/), zählen deine BNB in der Vault ebenfalls zur Stufenermittlung.
- Teilnahme an [Airdrops](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#airdrop) und Verteilung neuer Token des Launchpools.<br />![Launchpool Auszahlungen](binance-launchpool-auszahlungen.webp)

## Anleitung

### BNB in den Tresor legen (Tutorial für die App)
1. [Binance](BINANCE_URL)-App öffnen.
2. Auf der Startseite auf **Mehr** klicken.<br />![Binance App Startseite Mehr Funktionen](binance-app-startseite-mehr.webp)
3. Unter **Finanzen** den Eintrag **BNB Vault** auswählen.<br />![BNB Vault App](bnb-vault-binance-app.webp)
4. Auf der BNB-Vault Übersichtsseite auf den Button **Stake** tippen.<br />![BNB Vault Stake Button](bnb-vault-stake-button.webp)
5. Gewünschten Betrag eingeben.<br />![Betrag Staking eingeben](betrag-staking.webp)
6. Service Agreement lesen und akzeptieren.<br />![Service Agreement Checkbox](bnb-staking-service-agreement.webp)
7. Einzahlung mit dem Klick auf **Confirm** bestätigen.<br />![Einzahlung bestätigen](einzahlung-bestaetigen.webp)
8. Transfer in den BNB-Tresor ist abgeschlossen.<br />![Staking erfolgreich](erfolgreiches-staking.webp)
9. Willst du in Zukunft deine BNB automatisch in die Vault legen lassen, aktiviere **Auto Transfer**. Hierbei werden ab 00:00 [UTC](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#utc) die BNB der Spot-Wallet in den Vault transferiert.<br />![BNB Vault Auto Transfer](bnb-vault-auto-transfer.webp)


### BNB aus dem Tresor holen (Tutorial für die App)
1. [Binance](BINANCE_URL)-App öffnen.
2. Auf der Startseite auf **Mehr** klicken.<br />![Binance App Startseite Mehr Funktionen](binance-app-startseite-mehr.webp)
3. Unter **Finanzen** den Eintrag **BNB Vault** auswählen.<br />![BNB Vault App](bnb-vault-binance-app.webp)
4. Auf den Button **UnStake** tippen.<br />![BNB Unstake Button](bnb-vault-unstake.webp)
5. Gewünschten Betrag eingeben.<br />![Betrag Unstaking](betrag-unstaking.webp)
6. Auszahlungsmethode auswählen.<br />&bull; **Fast redemption**: Hier werden die BNB sofort auf die Spot-Wallet transferiert. Für den heutigen Tag erhältst du dabei keine Zinsen mehr.<br />&bull; **Standard redemption**: Die Auszahlung erfolgt am nächsten Tag, du erhältst somit für den heutigen Tag noch Zinsen.<br />![Methode Auszahlung](methode-auszahlung.webp)
7. Service Agreement lesen und akzeptieren.<br />![Service Agreement der Auszahlung akzeptieren](service-agreement-auszahlung.webp)
8. Auszahlung mit einem Klick auf **Confirm** bestätigen.<br />![Auszahlung bestätigen](einzahlung-bestaetigen.webp)


## Fragen und Antworten

### Kann ich die BNB jederzeit aus dem Vault holen?
Bis zu einem Betrag von 100,000,000 BNB kannst du die BNB sofort und größere Beträge am nächsten Tag aus dem Tresor holen. 100,000,000 BNB entspricht der Hälfte aller ursprünglich existierender Token, somit kann man die Frage kurz mit "Ja" beantworten.

### Sind die BNB im Vault andere als in der Earn-Wallet?
Nein, legst du BNB in den Vault siehst du diese ebenfalls im Earn-Portfolio.

### Ab wann erhält man Einnahmen durch das Staking?
Die Einlagen werden ab 00:00 [UTC](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#utc) des zweiten Tages zur Einnahmenberechnung hinzugezogen und die Einnahmen ab dem dritten Tag ausgeschüttet.

### Wohin gehen die Einnahmen?
Die Einnahmen werden täglich auf die Spot-Wallet transferiert.

### Kann ich die BNB automatisch im Tresor ablegen lassen?
Ja, hierzu aktivierst du den **Auto Transfer**. Ist dieser aktiviert, werden deine BNB in der Spot-Wallet automatisch jeden Tag um 02:00 UTC in den Tresor gelegt.<br />![BNB Vault Auto Transfer](bnb-vault-auto-transfer.webp)

### Was muss ich steuerlich beachten?
Da ich kein Steuerberater bin, darf ich hier keine Auskunft geben. Kläre bitte mit deiner:m Steuerberater:in, ob und in welchem Umfang [§23 EStG](https://www.gesetze-im-internet.de/estg/__23.html) oder andere Gesetze Anwendung finden.


[➡️ Weitere Artikel zu Binance](/tag/binance/)


`##Include##: _crypto-disclaimer-de`

`##Tags##: binance bnb vault staking stake krypto`

<ul class="LanguageSelection">
    <li><a href="/zh/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81/%E5%B8%81%E5%AE%89/%E5%B8%81%E5%AE%89-BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-BNB-%E6%9D%83%E7%9B%8A%E8%B4%A8%E6%8A%BC/">中文</a></li>
</ul>

`##Translation##:de:https://##Domain##/krypto/binance/binance-bnb-vault-bnb-staking/`
`##Translation##:zh-Hans:https://##Domain##/zh/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81/%E5%B8%81%E5%AE%89/%E5%B8%81%E5%AE%89-BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-BNB-%E6%9D%83%E7%9B%8A%E8%B4%A8%E6%8A%BC/`

