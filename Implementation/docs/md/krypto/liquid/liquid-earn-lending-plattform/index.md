# Liquid Earn - Lending Plattform

`##Description##: Bei Liquid Zinsen auf die Kryptowährungen erhalten`
`##Type##: article`
`##Locale##: de-DE`

![Liquid führt Lending ein - Liquid Earn](liquid-earn.jpg)

Die Kryptobörse [Liquid](LIQUID_URL) (alter Name "Quoinex") führt demnächst **Liquid Earn** ein. Hierbei partnern sie mit der Lending-Plattform [Celsius](CELSIUS_URL) aus England. Bei Celsius kann man sich mittlerweile als deutscher Bürger nicht mehr registrieren, man hatte also keine Möglichkeit mehr von dem Zinssystem von Celsius zu profitieren. Nun ist das über Liquid dennoch möglich.

Solltest du noch keinen Liquid-Account haben, [kannst du dich hier registrieren](LIQUID_URL) - Bei der Registrierung über diesen Link erhältst du Zugang zur VIP-Gruppe mit zusätzlichem Support. [Mehr Infos hier](/support/vip-gruppe/).

[➡️ Zur Anleitung](#anleitung)

## Was ist Lending
Beim Lending kannst du deine Kryptowährungen, die du nicht kurzfristig für das Day-Trading benötigst auf eine Art Tagesgeldkonto legen. Die Lending-Plattformen selbst verleihen dabei deine eingelegten Kryptowährungen an andere Personen, welche aber ein Pfand (Collateral) hinterlegen müssen (das meist den eigentlichen Leihwert weit übersteigt) oder deren andere Assets die Kreditlinie bestimmen. Sollte der Leiher das Geliehene nicht zurückzahlen können, wird das Pfand eingezogen bzw. höhere Zinsen auf die verbleiben Assets erhoben. Wenn du über das Lending Kryptowährungen zum Leihen anbietest, hast du über die Plattform die Sicherheit, dass du deine Kryptowährungen auch wieder zurückerhältst. Eine alternative Lending-Plattformen ist z.B. [Nexo](NEXO_URL). Bei den Börsen [Binance](BINANCE_URL) oder [Bitfinex](BITFINEX_URL) mit der Automatisierung über [Coinlend](/version1/kryptowaehrungen-gegen-zinsen-verleihen-coinlend-anleitungtutorial) kannst du ebenfalls Zinsen erhalten, diese Assets werden aber nicht für Kredite verwendet.

## Zinsen
Durch die Verleihgebühren und das Einziehen der Collaterals von den Ausleihern, können Verleihplattformen gute Zinsen anbieten. Die aktuellen Zinsen bei Celsius:<br />
![Zinsen bei Celsius Network](zinsen-celsius.png)<br />
*Screenshot von [Coinlend](COINLEND_URL) (Stand 2021-01-04)*<br />
Da Liquid das Sparen über Celsius anbietet, gehe ich davon aus, dass die Zinsen etwas geringer sein werden.

## Anleitung
Liquid Earn ist noch nicht online, deshalb kannst du dich vorab für einen Early-Access registrieren. Sobald die Lending-Plattform online ist, werde ich das System vertesten und diesen Artikel um eine Anleitung ergänzen.

1. [Account bei Liquid registrieren](LIQUID_URL); falls du schon einen hast, kannst du diesen Punkt überspringen.
2. [Für den Early-Access anmelden](https://be.liquid.com/liquid-earn).

`##Include##: _crypto-disclaimer-de`

```##Tags##: liquid liquid-earn quoinex lending verleih zinsen kryptowährung sparen```
