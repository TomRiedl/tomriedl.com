# Abkürzungen, Definitionen und Begriffe der Kryptowelt

`##Description##: Beschreibung häufig verwendeter Begriffe aus der Welt der Kryptowährungen`
`##Type##: article`
`##Locale##: de-DE`

Wie jeder andere Fachbereich hat auch die Kryptowelt ihre eigenen Bezeichnungen. Die wichtigsten sind hier aufgelistet.

Sollte etwas fehlen, teile es mir gerne auf [GitLab](https://gitlab.com/TomRiedl/tomriedl.com/-/issues/new) mit.

`##Style##:table td { vertical-align: top; }`
`##Style##:table h3 { margin-top: 0; padding-top: 0; font-size: 1.1em; }`
`##Style##:.contentnav-0-0-x .h3, .contentnav-0-0-x a.h3 { display: none; }`
`##Style##:table { min-width:initial !important; width: 100%; }`
`##Style##:table tr { display:flex; flex-flow:row wrap; width:100%; }`
`##Style##:table td { flex:1 1 33%; }`
`##Style##:table td h3 { margin-bottom: 0; }`

<input type="search" id="searchInput" placeholder="Suchbegriff" oninput="filterTable(this.value)"></input>
<script>
    function filterTable(query) {
        const filter = query.toUpperCase();
        const trs = document.querySelectorAll('table tbody tr');
        trs.forEach(tr => tr.style.display = [...tr.children].find(td => td.innerHTML.toUpperCase().includes(filter)) ? '' : 'none');
    }

    function clickCallback(e) {
        if (e.target.tagName !== 'A') { return; }
        if (!e.target.href || e.target.href.indexOf("#") < 0) { return; }

        document.getElementById("searchInput").value = "";
        filterTable("");
    }

    if (document.addEventListener) {
        document.addEventListener('click', clickCallback, false);
    } else {
        document.attachEvent('onclick', clickCallback);
    }
</script>

[A](#a) - [B](#b) - [C](#c) - [D](#d) - [E](#e) - [F](#f) - [G](#g) - [H](#h) - [I](#i) - [K](#k) - [L](#l) - [M](#m) - [N](#n) - [O](#o) - [P](#p) - [R](#r) - [S](#s) - [T](#t) - [U](#u) - [V](#v) - [W](#w)

| Begriff/Abkürzung | Bedeutung | Beschreibung/Anmerkung |
| --- | ------ | --------- |
| <h2 id="a">A</h2> |
| <h3 id="ada">ADA</h3> | Cardano | Eine Kryptowährung mit dem Symbol "ADA". [Offizielle Homepage cardano.org](https://cardano.org/). Cardano kann u.a. auf [Binance](BINANCE_URL) gehandelt werden. |
| <h3 id="affiliate">Affiliate</h3> | Partner/anschließen | Auf Vermittlungsprovision basierende Partnerschaften. Diese Partnerschaften bieten meist Vorteile für beide Parteien, wie Rabatte oder Provisionszahlungen. |
| <h3 id="airdrop">Airdrop</h3> | Fallschirmabwurf | Das kostenlose Verteilen neuer Kryptowährungen/Tokens anhand bestimmter Bedingungen, welche je nach Projekt unterschiedlich sind. |
| <h3 id="altcoin">Altcoin</h3> | Alternative Coin | Alle Kryptowährungen, die nicht Bitcoin sind (Originaldefinition). Teilweise werden andere große Kryptowährungen, wie Ethereum von den Altcoins ausgenommen. |
| <h3 id="ama">AMA</h3> | Ask Me Anything | Eine Art Interview, bei der verschiedene Fragen aus der Community an eine Person gestellt werden. Je nach System geschieht das live oder mit Fragen vorab. |
| <h3 id="aml">AML</h3> | Anti Money Laundering | Maßnahmen gegen Geldwäsche. |
| <h3 id="apr">APR</h3> | Annual Percentage Rate | Effektiver Jahreszins. [Wikipedia "Effektiver Jahreszins"](https://de.wikipedia.org/wiki/Effektiver_Jahreszins) |
| <h3 id="apy">APY</h3> | Annual Percentage Yield | Jährliches Return on Investment ([ROI](#roi)) in Prozent. |
| <h3 id="ath">ATH</h3> | All Time High | Der höchste Kurs, den eine Währung je erreicht hat. |
| <h3 id="aum">AUM</h3> | Assets Under Management | Verwaltetes Vermögen. Beschreibt das Volumen an Kundenwerten, das von einem Unternehmen/einer Person verwaltet wird. [Wikipedia "Assets under management"](https://de.wikipedia.org/wiki/Assets_under_management) |
| <h2 id="b">B</h2> |
| <h3 id="bc">BC</h3> | Binance Chain | Ein Blockchain-System von [Binance](BINANCE_URL). [Siehe docs.binance.org](https://docs.binance.org/). [Alle Artikel zu Binance](/tag/binance/) |
| <h3 id="bep2">BEP2</h3> | Binance Chain Evolution Proposal 2 | Technischer Standard für die Ausgabe und Implementierung von Token auf der [Binance](BINANCE_URL)-Chain. [Siehe Binance-Academy "BEP-2"](https://academy.binance.com/de/glossary/bep-2?ref=BINANCE_CODE). Artikel zu [Binance](/tag/binance/) |
| <h3 id="bi">BI</h3> | Business Insider | Eine Finanz-Nachrichten-Webseite von Axel Springer SE. [Wikipedia "Business Insider"](https://de.wikipedia.org/wiki/Business_Insider) |
| <h3 id="blockchain">Blockchain</h3> | - | Eine erweiterbare Liste von Datensätzen (Blocks), die nachprüfbar miteinander verkettet sind. [Wikipedia "Blockchain"](https://de.wikipedia.org/wiki/Blockchain) |
| <h3 id="bnb">BNB</h3> | Binance Token | Das native Asset auf der [Binance](BINANCE_URL)-Chain. [Siehe Binance Academy "BNB"](https://academy.binance.com/de/glossary/bnb?ref=BINANCE_CODE). Artikel zu [Binance](/tag/binance/) |
| <h3 id="bsc">BSC</h3> | Binance Smart Chain | Ein Blockchain Netzwerk von [Binance](BINANCE_URL) zur Ausführung von dezentralisierten Anwendungen. BSC läuft parallel zur [Binance Chain (BC)](#bc). [Siehe binance.org/en/smartChain](https://binance.org/en/smartChain). [Alle Artikel zu Binance](/tag/binance/) |
| <h3 id="btc">BTC</h3> | Bitcoin | Die aktuell wichtigste und älteste Kryptowährung Bitcoin. <https://bitcoin.org/> |
| <h3 id="busd">BUSD</h3> | Binance USD Stablecoin | [Stablecoin](#stablecoin) von [Binance](BINANCE_URL) welcher 1:1 mit US-Dollar hinterlegt ist und durch das [NYSDFS](#nysdfs) kontrolliert wird. [Greenlist-Eintrag der DFS zu BUSD (Nachweis)](https://www.dfs.ny.gov/apps_and_licensing/virtual_currency_businesses/virtual_currencies) |
| <h2 id="c">C</h2> |
| <h3 id="captcha">CAPTCHA</h3> | Completely Automated Public Turing test to tell Computers and Humans Apart | Vollautomatischer öffentlicher Turing-Test zur Unterscheidung von Computern und Menschen. Wird verwendet, um zu erkennen, ob der Nutzer ein Mensch und keine Machine ist. [Wikipedia "Captcha"](https://de.wikipedia.org/wiki/Captcha) |
| <h3 id="cbdc">CBDC</h3> | Central Bank Digital Currency | Digitales Zentralbankgeld. Elektronisches Geld, das direkt von der Zentralbank verwaltet wird. [Wikipedia "Digitales Zentralbankgeld"](https://de.wikipedia.org/wiki/Digitales_Zentralbankgeld) |
| <h3 id="cdo">CDO</h3> | Collateralized Debt Obligation | Oberbegriff für Finanzinstrumente, die zur Gruppe der forderungsbesicherten Wertpapiere und strukturierten Kreditprodukten gehören. [Wikipedia "Collateralized Debt Obligation"](https://de.wikipedia.org/wiki/Collateralized_Debt_Obligation) |
| <h3 id="cefi">CeFi</h3> | Centralized Finance | Finanzservices, die zentral von einer Organisation verwaltet und angeboten werden. Siehe auch [DeFi](#defi). |
| <h3 id="cex">CEX</h3> | Centralized Exchange | Ein Börse, welche zentral organisiert und betrieben wird. Siehe auch [DEX](#dex). |
| <h3 id="cftc">CFTC</h3> | Commodity Futures Trading Commission | Behörde der USA zur Regulierung von Future- und Optionsmärkten. [Wikipedia "Commodity Futures Trading Commission"](https://de.wikipedia.org/wiki/Commodity_Futures_Trading_Commission) |
| <h3 id="challenger-bank">Challenger Bank</h3> | Herausforderer-Bank | Eine kleinere Bank, welche mit den größeren Banken in direkter Konkurrenz stehen. Viele dieser Banken bieten spezialisierte Services, welche große Banken nicht bieten (z.B. einfacher Kauf von Kryptowährungen). |
| <h3 id="churn">Churn</h3> | Umrühren | Churn bezeichnet das Senden von Kryptowährungen an die eigene Adresse. Je nach Kryptowährung zum Zweck zur Reduzierung der Nachverfolgbarkeit oder zur Zusammenfassung mehrerer Guthaben/Adressen zu einer gemeinsamen Adresse oder in einen einzelnen Block. |
| <h3 id="client">Client</h3> | Klient/Kunde | Als Client wird meist eine Software bezeichnet, die mit einem Server kommuniziert. Es handelt sich somit oft um Software/Apps auf Endgeräten. |
| <h3 id="cmc">CMC</h3> | CoinMarketCap | Website mit Kryptowährungen, deren Kurse und weitere Informationen. [coinmarketcap.com](https://coinmarketcap.com/) |
| <h3 id="collateral">Collateral</h3> | Pfand | Eine Sicherheit, die beim Leihen von Fiat/Kryptos hinterlegt werden muss. |
| <h3 id="coordicide">Coordicide</h3> | Tod des Koordinators | Das Entfernen des Koordinators. Der Begriff wurde erstmals bei [IOTA](https://iota.org) verwendet. [Siehe Coordicide bei IOTA](https://coordicide.iota.org/) |
| <h3 id="cost-average-effect">Cost Average Effect</h3> | Durchschnittskosteneffekt | Ein möglicher positiver Effekt von Investitionen von gleichbleibendem Wert und regelmäßiger Ausführung. Der CAE ist kritisch zu betrachten, da er gegen andere Anlagestrategien häufig unterlegen ist. [Wikipedia "Durchschnittskosteneffekt"](https://de.wikipedia.org/wiki/Durchschnittskosteneffekt) |
| <h3 id="cvv">CVV</h3> | Card Verification Value | Teil der Kreditkartennummer zur Überprüfung. Artikel zu [Kreditkarten](/tag/kreditkarte/) |
| <h2 id="d">D</h2> |
| <h3 id="dai">DAI</h3> | - | ERC20-Token der Ethereum-Blockchain dessen Kurs fest bei 1.00 $ liegt. |
| <h3 id="dao">DAO</h3> | Decentralized Autonomous Organization | Dezentralisierte Autonome Organisation. Eine Organisation, die auf Blockhain-Technologie basiert und deren Regeln, wie Stimmen und Stimmrechte darin abgebildet werden. [Siehe Wikipedia](https://de.wikipedia.org/wiki/Dezentralisierte_Autonome_Organisation) |
| <h3 id="dac">DAC</h3> | Decentralized Autonomous Corporation | siehe [DAO](#dao) |
| <h3 id="dapp">DApp</h3> | Decentralized Application | Dezentralisierte Anwendung. Eine Anwendung, die auf DeFi-Systemen basiert. |
| <h3 id="defi">DeFi</h3> | Decentralized Finance | Dezentralisierte Finanzen. Bezeichnet meist blockchain-basierte Systeme welche Smart Contracts unterstützen und das System selbst nicht zentral gesteuert/betrieben wird. Das bekannteste System ist Ethereum. Siehe auch [CeFi](#cefi). |
| <h3 id="deposit">Deposit</h3> | Einzahlung | Das Einzahlen von Fiat oder Kryptowährungen. Siehe auch [Withdraw](#withdraw) |
| <h3 id="dex">DEX</h3> | Decentralized Exchange | Dezentralisierte Online-Börse zum Handeln unterscheidlicher Währungen. Für einen Trade werden hierbei meist die einzelnen User direkt miteinander verbunden. Siehe auch [CEX](#cex). |
| <h3 id="diamond-hands">Diamond Hands</h3> | Diamantenhände | Eine Person, welche bei schlechten Kursen Assets hält und nicht verkauft. Dieser Begriff kann positiv oder negativ interpretiert werden. Positiv: Lang halten und langfristig Gewinne erzielen. Negativ: An einem Asset festhalten und am Ende alles verlieren. Häufig werden für Diamond Hands die Emojis 💎🙌 verwendet. Siehe auch [Paper Hands](#paper-hands) |
| <h3 id="doj">DoJ</h3> | United States Department of Justice | Justizministerium der USA. [Wikipedia "Justizministerium der Vereinigten Staaten"](https://de.wikipedia.org/wiki/Justizministerium_der_Vereinigten_Staaten) |
| <h3 id="double-spending">Double Spending</h3> | Doppelte Ausgabe | Ein möglicher Fehler von Kryptowährungen, bei denen der selbe Token mehrfach ausgegeben werden kann. [Wikipedia "Double-spending" (EN)](https://en.wikipedia.org/wiki/Double-spending) |
| <h2 id="e">E</h2> |
| <h3 id="ebit">EBIT</h3> | Earnings Before Interest and Taxes | Gewinn vor Zinsen und Steuern. Betriebswirtschaftliche Kennzahl zur Beschreibung des operativen Gewinns eines Unternehmens. [Wikipedia "EBIT"](https://de.wikipedia.org/wiki/EBIT) |
| <h3 id="ecb">ECB</h3> | European Central Bank | siehe [EZB](#ezb) |
| <h3 id="ekr">EKR</h3> | Eigenkapitalrentabilität | Kennzahl zur Entwicklung des Eigenkapitals eines Unternehmens. [Wikipedia "Rentabilität - Eigenkapitalrentabilität"](https://de.wikipedia.org/wiki/Rentabilit%C3%A4t#Eigenkapitalrentabilit%C3%A4t) |
| <h3 id="erc20">ERC20</h3> | Ethereum Request for Comment, proposal identifier 20 | Token/Smart-Contract-System der Ethereum-Blockchain. |
| <h3 id="etc">ETC</h3> | Ethereum | Neben Bitcoin eine der wichtigsten Kryptowährungen. <https://ethereum.org/de/> |
| <h3 id="ether">Ether</h3> | - | Währung des Ethereum-Netzwerkes. |
| <h3 id="ezb">EZB</h3> | Europäische Zentralbank | Währungsbehörde der Europäischen Währungsunion und Organ der EU. [Wikipedia "Europäische Zentralbank"](https://de.wikipedia.org/wiki/Europ%C3%A4ische_Zentralbank) |
| <h2 id="f">F</h2> |
| <h3 id="faq">FAQ</h3> | Frequently Asked Questions | Häufig gestellte Fragen. Eine Sammlung von Fragen und Antworten zu bestimmten Themen. |
| <h3 id="fee">Fee</h3> | Gebühr | Die Gebühr, die ein Service für seine Leistung erhebt. |
| <h3 id="fiat">Fiat</h3> | Fiatgeld | Ein Objekt ohne inneren Wert zur Verwendung als Tauschmittel. Bezeichnet Euro, US-Dollar, etc. |
| <h3 id="fomo">FOMO</h3> | Fear of missing out | Die Angst, etwas zu verpassen. [Wikipedia "Fear of missing out"](https://de.wikipedia.org/wiki/Fear_of_missing_out) |
| <h3 id="fud">FUD</h3> | Fear, uncertainty and doubt | Furcht, Ungewissheit und Zweifel. Kommunikationsstrategie zur Streuung falscher Information um Unsicherheit zu bewirken. Im Krypto-Umfeld häufig um Kurse zu beeinflussen. [Wikipedia "Fear, Uncertainty and Doubt"](https://de.wikipedia.org/wiki/Fear,_Uncertainty_and_Doubt) |
| <h2 id="g">G</h2> |
| <h3 id="gas">Gas</h3> | - | Der Preis/Fee einer Transaktion im Ethereum-Netzwerk. |
| <h3 id="gkr">GKR</h3> | Gesamtkapitalrentabilität | Kennzahl über die Effizienz des Kapitaleinsatzes einer Investition. [Wikipedia "Rentabilität - Gesamtkapitalrentabilität"](https://de.wikipedia.org/wiki/Rentabilit%C3%A4t#Gesamtkapitalrentabilit%C3%A4t) |
| <h3 id="gwei">Gwei</h3> | 1 Gigawei | 1 Gwei entspricht 0.000000001 [Ether](#ether) und 1,000,000,000 [Wei](#wei) |
| <h2 id="h">H</h2> |
| <h3 id="hodl">HODL</h3> | Hold | Ein Slangbegriff, der aus einem Tippfehler 2013 entstand. Der Begriff steht für das längere Halten eines Coins, im Gegensatz zum aktiven Trading. [Mehr Informationen hierzu auf Wikipedia](https://en.wikipedia.org/wiki/Hodl) |
| <h2 id="i">I</h2> |
| <h3 id="ico">ICO</h3> | Initial Coin Offering | Die erstmalige Ausgabe eines neuen Coins. Der Begriff ist angelehnt an [IPO](#ipo). |
| <h3 id="ido">IDO</h3> | Initial Dex Offering | Die erstmalige Ausgabe eines Tokens, welcher ein Asset auf einer dezentralen Börse repräsentiert. |
| <h3 id="ieo">IEO</h3> | Initial Exchange Offering | Die erstmalige Ausgabe eines Tokens/Coins über eine Börse, welche als dritte Partei die Assets handelt. |
| <h3 id="ipo">IPO</h3> | Initial Public Offering | Das erstmalige öffentliche Anbieten einer Aktie. |
| <h3 id="ito">ITO</h3> | Initial Token Offering | Die erstmalige Ausgabe eines neuen Tokens. Der Begriff ist angelehnt an [IPO](#ipo). |
| <h2 id="k">K</h2> |
| <h3 id="krypton">Krypton</h3> | Chemisches Element | Chemisches Element mit dem Elementarsymbol Kr und der Ordnungszahl 36. Hat keinen Bezug zu Kryptowährungen. [Wikipedia "Krypton"](https://de.wikipedia.org/wiki/Krypton) |
| <h3 id="kryptonit">Kryptonit</h3> | Fiktives Mineral | Ein fiktives Mineral aus dem DC-Universum. Bekannteste Schwachstelle von Superman. Hat keinen Bezug zu Kryptowährungen. [Wikipedia "Kryptonit"](https://de.wikipedia.org/wiki/Kryptonit) |
| <h3 id="kyc">KYC</h3> | Know Your Customer | Verifizierungsprozess bei einer Kontoerstellung. Meist werden hier Fotos des Personalausweises, sowie ein Nachweis für die aktuelle Adresse gefordert. |
| <h2 id="l">L</h2> |
| <h3 id="leverage">Leverage</h3> | Hebel | Eine Technik, bei dem Fremdkapital zur Steigerung der Eigenkapitalrendite eingesetzt wird, mit der Erwartung, dass die Rentabilität die Leihkonditionen weit übersteigt. [Wikipedia "Leverage-Effekt"](https://de.wikipedia.org/wiki/Leverage-Effekt) |
| <h3 id="lightpaper">Lightpaper</h3> | "Leichtpapier" | Eine kürzere Version eines [Whitepapers](#whitepaper) zur knappen Darstellung eines Projektes. |
| <h3 id="ltv">LTV</h3> | Loan-to-value | Beleihungsauslauf; der Quotient aus Darlehensbetrag und Beleihungswert. [Wikipedia "Beleihungsauslauf"](https://de.wikipedia.org/wiki/Beleihungsauslauf) |
| <h2 id="m">M</h2> |
| <h3 id="maker">Maker</h3> | Macher | Teilnehmer:innen an Börsen, die Liquidität geben und es dadurch anderen Teilnehmer:innen erlauben eine Order sofort auszuführen. Siehe auch [Taker](#taker) und [Wikipedia "Market-Maker"](https://de.wikipedia.org/wiki/Market-Maker) |
| <h3 id="mew">MEW</h3> | MyEtherWallet | Browser-Wallet für Ethereum. <https://www.myetherwallet.com/> |
| <h3 id="mining">Mining</h3> | Schürfen | Bezeichnet den Vorgang des Erzeugens von Coins/Tokens mit dem Einsatz von Ressourcen (wie z.B. Rechenarbeit). |
| <h3 id="minting">Minting</h3> | Prägen | Das Erzeugen von Coins/Tokens ohne den Einsatz von Ressourcen. Der Begriff entstammt dem Prägen von Münzen. |
| <h2 id="n">N</h2> |
| <h3 id="nanoeth">Nanoeth</h3> | - | Siehe [Gwei](#gwei). |
| <h3 id="neobank">Neobank</h3> | Online-Bank | Eine reine Online-Bank ohne physische Zweigstellen. |
| <h3 id="nft">NFT</h3> | Non-Fungible Token | Nicht austauschbare, einzigartige Crypto-Token, z.B. digitale Kunstobjekte. [Wikipedia "Non-Fungible Tokens"](https://de.wikipedia.org/wiki/Non-Fungible_Tokens) |
| <h3 id="node">Node</h3> | Knoten | Ein Knoten eines Netzwerkes. Meist ein Verbindungspunkt für die Datenübertragung, Speicherung oder der Validierung von Transaktionen. |
| <h3 id="nydfs">NYDFS</h3> | New York State Department of Financial Services | siehe [NYSDFS](#nysdfs) |
| <h3 id="nysdfs">NYSDFS</h3> | New York State Department of Financial Services | Amt des US Staates New York zur Regulierung finanzieller Services und Produkte. [Wikipedia "New York State Department of Financial Services"](https://en.wikipedia.org/wiki/New_York_State_Department_of_Financial_Services) |
| <h2 id="o">O</h2> |
| <h3 id="occ">OCC</h3> | Office of the Comptroller of the Currency | Behörde der USA zur Überwachung und Überprüfung des nationalen Kreditwesens. [Wikipedia "Office of the Comptroller of the Currency"](https://de.wikipedia.org/wiki/Office_of_the_Comptroller_of_the_Currency) |
| <h3 id="orderbuch">Orderbuch</h3> | - | Verzeichnis aller Kauf- und Verkaufsorders eines Handelspaares einer Börse. |
| <h3 id="otc">OTC</h3> | Over the Counter | Handel von Aktien/Kryptowährungen/Assets außerhalb der [Orderbücher](#orderbuch) von Börsen. |
| <h2 id="p">P</h2> |
| <h3 id="paper-hands">Paper Hands</h3> | Papierhände | Eine Person, welche bei den ersten Anzeichen von Unsicherheiten ein Asset verkauft. Häufig werden für Paper Hands die Emojis 🧻🤲 verwendet. Siehe auch [Diamond Hands](#diamond-hands) |
| <h3 id="p2p">P2P</h3> | Peer-to-Peer | Bezeichnet die Kommunikation unter gleichgestellten/gleichberechtigten Systemen in Rechnernetzwerken. [Wikipedia "Peer-to-Peer"](https://de.wikipedia.org/wiki/Peer-to-Peer) |
| <h3 id="pos">PoS</h3> | Proof of Stake | Konsensusverfahren zur Wahl des Teilnehmers, der den nächsten Block erstellen darf. [Wikipedia "Proof of Stake"](https://de.wikipedia.org/wiki/Proof_of_Stake) |
| <h3 id="pow">PoW</h3> | Proof of Work | Nachweis, meist in Form von CPU-Arbeit für die Echtheit einer Sache (z.B. einer Transaktion). Ein mögliches Mittel gegen Spam. |
| <h3 id="private-key">Private Key</h3> | Privater Schlüssel | Ein Code/Daten für eine Verschlüsselung, mit dessen Hilfe Signaturen erzeugt und Daten ver- und entschlüsselt werden können. Ein solcher privater Schlüssel wird bei den meisten Kryptowährungen in einer [Wallet](#wallet) verwendet und erlaubt dadurch die Verwaltung der zugehörigen Coins/Tokens. [Wikipedia "Schlüssel (Kryptologie)"](https://de.wikipedia.org/wiki/Schl%C3%BCssel_(Kryptologie)) |
| <h3 id="public-key">Public Key</h3> | Öffentlicher Schlüssel | Ein nicht geheimer Teilschlüssel der asymmetrischen Verschlüsselung. Mit einem öffentlichen Schlüssel können Signaturen geprüft werden (z.B. Downloads) und Daten verschlüsselt aber nicht entschlüsselt werden. [Wikipedia "Schlüssel (Kryptologie)"](https://de.wikipedia.org/wiki/Schl%C3%BCssel_(Kryptologie)) |
| <h2 id="r">R</h2> |
| <h3 id="roa">ROE</h3> | Return on Assets | Gesamtkapitalrentabilität. Siehe [GKR](#gkr). |
| <h3 id="roe">ROE</h3> | Return on Equity | Eigenkapitalrentabilität. Siehe [EKR](#ekr). |
| <h3 id="roi">ROI</h3> | Return on Investment | Betriebswirtschaftliche Kennzahl zur Messung der Rendite. [Wikipedia "Return on Investment"](https://de.wikipedia.org/wiki/Return_on_Investment) |
| <h2 id="s">S</h2> |
| <h3 id="safe">SAFE</h3> | Simple Agreement for Future Equity | Vereinbarung zwischen einem Investor und einer Firma für ein zukünftiges Asset/Kapital der Firma, oft in Form späterer Aktien. [Wikipedia "Simple agreement for future equity (EN)"](https://en.wikipedia.org/wiki/Simple_agreement_for_future_equity) |
| <h3 id="saft">SAFT</h3> | Simple Agreement for Future Tokens | Siehe [SAFE](#safe), mit dem Unterschied, dass dem Investor Tokens nach der abgeschlossenen Entwicklung zugesichert werden. |
| <h3 id="safu">SAFU</h3> | Secure Asset Fund for Users | Eine Notfallreserve um User bei Hacks oder Verlusten Assets zurückzuzahlen. Ein [Backronym](https://de.wikipedia.org/wiki/Backronym) das durch das Video [Funds Are Safu von Bizonacci](https://www.youtube.com/watch?v=DelF6zEHXpE) nach den [Twitter-Posts "Funds are safe"](https://twitter.com/cz_binance) von [Binance](BINANCE_URL)-CEO entstand. Artikel zu [Binance](/tag/binance/) |
| <h3 id="satoshi">Satoshi</h3> | - | Kleinste Bitcoin-Einheit. 1 Satoshi entspricht 0.00000001 [BTC](#btc) |
| <h3 id="shill">Shill</h3> | Anpreisung von Waren/Dienstleistungen | Das Werben für ein Krypto-Projekt in projektfremden Gruppen/Foren, um auf das eigene Projekt aufmerksam zu machen. In unseriösen Gruppen werden häufig die Mitglieder direkt von den Betreibern dazu aufgefordert. |
| <h3 id="smart-contract">Smart Contract</h3> | "Intelligenter" Vertrag | Ein Smart Contract ist ein Vertrag, der über Protokolle und Schnittstellen digital bedient werden kann. [Smart Contract auf Wikipedia](https://de.wikipedia.org/wiki/Smart_Contract) |
| <h3 id="stablecoin">Stablecoin</h3> | - | Kryptowährungen, die den Wert einer bestimmten Währung abbilden und immer konstant halten. Beispiel: 1 USDT entspricht immer $1.00 |
| <h2 id="t">T</h2> |
| <h3 id="testnet">Testnet</h3> | Test-Netzwerk | Ein Blockchain-Netzwerk zu Testzwecken, welche zur Entwicklung und der Vertestung einer Blockchain verwendet wird. |
| <h3 id="taker">Taker</h3> | Nehmer | Teilnehmer:innen an Börsen, die die vorhandenen Orders der [Maker](#maker) sofort ausführen. Siehe auch [Maker](#maker) und [Wikipedia "Market-Maker"](https://de.wikipedia.org/wiki/Market-Maker) |
| <h3 id="tokenomics">Tokenomics</h3> | - | Die Beschreibung des Angebots und der Nachfrage eines Tokens. Häufig als Ergänzung zu oder in einem [Whitepaper](#whitepaper). |
| <h3 id="trading">Trading</h3> | Handel | Das Handeln von Aktien/Fiat/Coins/etc. auf Börsen |
| <h3 id="trc20">TRC20</h3> | - | Technischer Standard für die Verwendung von [Smart Contracts](#smart-contract) auf [TRON](https://tron.network/). |
| <h2 id="u">U</h2> |
| <h3 id="usdt">USDT</h3> | Tether USD | Ein [Stablecoin](#stablecoin), der 1:1 zum Dollarkurs gehandelt wird. Anders als z.B. [BNB](#bnb) ist USDT nicht reguliert. USDT kann u.a. auf [Binance](BINANCE_URL) gehandelt werden. |
| <h3 id="utc">UTC</h3> | Coordinated Universal Time | Die aktuell gültige Weltzeit. [Wikipedia "Koordinierte Weltzeit"](https://de.wikipedia.org/wiki/Koordinierte_Weltzeit) |
| <h2 id="v">V</h2> |
| <h3 id="volatilität">Volatilität</h3> | Schwankung von Zeitreihen | Im Kryptobereich die Kursschwankung einer Währung. [Wikipedia "Volatilität"](https://de.wikipedia.org/wiki/Volatilit%C3%A4t) |
| <h2 id="w">W</h2> |
| <h3 id="wallet">Wallet</h3> | Geldbörse | Eine Software, die es erlaubt, die eigenen Kryptowährungen zu verwalten (Guthaben und Transkationen anzeigen, sowie Transaktionen durchführen) |
| <h3 id="wei">Wei</h3> | - | Die kleinste Einheit des Ethereum-Netzwerkes. Entspricht 0.000000000000000001 [Ether](#ether). Die verwendung von metrischen Vorsätzen, wie Kilo, Mega, Giga sind möglich: Kwei, Mwei, [Gwei](#gwei), Twei und Pwei. |
| <h3 id="whitepaper">Whitepaper</h3> | Weißbuch | Übersicht über Leistungen, Standards und Technik. [Wikipedia "White Paper"](https://de.wikipedia.org/wiki/White_Paper) |
| <h3 id="withdraw">Withdraw</h3> | Auszahlung | Die Auszahlung von Fiat oder Kryptowährungen auf das eigene Konto, einer eigenen Wallet oder zu einer anderen Plattform. Siehe auch [Deposit](#deposit) |

`##Include##: _crypto-disclaimer-de`

`##Tags##: abkürzung abkuerzung definition fachbegriff kryptowährung krypto`
