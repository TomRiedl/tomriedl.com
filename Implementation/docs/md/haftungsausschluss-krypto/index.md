# Haftungsausschluss Kryptowährungen und Investments

Das Handeln mit Kryptowährungen ist immer mit Risiko verbunden. Weitere Informationen hierzu findest du in [einem eigenen Artikel](/version1/risiken-beim-spekulieren-mit-kryptowaehrungen/).

Beachte hierzu auch folgende zusätzlichen Punkte:

1. **Auf dieser Website veröffentlichten Informationen**<br />Die auf dieser Website angegebenen Informationen sind allgemeiner Art und dienen lediglich zur Orientierung. Sind die konkreten Konsequenzen nicht absehbar oder nicht bekannt, solltest du vorher mit einer/m Rechtsanwältin/Rechtsanwalt oder Steuerberater:in sprechen. Nur diese Fachgruppe kann dir rechtlich sichere Auskunft geben. Ich bin weder Rechtsanwalt noch Steuerberater.
2. **Übersetzungen**<br />Manche Artikel sind in anderen Sprachen verfügbar (z.B. Englisch oder Chinesisch). Sollte es einen Widerspruch zwischen dem deutschen und dem übersetztem Artikel geben, hat der deutsche Artikel Vorrang.
3. **Spekulationsrisiken/Investitionsrisiken**<br />Das Erwerben von Kryptowährungen kann zu einem Totalverlust führen, wobei der Zeitraum nicht absehbar ist. Ebenfalls sind große Preisschwankungen nicht ungewöhnlich. Die Informationen auf dieser Website können nicht dazu führen, dass du kein Geld verlierst.
4. **Einhaltung von Steuerpflichten**<br />Du bist selbst dafür verantwortlich, ob dein Handeln steuerrechtliche Konsequenzen nach sich zieht. Spreche hierüber bitte mit deiner/deinem Steuerberater:in.