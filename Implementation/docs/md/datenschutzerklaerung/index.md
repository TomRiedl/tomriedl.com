# Datenschutzerklärung

`##Description##: Datenschutzerklärung von tomriedl.com`
`##Type##: website`
`##Locale##: de-DE`

## Datenschutz

Ich nehme den Schutz deiner persönlichen Daten sehr ernst. Ich behandle deine personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung.
Die Nutzung meiner Website ist ohne Angabe personenbezogener Daten möglich.

 
## Cookies

Diese Website verwendet keine Cookies.


## Server-Standort und Server-Log-Files

Ich verwende ausschließlich Hosting-Provider mit Sitz in Deutschland und Serverstandorten in Deutschland. Diese sind verpflichtet sich an die Datenschutz-Grundverordnung zu halten.

Die verwendeten Server erzeugen Log-Dateien der Zugriffe. Diese Log-Dateien können für rudimentäre Statistiken verwendet werden. Personenbezogene Daten sind hierbei nicht enthalten.

 
## E-Mails

Aufgrund der Archivierungspflicht der Grund­sät­ze zur ord­nungs­mä­ßi­gen Füh­rung und Auf­be­wah­rung von Bü­chern, Auf­zeich­nun­gen und Un­ter­la­gen in elek­tro­ni­scher Form so­wie zum Da­ten­zu­griff (GoBD) habe ich mein Unternehmen CyanCor GmbH mit der Archivierung der E-Mails beauftragt (siehe Anlage 1). CyanCor GmbH archiviert die E-Mails auf deutschen Servern und gibt diese Daten an keine weitere Partei weiter.

 
## Telegram

Ich biete meine Hilfe über Telegram-Channels und -Gruppen an. Du kannst diesen Service nur nutzen, wenn du einen Account bei Telegram erstellst. Bei der Nutzung von Telegram musst du deren Datenschutzerklärung und AGBs annehmen. Die geschriebenen Nachrichten in einem meiner Telegram-Channels sind für alle Mitglieder des Kanals sichtbar. Je nach Telegram-Einstellung ist es auch möglich, dass andere Telegram-Nutzer deinen echten Namen, Benutzernamen und eventuell auch deine Mobilnummer sehen können. Informiere dich bitte vor der Nutzung von Telegram über deren Datenschutzbestimmungen: <https://telegram.org/>

 
## Auftragsverarbeitung

Für das Hosting der Website und E-Mails, sowie für die gesetzliche Archivierung der E-Mails werden Unterauftragsverarbeiter eingesetzt. Eine Liste der Unterauftragsverarbeiter findest du in Anlage 1.

 
## Recht auf Auskunft, Löschung, Sperrung

Du hast jederzeit das Recht auf unentgeltliche Auskunft über deine gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung sowie ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten kannst du jederzeit unter der im Impressum angegebenen Adresse an mich wenden.

Es gilt zu beachten, dass die Kommunikation über E-Mail (und somit auch das Kontaktformular) der Grund­sät­ze zur ord­nungs­mä­ßi­gen Füh­rung und Auf­be­wah­rung von Bü­chern, Auf­zeich­nun­gen und Un­ter­la­gen in elek­tro­ni­scher Form so­wie zum Da­ten­zu­griff (GoBD) unterliegt. Es kann daher sein, dass E-Mails nicht gelöscht werden dürfen.

 
## Widerspruch Werbe-Mails

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit widersprochen. Der Betreiber der Seite behält sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch E-Mails, vor.

 
## Anlage 1

Liste der Unterauftragsverarbeiter nach Art. 28 Abs. 1 DSGVO.

| Nummer | Firma | Kontakt | Leistung |
| --- | --- | --- | --- |
| 1 | CyanCor GmbH | Innstr. 71, 83022 Rosenheim, Germany<br /><https://cyancor.com/> | - Archivierung der tomriedl.com-E-Mails nach der gesetzlichen Archivierungspflicht<br />-Website-Backup |
| 2 | Serverprofis GmbH | Mondstr. 2-4, 85622 Feldkirchen, Germany<br /><https://service.serverprofis.net/aff.php?aff=846> | - Hosting der Website<br />- Hosting der E-Mails |

`##Tags##: datenschutzerklärung dsgvo gdpr`