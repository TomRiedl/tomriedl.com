# Binance Referral Code – Affiliate Link

`##Description##: Get 20% commission, Tutorial how to create own binance referral link/affiliate code`
`##Type##: article`
`##Locale##: en-US`

`##Picture##:1600:435:/wp-content/uploads/2017/12/binance-referral-code:png:Create binance referral code - affiliate link`

[Binance](BINANCE_EN_URL) offers 20% commission for all invited friends. The commission is paid in the currency the user trades. It is also possible to specify a commission kickback rate to share your part with your friend.

If you don't have a [Binance account](BINANCE_EN_URL) and register through my link, you'll get access to my [VIP group](/en/support/vip-group/) ([more information here](/en/support/vip-group/)):<br />[BINANCE_EN_URL](BINANCE_EN_URL)

## How to

1. Log in to [Binance](BINANCE_EN_URL).
2. Select the account symbol in the menu and click **Referral**.<br />`##Picture##:460:478:tutorial-binance-referral-link-menu:webp:binance referral link affiliate program`
3. Click **Generate your link**<br />`##Picture##:523:181:generate-your-referral-link:webp:Generate your referral link`
4. On the shown popup set your desired kickback rate and press **Generate your link**.<br />`##Picture##:398:492:binance-affiliate-kickback-rate-configuration:webp:binance affiliate link friend share`
5. Share your link with your friends.

<!-- ##Tags##: affiliate binance code program referral affiliate-system friend -->

<ul class="LanguageSelection">
    <li><a href="/version1/binance-referral-code-affiliate-link-wo-finde-ich-ihn/">Deutsch</a></li>
    <li><a href="/zh/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81/%E5%B8%81%E5%AE%89/%E5%B8%81%E5%AE%89-%E6%8E%A8%E8%8D%90-%E4%BB%A3%E7%A0%81-%E5%BE%81%E9%9B%86-%E9%93%BE%E6%8E%A5/">中文</a></li>
</ul>

`##Translation##:de:https://##Domain##/version1/binance-referral-code-affiliate-link-wo-finde-ich-ihn/`
`##Translation##:en:https://##Domain##/en/crypto/binance/binance-referral-code-affiliate-link/`
`##Translation##:zh-Hans:https://##Domain##/zh/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81/%E5%B8%81%E5%AE%89/%E5%B8%81%E5%AE%89-%E6%8E%A8%E8%8D%90-%E4%BB%A3%E7%A0%81-%E5%BE%81%E9%9B%86-%E9%93%BE%E6%8E%A5/`
