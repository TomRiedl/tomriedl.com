# VIP group with personal support

`##Description##: Support for trading, cryptocurrencies, wallets, programming and software`
`##Type##: article`
`##Locale##: en-US`

![VIP group with personal support](cover-tomriedl-vip.png)

If you register through my referral link you'll get access to my VIP group (English and German). In the VIP group you'll get much more support than in the public Telegram group.

To get access proceed as follows:
1. Register through a referral link:<br />&bull; **Binance**: [BINANCE_EN_URL](BINANCE_EN_URL)<br />&bull; **Liquid**: [LIQUID_URL](LIQUID_URL)
2. After successful registration write a short mail to the [e-mail in my imprint](/impressum/). The following information is mandatory:<br />&nbsp;&nbsp;&nbsp;&nbsp;&bull; For Binance your Binance-ID and for Liquid your e-mail<br />&nbsp;&nbsp;&nbsp;&nbsp;&bull; Your Telegram username or your mobile phone number
3. Afterwards I'll add you to the group.

➡️ More articles: [Binance](/tag/binance/), [Liquid](/tag/liquid/)

<!-- ##Tags##: vip group support -->

<ul class="LanguageSelection">
    <li><a href="/support/vip-gruppe/">Deutsch</a></li>
</ul>

`##Translation##:de:https://##Domain##/support/vip-gruppe/`
