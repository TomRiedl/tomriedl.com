# IOTA Trading auf Bitfinex

`##Description##: IOTA auf Bitfinex erwerben`
`##Type##: article`
`##Locale##: de-DE`

![IOTA Trading auf Bitfinex](/wp-content/uploads/2017/06/iota-trading-announcement.jpeg)

Ab morgen, den 13. Juni 2017 kann endlich IOTA auf [Bitfinex](BITFINEX_URL) gehandelt werden. Der Start ist 15:00 Uhr lokale Zeit (13:00 UTC).

Mehr Infos findet ihr auf dem [offiziellen Blog-Post von Bitfinex](http://blog.bitfinex.com/uncategorized/iota-launch/).

`Veröffentlicht 2017-06-12, Autor: Tom Riedl`

`##Include##: _crypto-disclaimer-de`

```##Tags##: iota trading handel bitfinex kryptobörse börse krypto```
