# IOTA – Erste Hilfe, Fragen und Antworten (FAQ)

`##Description##: Häufige Fragen und Antworten zu IOTA`
`##Type##: article`
`##Locale##: de-DE`

![IOTA – Erste Hilfe, Fragen und Antworten (FAQ)](/wp-content/uploads/2017/12/iota-faq.png)

Auf dieser Seite findest du Antworten und Hilfe zu bestimmten Fehlern der IOTA Wallet oder ganz allgemein zu IOTA. Solltest du einen Fehler oder eine Frage vermissen, dann [erstelle hierzu einfach ein Issue](https://gitlab.com/TomRiedl/tomriedl.com/-/issues).

Weitere Informationen findest du unter <https://www.iota-wiki.com/de/>

Zum Suchen in diesem Dokument benutze die Suchfunktion deines Browsers (z.B. mit Strg+F).

Hinweis 2020-12-31: Die Einträge zur alten Light-Wallet wurden entfernt.

## Transaktionen

### Transaktion dauert lange (im Status Pending/Ausstehend)

Transaktionen (Überweisungen) von IOTA können momentan noch langsam sein. Der Grund hierfür liegt in der IOTA-Architektur. Transaktionen werden schneller, je mehr IOTA genutzt wird und je mehr Nodes es gibt. In Zukunft wird es also schneller.
Die genauen Gründe einer langsamen Transaktion (lange im Status Pending oder Ausstehend) sind:

- Wenige Transaktionen im gesamten Netzwerk. Je mehr Transaktionen stattfinden, desto häufiger wird ein PoW (Proof of Work) durchgeführt. Die Chance, dass deine Transaktion durch ein PoW bestätigt wird steigt somit.
- Aktuelle Tangle Topologie. Damit das IOTA-Netzwerk funktioniert, werden Nodes (Full Nodes) benötigt. Von den Full Nodes gibt es momentan noch nicht so viele. Je mehr Nodes in Zukunft dem Netzwerk hinzugefügt werden, desto schneller und reibungsloser funktionieren Transaktionen.
- Deine Position in der Tangle. Wenn du von deinem Cluster eine Transaktion auf eine Adresse in einem anderen Cluster machen willst, kann das momentan eine sehr lange Zeit dauern. Sendest du auf eine Adresse im selben Cluster vergehen nur wenige Sekunden.

**Können IOTA dadurch verloren gehen?**
- Nein, deine IOTA bleiben so lange auf deiner Adresse bis die Transaktion betätigt wurde.

**Was kann ich tun?**
- Verwende die Möglichkeit des [Promotes](#promote) oder [Reattach](#reattach). Klicke nach ca. 30 Minuten auf den Promote/Reattach-Button. Wiederhole das mehrere Male.
- Überprüfe die Vorangegangenen Transaktionen. Sind davon welche unbestätigt, versuche diese zuerst durch ein Reattach zu bestätigen.
- Wähle eine andere Node aus. Die Einstellung dazu findest du unter Tools -> Edit Node configuration oder Werkzeuge -> Konfiguration der Node bearbeiten

Quelle: <https://matthewwinstonjohnson.gitbooks.io/iota-guide-and-faq/getting-started/why-is-my-transaction-pending-for-so-long.html>

### Muss eine Adresse confirmed/bestätigt sein, damit ich IOTA empfangen kann
Nein, direkt nachdem du eine neue Adresse erzeugt hast, kannst du diese verwenden. Es ist nicht nötig, dass die Adresse bestätigt wird.

### Bei der Seed vertippt und somit an die falsche Adresse gesendet
Solltest du dich bei der Seed vertippt haben, gibt es hierfür ein Tool, mit dem verschiedene ähnliche Seeds geprüft werden: <https://github.com/FVANtom/find-typo-in-iota-seed>


## Wallet

### Sind die IOTA verloren, wenn ich die Wallet deinstalliere
Nein, du kannst die Wallet jederzeit von deinem PC oder Mobilgerät entfernen. Die IOTA sind nicht in der Wallet gespeichert, sondern in der Tangle. Solange du im Besitz deiner Seed bist, kannst du von überall darauf zugreifen, auch nach einer Neuinstallation der Wallet. 

### Kontostand ist 0
Ursache:
- Der Kontostand wird nicht korrekt angezeigt
- Es sind keine IOTA in diesem Konto vorhanden

Lösung:
- Gehe diese Anleitung Schritt für Schritt durch: <https://forum.helloiota.com/588/Help-My-IOTA-balance-is-zero-steps-to-help-you-find-your-balance-v252>
- Überprüfe deine Seed und die Hashes deiner Transaktionen. Benutze hierfür u.A. <https://iotasear.ch/>

### Unterschied zwischen CCurl und WebGL 2 Curl
Bei CCurl wird der [Proof of Work (PoW)](#proof-of-work) von der CPU übernommen und bei WebGL 2 Curl von der Grafikkarte. Für gewöhnlich ist bei den meisten Rechnern (ausgenommen Laptops) WebGL schneller. Manche haben allerdings bei WebGL das Problem, dass die Wallet Invalid Transaction Hash anzeigt. Sollte das bei dir so sein, verwende CCurl oder stelle sicher, dass die Wallet die richtige Grafikkarte verwendet (Anleitung dazu folgt demnächst). 

## Glossar

### Promote
Kurz: Promote verleiht deiner Transaktion mehr Gewicht und die Wahrscheinlichkeit, dass die Transaktion bestätigt wird steigt.
Ausführlich: Beim Promote wird eine 0-IOTA-Transaktion erstellt, die sowohl deine Transaktion, wie auch den aktuellen Milestone bestätigt. Hierbei wird, ausgehend von deiner Ursprungstransaktion, sowie dem Milestone ein MCMC-Walk ([Markov-Chain-Monte-Carlo](https://de.wikipedia.org/wiki/MCMC-Verfahren)) durchgeführt und die dabei ausgewählten Tips als Tips für die neu erstellte Transaktion verwendet. Auf diese Art wird das Gewicht der eigentlichen Transaktion erhöht und dadurch die Wahrscheinlichkeit erhöht, dass deine Transaktion bestätigt wird.

### Reattach
Beim Reattach wird eine Transaktion an einer anderen Stelle der Tangle angefügt und der Proof of Work (PoW) erneut durchgeführt. Sollte eine Transaktion zu lange im Status Pending/Ausstehend sein, kannst du nach ca. 30 Minuten einen Reattach durchführen und dies ggf. wiederholen.

Der Reattach wurde seit der Light Wallet 2.5.5 durch Promote ersetzt.

### Rebroadcast
Beim Rebroadcast wird eine Transaktion erneut an alle Nachbar-Nodes übertragen. Für gewöhnlich ist dieser Vorgang nicht notwendig. Solltest du jedoch nicht sicher sein, ob die Nachbar-Nodes die Transaktion erhalten haben, kannst du einen Rebroadcast starten. Sollte deine Transaktion länger im Status Pending/Ausstehend sein, dann ist Reattach der richtige Weg.

### Proof of Work
Nachdem deine Transaktion erstellt, signiert und zwei zufällige andere Transaktionen zu deinem Transaktionsbundle hinzugefügt wurden, muss die Proof of Work durchgeführt werden. Diese PoW ist Arbeit, die dein Rechner übernimmt um zu bestätigen, dass es sich um eine wahre Transaktion handelt. Nur mit dieser PoW kann eine Transaktion in der Tangle akzeptiert werden. Dieser Schritt verhindert Spam und Hacks.

`##Include##: _crypto-disclaimer-de`

```##Tags##: iota erste-hilfe faq frage antwort krypto```
