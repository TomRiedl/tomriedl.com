# IOTA Wallet „No Connection / Verbindung verweigert / Keine Verbindung“

`##Description##: Problemlösung bei keiner Verbindung in der IOTA Wallet`
`##Type##: article`
`##Locale##: de-DE`

Zeigt die IOTA Wallet „No Connection“, „Connection refused“, „Verbindung verweigert“ oder „Keine Verbindung“ an, gibt es für gewöhnlich folgende Ursachen:

1. Du hast die alte Wallet installiert - verwende die [Trinity Wallet](https://trinity.iota.org/)
2. Die Wallet ist veraltet - auf [dieser Seite siehst du die aktuelleste Versionsnummer](https://github.com/iotaledger/trinity-wallet/releases/)
3. Die Node ist ungültig, wähle über das Menü "Trinity -> Optionen -> Node" den Punkt "Automatische Node-Verwaltung"

[➡️ Weitere Artikel zu IOTA](/tag/iota/)

`##Include##: _crypto-disclaimer-de`

```##Tags##: iota verbindung krypto```
