# Two-Factor Authentication / Multi-Factor Authentication (TFA, 2FA, MFA, TOTP)

`##Description##: Was ist und wie funktioniert eine Authentifizierung über mehrere Faktoren`
`##Type##: article`
`##Locale##: de-DE`

![Two-Factor Authentication / Multi-Factor Authentication (TFA, 2FA, MFA, TOTP)](/wp-content/uploads/2017/12/tfa.jpg)

Die Two-Factor Authentication (TFA, 2FA) oder Zwei-Faktor-Authentifizierung ist eine Form der Multi-Factor Authentication (MFA), bei der zwei der drei Faktoren verwendet werden.<br />
Die möglichen Faktoren sind:
- **Wissen** (etwas, da man weiß)<br />Beispiel: Ein Passwort
- **Besitz** (etwas, das man hat)<br />Beispiel: Eine EC-Karte für den TAN-Generator
- **Inhärenz** (etwas, das man ist)<br />Beispiel: Fingerabdruck, DNA

Je mehr Faktoren zum Schutz eines Systems verwendet werden, desto sicherer wird es. Eine Plattform, die nur über einen Benutzernamen oder Passwort geschützt ist, verwendet lediglich einen Faktor (Wissen). Gerät diese Information an eine andere Person, so hat diese vollständigen Zugriff auf das dahinterliegende System. 

Folglich ist es sinnvoll und wichtig, jegliches System durch möglichst viele Faktoren zu schützen, um eine Kompromittierung zu verhindern. 

Viele Plattformen bieten deshalb die Two-Factor Authentication in Form der Time-Based One-Time-Passwörter (TOTP, zeitbasierte Einmalpasswörter) an. TOTP ist ein standardisiertes Verfahren nach [RFC 6238](https://tools.ietf.org/html/rfc6238), welches auf Keyed-Hash Message Authentication Code (HMAC, [RFC 2104](https://tools.ietf.org/html/rfc2104)) basiert und von [Initiative For Open Authentication (OATH)](https://openauthentication.org/) entwickelt wurde. TOTP zählt zum Faktor *Besitz*.

Für die Einmalpasswörter wird eine Software (in Form einer mobilen App oder einer Anwendung) benötigt, die diese Codes erzeugt. Häufig wird hierfür der Google Authenticator verwendet. Da es sich bei TFA um einen offenen Standard handelt, gibt es viele Alternativen. Gute Alternativen sind z.B. für Android [FreeOTP](https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp) und für PC das [KeePass](https://keepass.info/)-Plugin [KeeOtp](https://keepass.info/plugins.html#keeotp). 

Die Verwendung ist sehr einfach und folgt für gewöhnlich folgendem Muster:

1. Konto auf einer Online-Plattform erstellen
2. Nach dem ersten Login die TFA-Erstellung starten
3. Es wird ein [QR-Code](https://de.wikipedia.org/wiki/QR-Code) angezeigt und auch häufig der darin enthaltene Code
4. Den QR-Code mit der Authenticator scannen oder den Code in der TOTP-Software speichern
5. Nun zeigt der Authenticator einen 6-stelligen Zahlencode an (welcher sich alle 30 Sekunden ändert)
6. Dieser Code wird zur Bestätigung in das vorgesehene Feld eingetragen
7. Anschließend ist die TFA aktiviert
8. Beim nächsten Login muss neben Benutzername und Passwort der TFA-Code eingegeben werden


## Gegen Verlust schützen
Verliert man das Gerät, auf dem die 2FA-Codes generiert werden, ist der Zugriff auf das jeweilige Konto nicht mehr möglich. Manche Plattformen bieten eine Wiederherstellung über E-Mail oder SMS an, aber nicht alle. 

Um sich vor einem Verlust zu schützen, sollten die Schlüssel für die Two-Factor Authentication beim Erstellen gesichert werden. Wähle dazu eine oder mehrere Möglichkeiten:
- Der angezeigte QR-Code ist häufig eine Bilddatei. Dieser kann somit problemlos gespeichert oder ausgedruckt werden. Sollte der Authenticator-Zugang verloren sein, kann man jederzeit auf einem anderen Gerät den QR-Code erneut scannen.
- Neben dem QR-Code steht häufig eine Zeichenkombination aus Buchstaben und Zahlen. Hierbei handelt es sich um den Schlüssel für die Generierung der TOTPs. Dieser Code wird z.B. für das [KeePass](https://keepass.info/)-Plugin [KeeOtp](https://keepass.info/plugins.html#keeotp) verwendet. Zur Sicherung kann dieser Code gespeichert oder ausgedruckt werden.


## Fazit
Nutze immer eine Authentifizierung mit mehreren Faktoren, um deine Zugänge zu schützen. Die 2FA mit einem Authenticator ist hierfür eine einfache und schnell umzusetzende Möglichkeit.


*Titelbild von [George Becker](https://stocksnap.io/author/31938)*

```##Tags##: authenticator mfa schutz sicherheit tfa totp two-factor authentication verlust```
