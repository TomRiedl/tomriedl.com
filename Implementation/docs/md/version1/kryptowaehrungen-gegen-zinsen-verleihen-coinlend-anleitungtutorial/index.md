# Kryptowährungen gegen Zinsen verleihen – Coinlend Anleitung/Tutorial

`##Description##: Zinsen für Krypto-Assets erhalten, eine Anleitung`
`##Type##: article`
`##Locale##: de-DE`

![Kryptowährungen gegen Zinsen verleihen – Coinlend Anleitung Tutorial](/wp-content/uploads/2017/08/StockSnap_G3YOGRBLF3.jpg)

Wer sich Kryptowährungen zurücklegt, für den Fall, dass sie später mehr wert sind, lässt diese meist in einem Wallet liegen. Besser ist es natürlich, wenn man hierfür Zinsen bekommt.

Das funktioniert auf einigen Trading-Plattformen, wie [Bitfinex](BITFINEX_URL) oder [Poloniex](POLONIEX_URL). Hierbei verleiht man seine Coins/Token an andere Trader, welche mit diesen handeln können. Beim Verleihen haben wir dabei ein geringes Risiko, da der Plattformbetreiber in jedem Fall (egal ob der Leiher Gewinn oder Verlust macht) die Leihsumme + Zinsen zurückzahlt. Ein geringes Risiko besteht nur, sollte die Trading-Plattform [gehackt oder unerwartet geschlossen](/version1/risiken-beim-spekulieren-mit-kryptowaehrungen/) werden.

Da das manuelle Verleihen mit den besten Zinsen sehr aufwändig ist, haben deutsche Entwickler den [CoinLend-Bot](COINLEND_URL) entwickelt ([Android-/iOS-App](/version1/coinlend-app/)). Der Bot spricht mit den Trading-Plattformen über eine API (Schnittstelle), welche dem Bot nur die nötigen Rechte zum Verleihen gibt. Die CoinLend-Betreiber können somit keine der Währungen auszahlen und ist damit vollkommen sicher.

Steuerrechtlicher Hinweis: Bitte kläre mit deinem Steuerberater, ob [§23 EStG](https://www.gesetze-im-internet.de/estg/__23.html), Abs. 1, Nummer 2, Satz 4 Verwendung findet („Bei Wirtschaftsgütern im Sinne von Satz 1, aus deren Nutzung als Einkunftsquelle zumindest in einem Kalenderjahr Einkünfte erzielt werden, erhöht sich der Zeitraum auf zehn Jahre“). Ich selbst darf hierüber keine Auskunft geben, da ich kein Steuerberater bin.

Und hier nun die Anleitung, wie man Coinlend einrichtet, am Beispiel von [Bitfinex](BITFINEX_URL) ([Poloniex](POLONIEX_URL) funktioniert ähnlich).

## Anleitung

1. Bei [Bitfinex](BITFINEX_URL) oder [Poloniex](POLONIEX_URL) registrieren (Die Anleitung ist für Bitfinex, funktioniert aber nahezu identisch bei Poloniex).
2. [coinlend.org](COINLEND_URL) öffnen und auf **Account** oder **Bot einrichten** klicken.<br />![coinlend tutorial registrieren](/wp-content/uploads/2017/08/coinlend-tutorial-01-registrieren.png)
3. Auf **Registrieren** klicken.<br />![coinlend tutorial registrieren](/wp-content/uploads/2017/08/coinlend-tutorial-02-registrieren.png)
4. E-Mail und Passwort eingeben und anschließend auf **Registrieren** klicken.<br />![coinlend tutorial registrieren e-mail passwort](/wp-content/uploads/2017/08/coinlend-tutorial-03-registrieren.png)
5. E-Mail-Postfach öffnen und die E-Mail bestätigen.<br />![coinlend tutorial register e-mail bestätigen](/wp-content/uploads/2017/08/coinlend-tutorial-04-mail-best%C3%A4tigen.png)
6. Im Menü auf **Account** klicken und mit E-Mail und Passwort anmelden.<br />![coinlend anleitung einloggen](/wp-content/uploads/2017/08/coinlend-tutorial-05-login.png)
7. [Bitfinex](BITFINEX_URL) öffnen, im Menü Wallets auswählen und die gewünschten Beträge von Exchange/Margin nach Funding verschieben.<br />![coinlend bitfinex wallet exchange margin funding](/wp-content/uploads/2017/08/coinlend-tutorial-07-bitfinex-wallet.png)
8. Im Profil-Menü den Punkt **API** auswählen.<br />![coinlend bitfinex api code](/wp-content/uploads/2017/08/coinlend-tutorial-06-bitfinex-api.png)
9. Den Bereich **Create New Key** öffnen und die Berechtigungen, wie im Bild angezeigt aktivieren. Anschließend als Label "coinlend" eingeben und auf **Generate API key** klicken.<br />![coinlend bitfinex anleitung api key erstellen](/wp-content/uploads/2017/08/coinlend-tutorial-08-api-key-erstellen.png)<br />*Aktiviert sind: Account History (read), Margin Funding (read, write), Wallets (read)*
10. Den angezeigten **API key** und **API key secret** für später speichern.<br />![coinlend bitfinex api key speichern tutorial](/wp-content/uploads/2017/08/coinlend-tutorial-08-api-key-speichern.png)
11. Zu [coinlend.org](COINLEND_URL) wechseln und die beiden Codes in den vorgesehenen Feldern bei **Bitfinex** eintragen. Mit Klick auf **Speichern** bestätigen.<br />![coinlend api keys eintragen anleitung](/wp-content/uploads/2017/08/coinlend-tutorial-09-api-key-eintragen.png)
12. Unter **Meine Zinsen** sieht man alle verfügbaren Währungen und die Zinsen der letzten 30 Tage. Bei **Meine Loans** die aktiven Verleihvorgänge. Ab jetzt arbeitet der Bot von alleine.<br />
Um weitere Währungen für das Trading zu aktivieren, müssen diese in Bitfinex nur in das Funding-Wallet verschoben werden.<br />![coinlend anleitung übersicht loans](/wp-content/uploads/2017/08/coinlend-tutorial-10-%C3%BCbersicht.png)

[Weitere Artikel zu coinlend](/tag/coinlend/)

`##Include##: _crypto-disclaimer-de`

```##Tags##: anleitung tutorial coinlend lending verleih kryptowährung bitfinex poloniex krypto```
