# Anleitung für neues Bitfinex Affiliate/Referral-System

`##Description##: Affiliate Link erstellen, Erklärung Referral-System`
`##Type##: article`
`##Locale##: de-DE`

![Anleitung für neues Bitfinex Affiliate/Referral-System](/wp-content/uploads/2019/10/bitfinex-affiliate-programm-anleitung.png)

Seit über einem Jahr war das [Affiliate-System von Bitfinex](BITFINEX_URL) deaktiviert und wurde nun als neues System 2019-10-17 um 12:00 Uhr neu freigegeben.

In einer E-Mail, die an alle bestehenden Affiliate-Partner gesendet wurde, kündigten sie es als Evolution bzw. Revolution an.

> […] In the brainstorming process, we revisited our old affiliation system and also evaluated other affiliate programs to formulate the best plan. We knew that we wanted to revolutionize the existing idea of what a crypto community represents. […]

Das System ist wirklich gelungen und auch sehr übersichtlich aufgebaut.<br />
Das dreistufige System verteilt die Prozente wie folgt: 1. Level 18%, 2. Level 6% und 3. Level 2%. Sind die Referrals verifiziert, gibt es 20% mehr, also 1. Level 21.6%, 2. Level 7.2% und 3. Level 2.4%.

## Anleitung

1. [Bei Bitfinex anmelden.](BITFINEX_URL)
2. Über das Menü die Account-Seite öffnen.<br />![bitfinex menü account konto seite](bitfinex-referral-menu-account-konto.png)
3. Die Referral-Seite über das linke Menü öffnen (Klick auf * Affiliate*).<br />![bitfinex affiliate menüpunkt referral center](affiliate-seite-account-menue.png)
4. Rechts oben auf „Generate new code“ klicken.<br />![Bitfinex Referral Code erzeugen](/wp-content/uploads/2019/10/bitfinex-referral-code-erzeugen.png)
5. Einen beliebigen Namen vergeben (dieser wird später in der Liste angezeigt und hilft dir, deine Codes besser zu verwalten).<br />![Bitfinex Affiliate Code Name vergeben](/wp-content/uploads/2019/10/bitfinex-affiliate-code-name-vergeben.png)
6. Der Affiliate-Link wird generiert und kann nun kopiert und verteilt werden.<br />Der Link hat folgendes Format: [BITFINEX_URL](BITFINEX_URL)<br />![Bitfinex Referral Link Code](/wp-content/uploads/2019/10/bitfinex-referral-link-code.png)
7. In der linken Leiste hast du die Möglichkeit, die Übersicht zu öffnen (1), dein Netzwerk anzuzeigen (2) und deine Links bzw. Codes zu verwalten (3).<br />![Bitfinex Affiliate Menu Dashboard Network Links Codes](/wp-content/uploads/2019/10/bitfinex-affiliate-menu-dashboard-network-links.png)

`##Include##: _crypto-disclaimer-de`

```##Tags##: affiliate bitfinex code partner partnerprogramm program programm referral system referral-system werben krypto```
