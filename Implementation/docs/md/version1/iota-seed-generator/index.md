# IOTA Seed erzeugen / IOTA-Seed-Generator

`##Description##: Eine sichere Seed für IOTA erstellen`
`##Type##: article`
`##Locale##: de-DE`

![IOTA Seed-Generator](/wp-content/uploads/2017/06/iota-seed-generator.png)

Die IOTA-Seed muss 81 Zeichen lang sein und darf nur die Zeichen A-Z und die Ziffer 9 enthalten.

Kann ich einen Online-Seed-Generator verwenden? **Nein.**
Warum nicht? Niemand kann garantieren, dass die Online-Seite die Seed nicht an andere Personen sendet. Auch wenn die Entwickler ehrliche Entwickler sind, können sie nicht garantieren, dass ihre Seite nicht gehackt wurde.

Um 100% sicher zu gehen, solltest du einen Seed-Generator lokal auf deinem Rechner ausführen (idealerweise ohne Internet).

Folgende Möglichkeiten (bis auf die letzte) kann ich empfehlen. Ich habe sie nach Sicherheit absteigend sortiert:
- [IOTA Seed-Generator](#iota-seed-generator)
  - [Generierung der Seed mit der Konsole](#generierung-der-seed-mit-der-konsole)
    - [Linux](#linux)
    - [MacOS](#macos)
    - [Windows](#windows)
  - [Generierung der Seed mit KeePass](#generierung-der-seed-mit-keepass)
  - [Generierung der Seed mit Docker](#generierung-der-seed-mit-docker)
  - [Generierung der Seed über einen Download](#generierung-der-seed-über-einen-download)
  - [Seed selbst wählen](#seed-selbst-wählen)


## Generierung der Seed mit der Konsole

### Linux

Konsole (Terminal, bash, sh, …) öffnen und folgendes Kommando ausführen:

```bash
cat /dev/urandom | tr -dc A-Z9 | head -c${1:-81}
```

### MacOS

Terminal öffnen und folgendes Kommando ausführen:
```bash
cat /dev/urandom | LC_ALL=C tr -dc 'A-Z9' | fold -w 81 | head -n 1
```

Das Terminal öffnest du folgendermaßen:

- Öffne die Spotlight-Suche (Tastenkürzel: ⌘ + Leertaste)
- Suche nach ‚terminal‘
- Kopiere das oben angezeigte Kommando und drücke anschließend Return

### Windows

Windows hat die Kommandozeile (cmd) und die PowerShell, beides leider ohne kryptografisch sicheren Zufallsgenerator. Du kannst dennoch unter Windows eine Seed in der Konsole generieren, in dem du [Git Bash](https://git-for-windows.github.io/) (empfohlen, da unkomplizierter) oder [Cygwin](https://cygwin.com/install.html) verwendest. Lade dir eines der beiden herunter und starte die Bash-Emulation durch drücken der Windows-Taste und das anschließende Eingeben von ‚bash‘ oder ‚cygwin‘.
![Git Bash Windows](/wp-content/uploads/2017/06/git-bash-windows-search-300x142.png)

In der nun geöffneten Konsole gibst du folgendes Kommando ein und drückst Enter:
```bash
cat /dev/urandom | LC_ALL=C tr -dc 'A-Z9' | fold -w 81 | head -n 1
```
![IOTA Seed Windows Bash](/wp-content/uploads/2017/06/git-bash-iota-seed-generator.png)

## Generierung der Seed mit KeePass
1. KeePass herunterladen
2. KeePass starten
3. Im Menü Extras -> Passwort generieren… (Tools -> Generate Password…) öffnen
4. Folgende Einstellungen verwenden:<br />
    Länge des Passworts: 81<br />
    [✔] Großbuchstaben<br />
    Zusätzliche Zeichen: 9<br />
    ![IOTA Seed Generator KeePass](/wp-content/uploads/2017/06/iota-seed-generator-keepass.png)
5. Die erstellte Seed in KeePass speichern bevor du sie zum Öffnen einer Wallet verwendest. Damit verhinderst du, dass die Seed verloren geht (z.B. bei einem Stromausfall oder Absturz).

## Generierung der Seed mit Docker
1. Docker installieren: Docker für [Windows](https://docs.docker.com/docker-for-windows/install/), [Ubuntu](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/), [Debian](https://docs.docker.com/engine/installation/linux/docker-ce/debian/), [MacOS](https://docs.docker.com/docker-for-mac/install/) oder [andere](https://docs.docker.com/engine/installation/)
2. Ein Terminal starten
   - Windows: Windows-Taste drücken und `cmd` eingeben
   - MacOS: Spotlight-Suche (Tastenkürzel: ⌘ + Leertaste), Suche nach `terminal`
   - Linux: Wer Linux benutzt, weiß wie es geht ;-)
3. Nachfolgenden Befehl ausführen:
```bash
docker run -t alpine /bin/sh -c 'cat /dev/urandom | tr -dc A-Z9 | head -c${1:-81}; echo'
```

## Generierung der Seed über einen Download
Ein Download eines Seed-Generators kann ich nicht empfehlen. Es kann keiner zu 100% garantieren, dass die Seed nicht an andere Personen gesendet wird.

## Seed selbst wählen
Von allen Optionen ist die Seed selbst wählen/ausdenken die schlechteste Idee. Das menschliche Gehirn ist nicht in der Lage, einen kryptografisch sicheren Code zu erstellen. Die Gefahr einer Kollision (einer identischen Seed einer anderen Person) ist um ein vielfaches höher, als bei der Erstellung über technische Hilfsmittel. 

`##Include##: _crypto-disclaimer-de`

```##Tags##: iota seed generator krypto```
