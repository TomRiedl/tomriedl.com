# Binance Referral Code – Affiliate Link, wo finde ich ihn?

`##Description##: 20% Kommission erhalten, Binance Referral Link/Affiliate Code zum Freunde werben erstellen`
`##Type##: article`
`##Locale##: de-DE`

`##Picture##:1600:907:/wp-content/uploads/2017/12/binance-referral-code:png:Binance Referral Code Affiliate Link erzeugen`

Bei [Binance](BINANCE_URL) erhält man 20% Kommission aller eingeladenen Personen. Ähnlich wie bei [Bitfinex](BITFINEX_URL) ([Anleitung](/version1/anleitung-fuer-neues-bitfinex-affiliate-referral-system/)) erhält man die Vergütung in der jeweiligen Währung, in der die geworbene Personen gehandelt hat. Es ist möglich, einen Rückvergütungssatz für den geworbenen festzulegen.

Hast du noch keinen Binance-Account und meldest dich über meinen Link an, erhältst du Zugang zur [VIP-Gruppe](/support/vip-gruppe/) ([mehr Infos](/support/vip-gruppe/)):<br />[BINANCE_URL](BINANCE_URL)

## Anleitung

1. In [Binance](BINANCE_URL) einloggen.
2. Im Menü rechts oben auf das Account-Symbol klicken und *Empfehlung* auswählen.<br />`##Picture##:543:525:anleitung-binance-ref-link-menu:png:binance referral link affiliate programm`
3. Auf der Affiliate-Seite auf **Generiere deinen Link** klicken.<br />`##Picture##:501:206:referral-link-generieren:png:affiliate link generieren`
4. Im erscheinenden Dialog den Rückvergütungssatz festlegen und neuen Link erzeugen.<br />`##Picture##:427:486:binance-affiliate-programm-link-erzeugen:png:binance affiliate link partner freunde`
5. Link an Freunde verteilen.

```##Tags##: affiliate binance code programm referral affiliate-system freunde werben krypto```

<ul class="LanguageSelection">
    <li><a href="/en/crypto/binance/binance-referral-code-affiliate-link/">English</a></li>
    <li><a href="/zh/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81/%E5%B8%81%E5%AE%89/%E5%B8%81%E5%AE%89-%E6%8E%A8%E8%8D%90-%E4%BB%A3%E7%A0%81-%E5%BE%81%E9%9B%86-%E9%93%BE%E6%8E%A5/">中文</a></li>
</ul>

`##Include##: _crypto-disclaimer-de`

`##Translation##:de:https://##Domain##/version1/binance-referral-code-affiliate-link-wo-finde-ich-ihn/`
`##Translation##:en:https://##Domain##/en/crypto/binance/binance-referral-code-affiliate-link/`
`##Translation##:zh-Hans:https://##Domain##/zh/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81/%E5%B8%81%E5%AE%89/%E5%B8%81%E5%AE%89-%E6%8E%A8%E8%8D%90-%E4%BB%A3%E7%A0%81-%E5%BE%81%E9%9B%86-%E9%93%BE%E6%8E%A5/`
