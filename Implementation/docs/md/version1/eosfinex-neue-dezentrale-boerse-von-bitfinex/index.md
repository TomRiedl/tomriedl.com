# EOSfinex: Neue dezentrale Börse von Bitfinex

`##Description##: Ankündigung der dezentralen Kryptobörse EOSfinex`
`##Type##: article`
`##Locale##: de-DE`

![EOSfinex: Neue dezentrale Börse von Bitfinex](/wp-content/uploads/2018/02/eosfinex-1.png)

Bitfinex hat am 12. Februar 2018 [angekündigt](https://medium.com/bitfinex/announcing-eosfinex-69eea273369f), dass sie an der dezentrale Kryptobörse [EOSfinex](https://www.eosfinex.com/) arbeiten. Dabei nutzen sie die Technologie EOS. EOS ([Homepage](https://eos.io/), [Whitepaper](https://github.com/EOSIO/Documentation/blob/master/TechnicalWhitePaper.md), [Quelltexte](https://github.com/eosio)) ist eine Open Source Smart Contract Plattform, auf dessen Basis dezentrale Anwendungen entwickelt werden können.

Bitfinex verspricht mit EOSfinex eine schnelle, skalierbare und transparente Börse auf Basis einer Blockchain.

 Offizielle Ankündigungen:
- <https://medium.com/bitfinex/announcing-eosfinex-69eea273369f>
- <https://twitter.com/bitfinex/status/963103151132954631>

`Veröffentlicht 2018-02-13, Autor: Tom Riedl`

`##Include##: _crypto-disclaimer-de`

```##Tags##: eosfinex bitfinex börse kryptobörse handle trading krypto```
