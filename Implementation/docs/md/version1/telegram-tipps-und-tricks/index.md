# Telegram Tipps und Tricks

`##Description##: Tipps und Tricks für Telegram`
`##Type##: article`
`##Locale##: de-DE`

![Telegram Tipps und Tricks](/wp-content/uploads/2018/06/telegram-tipps-und-tricks.png)

[Telegram](https://telegram.org/) ist einer der besten Messenger, die es derzeit gibt. Neben den sehr stabilen Apps bietet Telegram end-to-end verschlüsselte Chats, Gruppen, Channels, Bots, verschlüsselte Telefonanrufe, eine Client-Library und API für Entwickler, Apps für [Android](https://play.google.com/store/apps/details?id=org.telegram.messenger) und [iOS](https://itunes.apple.com/app/telegram-messenger/id686449807), sowie eine [Web-Version](https://web.telegram.org/) und Clients für [Windows](https://desktop.telegram.org/), [Linux](https://desktop.telegram.org/) und [MacOS](https://macos.telegram.org/). Telegram ist [DSGVO-konform](https://telegram.org/faq#q-what-about-gdpr).

Im Folgenden zeige ich dir ein paar nützliche Tricks, wie man Telegram noch besser verwenden kann.
Die Tipps und Tricks werden in unregelmäßigen Abständen erweitert.

## Nicknames/Spitznamen für Kontakte vergeben
1. Im Chat der Person auf den Namen oder das Bild klicken.<br />![Telegram Spitzname Kontakt auswählen](/wp-content/uploads/2018/06/Telegram-Nickname-Kontakt-ausw%C3%A4hlen-169x300.png)
2. Das Menü öffnen und **Bearbeiten** auswählen.<br />![Telegram Nickname Kontakt bearbeiten](/wp-content/uploads/2018/06/Telegram-Nickname-Kontakt-bearbeiten-169x300.png)
3. Beim Vornamen den Spitznamen eingeben und durch ein Zeichen (z.B. „/“) vom vollen Vornamen trennen.<br />![Telegram Spitzname Vorname ändern](/wp-content/uploads/2018/06/Telegram-Nickname-Name-aendern-169x300.png)
4. Die Änderung speichern.<br />![Telegram Nickname Änderung speichern](/wp-content/uploads/2018/06/Telegram-Nickname-Aenderung-speichern-169x300.png)
5. Nun wird der Spitzname vor dem vollen Namen angezeigt und es kann nach allen Namensvariationen gesucht werden.<br />![Telegram nach Spitzname durchsuchen](/wp-content/uploads/2018/06/Telegram-nach-Spitzname-suchen-169x300.png)

## Kontakte kategorisieren und gruppieren

Wer Telegram privat, beruflich, in der Familie, in Vereinen, etc. verwendet hat schnell das Problem, dass die Kontaktliste sehr lang wird. Will man in Telegram alle Kontakte einer bestimmten Gruppe anzeigen ist das mit dem folgenden Trick möglich.

Hierzu fügen wir ein Gruppensymbol an den Anfang des Namens ein:
1. Im Chat der Person auf den Namen oder das Bild klicken.<br />![Telegram Kontakt kategorisieren Person öffnen](/wp-content/uploads/2018/06/Telegram-Kontakt-kategorisieren-Person-oeffnen-169x300.png)
2. Das Menü öffnen und **Bearbeiten** auswählen.<br />![Telegram Personen kategorisieren Menü öffnen](/wp-content/uploads/2018/06/Telegram-Personen-kategorisieren-Menue-oeffnen-169x300.png)
3. Vor dem Vornamen ein Emoji für die jeweilige Gruppe einfügen. Ich verwende z.B. folgende Emojis:<br />🏠 Familie<br />🌻 Freunde<br />🏢 Kollegen und Geschäftspartner<br />🏃 Sport-Partner<br />👤 Bekannte<br />![Telegram Kontakte gruppieren Emoji einfügen](/wp-content/uploads/2018/06/Telegram-Kategorien-Emoji-einfuegen-169x300.png)
4. Die Änderung speichern.<br />![Telegram Kontakte kategorisieren Änderungen speichern](/wp-content/uploads/2018/06/Telegram-Kategorien-Aenderung-speichern-169x300.png)
5. Nun wird die Kontaktliste automatisch nach den Kategorien sortiert und du kannst in der Suche gezielt nach einer Person in einer bestimmten Gruppe suchen.<br />![Telegram kategorisierte Kontakte](/wp-content/uploads/2018/06/Telegram-kategorisierte-Kontakte-169x300.png) ![Telegram nach Kategorien suchen](/wp-content/uploads/2018/06/Telegram-nach-Kategorie-suchen-169x300.png) ![In Telegram nach kategorie und Name suchen](/wp-content/uploads/2018/06/Telegram-nach-Kategorie-und-Name-suchen-169x300.png)

## Account mit zweistufiger Bestätigung absichern
Hierfür habe ich einen eigenen Artikel geschrieben: [Telegram-Account mit zweistufiger Bestätigung absichern (Two-Step-Verification)](/version1/telegram-account-mit-zweistufiger-bestaetigung-absichern-two-step-verification/)

## Gemeinsame Einkaufsliste/ToDo-Liste

Eine gemeinsame Liste kann man in einem privaten Channel gut pflegen. Dabei können zwei bis mehrere Personen Einträge hinzufügen und wenn sie erledigt/gekauft sind wieder löschen.

1. Neuen Kanal erstellen (über den Button rechts unten oder über das Menü).
2. Kanalname eingeben.
3. Bei den Kanal-Einstellungen **privat** auswählen
4. Die andere Person zum Channel hinzufügen.
5. Wieder auf den Gruppennamen klicken, um die Gruppendetails zu öffnen.
6. Auf **Administratoren** klicken.
7. **Admin hinzufügen** wählen.
8. Die andere Person auswählen.
9. Rechte festlegen. Die andere Person benötigt die Rechte **Nachrichten senden**, **Andere Nachrichten bearbeiten** und **Nachrichten von anderen löschen**.
10. Mit einem Klick auf den Haken rechts oben bestätigen.
11. Mit dem Pfeil links oben zurück.
12. Nun können Einträge durch normale Nachrichten der Liste hinzugefügt werden.
13. Ist ein Eintrag erledigt/gekauft, die zugehörige Nachricht anklicken und Löschen auswählen. Dadurch, dass es ein Kanal ist, wird der Eintrag auch bei allen anderen Mitgliedern der Gruppe gelöscht.

```##Tags##: telegram tipp trick frage antwort faq```
