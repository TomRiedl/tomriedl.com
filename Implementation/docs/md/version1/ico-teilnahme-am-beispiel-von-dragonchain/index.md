# ICO-Teilnahme am Beispiel von DragonChain

`##Description##: Anleitung und Beschreibung einer ICO-Teilnahme`
`##Type##: article`
`##Locale##: de-DE`

![ICO-Teilnahme am Beispiel von DragonChain](/wp-content/uploads/2017/10/dragonchain-cover.png)

[ICOs (Initial Coin Offerings)](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/) sind eine interessante und oft auch lukrative Möglichkeit der Spekulation.

Beim ICO (oder oft auch Token Sale) werden neue Krypto-Währungen das erste Mal der Öffentlichkeit zur Verfügung gestellt. Dabei werden Anteile an einer bestimmten Menge von Tokens/Coins zum Kauf angeboten. Bei guten Kryptowährungen rentiert sich der Kauf besonders zu diesem Zeitpunkt, da der Preis der Tokens noch gering ist und sehr häufig später, wenn diese auf Börsen gehandelt werden, einen vielfach höheren Wert haben.

Wie man bei einem solchen ICO mitmacht, zeige ich euch anhand des ICOs der DragonChain. Bitte beachten, dass andere Kryptowährungen anderen Prozesse haben können.
Das DragonChain-Projekt wurde ursprünglich von Disney gestartet und wurde 2017 in die Dragonchain Foundation und Dragonchain, Inc ausgegründet.

1. Für den DragonChain ICO benötigt man ein Wallet, das ERC20-Tokens unterstützt. Am einfachsten geht das mit einem Ethereum-Wallet bei <https://www.myetherwallet.com/><br />![Dragonchain ICO MEW-Wallet](/wp-content/uploads/2017/10/dragonchain-01-mew.png)
2. Falls kein solches Ethereum-Wallet verfügbar ist, muss dieses erst angelegt werden.
3. Die Tokensale-Seite <https://dragonchain.com/tokensale/> öffnen.
4. Die [Terms and Conditions](https://dragonchain.com/terms/) und die [Payment Instructions](https://dragonchain.com/ico-guide/) akzeptieren.<br />![dragonchain ico anleitung terms and conditions](/wp-content/uploads/2017/10/dragonchain-02-terms.png)
5. Nun wird die Ethereum-Empfänger-Adresse 0x8bA7408497883d3a569066FA3a331aa892138868 angezeigt. Die Gültigkeit dieser Adresse kann wie in diesem Video gezeigt überprüft werden: `https://www.youtube.com/watch?v=OHUajNgvZt4` [Anmerkung 2020-12-28: Video wurde entfernt]<br />![dragonchain ico empfängeradresse tutorial](/wp-content/uploads/2017/10/dragonchain-02a-address.png)
6. MyEtherWallet (MEW) über den Menüpunkt **Send Ether & Tokens** öffnen und mit einer beliebigen Methode das Wallet freischalten.<br />![tutorial myetherwallet anleitung mew login](/wp-content/uploads/2017/10/dragonchain-03-unlock-mew-1.png)
7. Nachdem die Transaktion angezeigt wird auf **Send Transaction** klicken.<br />![MyEtherWallet send transaction transaktion senden](/wp-content/uploads/2017/10/dragonchain-05-send-transaction.png)
8. Im nun erscheinenden Dialog nochmal alle Daten überprüfen und auf **Yes, I am sure! Make Transaction** klicken.<br />![Transaktion bestätigen MEW Dragonchain](/wp-content/uploads/2017/10/dragonchain-06-confirm.png)
9. Der ICO-Kauf ist hiermit abgeschlossen. Nachdem der ICO beendet ist, werden die Dragonchain-Tokens auf das MEW-Wallet transferiert.<br />![Erfolgreiche MyEtherWallet Transaktion](/wp-content/uploads/2017/10/07-dragonchain-ico.png)

`##Include##: _crypto-disclaimer-de`

```##Tags##: ico dragonchain myetherwallet mew krypto```
