# Telegram-Account mit zweistufiger Bestätigung absichern (Two-Step-Verification)

`##Description##: Eine Anleitung um Telegram zusätzlich mit einem Passwort abzusichern`
`##Type##: article`
`##Locale##: de-DE`

![Telegram-Account mit zweistufiger Bestätigung absichern (Two-Step-Verification)](/wp-content/uploads/2018/04/telegram-zwei-wege-authentifizierung.png)

Erstellt man einen Telegram-Account ist dieser zuerst nur über die eigenen Handynummer gesichert. Gelangt jemand in den Besitz des Handys kann dieser ohne großen Aufwand den Telegram-Account auf einem anderen Gerät neu verbinden. Hierzu muss das Mobilgerät oftmals nicht entsperrt werden, da die Nachrichten oder SMS direkt auf dem Lock-Screen angezeigt werden.

Um das zu verhindern, sollte Telegram immer mit der Two-Step-Verification abgesichert werden. [Mehr Informationen zur Zwei-Wege-Authentifizierung findest du in einem anderen Artikel](/version1/two-factor-authentication-multi-factor-authentication-tfa-2fa-mfa-totp/).

Hier findest du Anleitungen für Telegram, Telegram X, sowie Telegram Desktop.

## Mobilgerät (Telegram und Telegram X)
1. Das Telegram-Menü öffnen.<br />![Telegram zweistufige Bestätigung Hauptmenü](/wp-content/uploads/2018/04/telegram-mobile-two-factor-01-169x300.png) ![telegram x two-factor menu](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-01-169x300.png)
2. **Einstellungen**/**Settings** öffnen<br />![Telegram Menü Einstellungen](/wp-content/uploads/2018/04/telegram-mobile-two-factor-02-169x300.png) ![Telegram Zwei Wege Authentifizierung Einstellungen](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-02-169x300.png)
3. Im Menü nach **Privatsphäre und Sicherheit**/**Privacy and Security** suchen.<br />![Telegram Privatsphäre und Sicherheit Mehrstufige Bestätigung](/wp-content/uploads/2018/04/telegram-mobile-two-factor-03-169x300.png) ![Telegram X Privacy and Security Menu Two-Factor](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-03-169x300.png)
4. **Zweistufige Bestätigung**/**Two-Step Verification** öffnen.<br />![Telegram Zweistufige Bestätigung Menü](/wp-content/uploads/2018/04/telegram-mobile-two-factor-04-169x300.png) ![Telegram X Two-Step Verification settings menu](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-04-169x300.png)
5. Den Bestätigungsprozess mit einem Klick auf **Zusätzliches Kennwort festlegen**/**Set Additional Password** starten.<br />![Telegram Zweistufige Bestätigung Vorgang starten](/wp-content/uploads/2018/04/telegram-mobile-two-factor-05-169x300.png) ![Telegram X Two-Factor-Authentication Anleitung Tutorial Password](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-05-169x300.png)
6. Dein Wunschpasswort setzen.
7. Das Passwort zur Bestätigung noch einmal eingeben.
8. Optional: Einen Passworthinweis hinterlegen (hier aber keine relevanten Informationen über das Passwort selbst preisgeben).
9. Deine E-Mail-Adresse angeben.
10. Nun wird an deine E-Mail-Adresse eine Aktivierungsmail gesendet.<br />![Telegram zweistufige Bestätigung abgeschlossen](/wp-content/uploads/2018/04/telegram-mobile-two-factor-06-169x300.png) ![Telegram X Two-Factor E-Mail Bestätigung](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-06-169x300.png)
11. E-Mail öffnen und Bestätigungslink anklicken.<br />![Telegram Bestätigungsemail Zweistufige Bestätigung](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-07-300x120.png)
12. Willst du die E-Mail oder das Passwort später ändern, findest du diese Optionen im zugehörigen Menü (Schritte 1 bis 4).<br />![Telegram Passwort ändern E-Mail ändern](/wp-content/uploads/2018/04/telegram-mobile-two-factor-09-169x300.png) ![Telegram X Passwort oder E-Mail ändern](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-09-169x300.png)

## Telegram Desktop
1. Das Hauptmenü öffnen.<br />![Telegram Desktop Menü Zweistufige Bestätigung](/wp-content/uploads/2018/04/telegram-desktop-two-factor-01-300x225.png)
2. **Einstellungen** öffnen.<br />![Telegram Desktop Einstellungen](/wp-content/uploads/2018/04/telegram-desktop-two-factor-02-300x225.png)
3. Unter **Privatsphäre und Sicherheit** den Menüpunkt **Zweistufige Bestätigung aktivieren** anklicken.<br />![Telegram Desktop zweistufige Bestätigung aktivieren](/wp-content/uploads/2018/04/telegram-desktop-two-factor-03-300x225.png)
4. Zweimal dein Passwort eingeben, optional ein Kennwort-Hinweis und deine E-Mail. Anschließend auf **Speichern** klicken.<br />![Telegram Desktop Two-Factor-Authentication aktivieren](/wp-content/uploads/2018/04/telegram-desktop-two-factor-04-300x225.png)
5. E-Mail öffnen und Bestätigungslink anklicken.<br />![Telegram Bestätigungsemail Zweistufige Bestätigung](/wp-content/uploads/2018/04/telegram-x-mobile-two-factor-07-300x120.png)

```##Tags##: telegram tfa 2fa totp sicherheit```
