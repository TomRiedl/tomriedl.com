# Bitcoin Expresskauf mit Fidor und Bitcoin.de

`##Description##: Bitcoin schnell und unkompliziert über bitcoin.de und Fidor kaufen`
`##Type##: article`
`##Locale##: de-DE`

![Bitcoin Expresskauf mit Fidor und Bitcoin.de](/wp-content/uploads/2017/10/bitcoin.de-1000.png)

Bitcoins können auf verschiedenen Wegen erworben werden, sehr schnell geht das jedoch mit dem neuen [Express-Kauf von bitcoin.de](https://www.bitcoin.de/de/r/b3dhpd) über [Fidor](https://banking.fidor.de/register?ibid=22253282). Fidor ist eine Online-Bank mit kostenlosem Bankkonto und vielen nützlichen Werkzeugen, u.a. einer vollständigen Banking-API. Über diese Banking-API wird der Bitcoinkauf von [bitcoin.de](https://www.bitcoin.de/de/r/b3dhpd) abgewickelt. Ab 14.02.2018 wird hierbei allerdings eine Transaktionsgebühr von 0.5% des Transaktionsvolumens erhoben.

## Anleitung

1. Voraussetzung ist ein Bankkonto bei [Fidor](https://banking.fidor.de/register?ibid=22253282) und ein Account bei [bitcoin.de](https://www.bitcoin.de/de/r/b3dhpd).
2. Im Menü (mein Bitcoin.de) auf Express-Handel klicken.<br />![bitcoin.de fidor express handel anleitung menü](/wp-content/uploads/2017/08/bitcoin.de-fidor.de-express-kauf-00.png)
3. Ein gewünschtes Bankguthaben reservieren. Hierbei Betrag und die Dauer der Reservierung angeben (eine Stunde sollte ausreichend sein). Anschließend mit einem Klick auf Reservierung einleiten bestätigen.<br />![bitcoin.de fidor reservierung einleiten anleitung tutorial](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-04.png)
4. Die Fidor-Homepage wird geöffnet. Hier einloggen und die Reservierung mit einem Klick auf Auftrag via App/mTAN bestätigen fortführen. <br />![fidor.de Reservierung Anleitung](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-05.png)
5. Die Reservierung in der App bestätigen oder die mTAN aus der SMS eingeben und den Dialog mit einem Klick auf mTAN bestätigen schließen.<br />![fidor mTAN App TAN bitcoin.de](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-06.png)
6. Bitcoin.de öffnet sich wieder mit der Bestätigung, dass die Reservierung vorgenommen wurde.<br />![Bitcoin.de Reservierung bestätigt](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-07.png)
7. Erneut das Menü öffnen und den Punkt Express-Kauf-Kompakt auswählen.<br />![bitcoin.de express-kaufen menü-eintrag](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-01.png)
8. Die gewünschte Menge an Bitcoins eingeben und auf Angebote ermitteln klicken.<br />![bitcoin.de Angebote ermitteln Expresskauf](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-02.png)
9. In der nun folgenden Übersicht wird das beste Angebot angezeigt. Die Anzahl der Bitcoins können weiterhin editiert werden.
Ist alles wie gewünscht, den angezeigten Hinweis akzeptieren und den Kauf mit einem Klick auf *Jetzt sofort n Angebote kaufen* bestätigen.<br />![bitcoin.de Expresskauf bestätigen](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-10.png)
Der Kauf wurde erfolgreich abgeschlossen und die Bitcoins liegen auf dem [Bitcoin.de-Konto](https://www.bitcoin.de/de/r/b3dhpd).<br />![bitcoin.de Expresskauf abgeschlossen](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-11.png)

## Bitcoins auszahlen

1. Bei [bitcoin.de](https://www.bitcoin.de/de/r/b3dhpd) einloggen.
2. Im Menü den Punkt Auszahlungen auswählen.![bitcoin.de Ein-Auszahlungen Anleitung](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-12.png)
3. Den Reiter Auszahlung auswählen und folgende Informationen ausfüllen:<br />-Betrag<br />- Netzwerkgebühr (Ein Hinweis dazu, wie viel es sein muss, findet sich im Formular weiter unten)<br />- Passwort<br />- Einmal-Passwort (OTP; [weitere Informationen zu OTP](/version1/two-factor-authentication-multi-factor-authentication-tfa-2fa-mfa-totp/))<br />![bitcoin.de Auszahlung](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-13.png)
4. Auf *Überweisung anfordern* klicken. Die Bitcoins werden dann innerhalb von einigen Minuten ausgezahlt.<br />![bitcoin.de Auszahlung bestätigt](/wp-content/uploads/2017/10/bitcoin.de-fidor.de-express-kauf-14.png)

`##Include##: _crypto-disclaimer-de`

```##Tags##: bitcoin fidor handel trading kaufen krypto```
