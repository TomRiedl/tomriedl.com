# Coinlend-App

`##Description##: Beschreibung der Coinlend-App für Android und iOS`
`##Type##: article`
`##Locale##: de-DE`

![coinlend app](/wp-content/uploads/2018/07/coinlend-app.jpg)

Die deutschen Entwickler aus Mannheim haben mit [coinlend](COINLEND_URL) das zinsbringende Verleihen von Kryptowährungen zu einem effizienten und komfortablen Service gemacht.

Nach den letzten Neuerungen der Plattform, wie z.B. [Two-Factor-Authentication](/version1/two-factor-authentication-multi-factor-authentication-tfa-2fa-mfa-totp/), Notifications und den Steuerbericht entwickelten sie nun auch eine App für Android und iOS.

Ich durfte die App vorab testen und bin begeistert. Die App bietet jetzt schon 90% aller Features, die auch auf der Website zu finden sind.
Geschäftsführer Jan Pumpe erwähnte, dass sie neben dem Look & Fell der Website auch alle Funktionen von [coinlend.org](COINLEND_URL) in die App übertragen wollen.

[Weitere Artikel zu coinlend](/tag/coinlend/)

[![Coinlend App Android](/wp-content/uploads/2018/07/google-play-badge-300x89-300x89.png)](https://play.google.com/store/apps/details?id=org.coinlend)
[![Coinlend App iPhone iOS](/wp-content/uploads/2018/07/download-on-the-app-store-300x89.png)](https://itunes.apple.com/us/app/coinlend/id1402850970?ls=1&mt=8)

- Der Login über eine eigene E-Mail, Facebook oder Google. Hast du noch keinen Account, kannst du diesen hier erstellen: [coinlend-Account registrieren](COINLEND_URL)<br />
![Coinlend Login über Facebook Google oder E-Mail](/wp-content/uploads/2018/07/Screenshot_2018-07-10-00-01-05-169x300.png)
![Coinlend Login Benutzername Passwort](/wp-content/uploads/2018/07/Screenshot_2018-07-10-00-01-22-169x300.png)
- Die erste Seite zeigt die aktuellen Lending-Rates.<br />![Coinlend Lending Rates USD Bitcoin Ethereum](/wp-content/uploads/2018/07/Screenshot_2018-07-10-00-02-44-169x300.png)
- Im Reiter Loans werden die eigenen Verleihvorgänge angezeigt.<br />
![Coinlend App Loans Verleih](/wp-content/uploads/2018/07/Screenshot_2018-07-10-00-03-01-169x300.png)
![Coinlend App Loans Verleih Details Zinsen](/wp-content/uploads/2018/07/Screenshot_2018-07-10-00-04-38-169x300.png)
- Unter Exchanges werden die verknüpften und möglichen Kryptobörsen angezeigt.<br />![Coinlend App Kryptobörsen API Keys Accounts](/wp-content/uploads/2018/07/Screenshot_2018-07-10-00-03-27-169x300.png)
- Hierüber können für jede Währung individuelle Einstellungen vorgenommen werden.<br />![Coinlend-App Einstellungen Bitfinex](/wp-content/uploads/2018/07/Screenshot_2018-07-18-08-19-10-169x300.png)
- Die Seite Account zeigt die aktuelle Balance aller Börsen, sowie die Gewinne der letzten 30 Tage und den Wert der Zinsen gesamt.<br />![Coinlendapp Balance Börse Interest Zinsen Gewinne](/wp-content/uploads/2018/07/Screenshot_2018-07-10-00-03-40-169x300.png)

[![Coinlend App Android](/wp-content/uploads/2018/07/google-play-badge-300x89-300x89.png)](https://play.google.com/store/apps/details?id=org.coinlend)
[![Coinlend App iPhone iOS](/wp-content/uploads/2018/07/download-on-the-app-store-300x89.png)](https://itunes.apple.com/us/app/coinlend/id1402850970?ls=1&mt=8)

[Weitere Artikel zu coinlend](/tag/coinlend/)

`##Include##: _crypto-disclaimer-de`

```##Tags##: android app coinlend ios lending saving zinsen verleihen krypto```
