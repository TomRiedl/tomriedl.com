# Risiken beim Spekulieren mit Kryptowährungen

`##Description##: Eine Übersicht von möglichen Risiken beim Spekulieren mit Kryptowährungen`
`##Type##: article`
`##Locale##: de-DE`

![Risiken beim Spekulieren mit Kryptowährungen](/wp-content/uploads/2017/12/AvalancheDanger.jpg)

Das Spekulieren mit Kryptowährungen ist mit hohen Risiken verbunden. Die wichtigsten sind:

## Fehlerfreie Software gibt es nicht
Sowohl die Technik hinter Kryptowährungen, wie auch die Wallets sind von Menschen programmiert und können dadurch Fehler enthalten. Diese Fehler können z.B. dazu führen, dass sich Angreifer Zugriff zu den Assets verschaffen und diese auf eigene Konten transferieren.

## Kurssteigerungen können nicht garantiert werden
Keiner kann garantieren, dass der Wert einer Kryptowährung stabil bleibt oder steigt.

## Kryptowährung wird eingestellt oder nicht mehr unterstützt
Die meisten Kryptowährungen sind von Nodes/Peers/Servern abhängig, damit diese funktionieren. Stellen die Betreiber (oder bei OpenSource-Projekten die Community) keine dieser zentralen Elemente mehr zur Verfügung, ist es nicht mehr möglich diese Kryptowährung zu verwenden.

## Gesetzliche Änderungen
Gesetzliche Änderungen können das Handeln mit Kryptowährungen verbieten oder einschränken.

## Eigene Fertigkeiten
Das Spekulieren mit Kryptowährungen kann unter anderem großes technisches Verständnis voraussetzen. Besonders bei Kryptowährungen, die noch im Alpha oder Beta-Stadium sind. Lese dir deshalb immer alle Anleitungen und Infos genau durch und sei dir sicher, dass du sie verstanden hast.

## Schlüssel verloren
Der Zugriff zu den eigenen Währungen erfolgt über einen Schlüssel/Key/Seed. Wird dieser verloren ist es nicht mehr möglich, auf seine eigenen Kryptowährungen zuzugreifen.

## Hacks und Viren
Der eigene PC kann gehackt oder mit Viren versucht sein. Diese Kompromittierung kann bedeuten, dass sich Angreifer Zugriff zu den eigenen Coins oder Tokens verschaffen.

## Verschlüsselung unsicher
Neue Technologien, wie Quantenrechner können die Verschlüsselungen unsicher machen. Um einen Schlüssel über Bruteforce (das systematische ausprobieren von Passwörtern) zu ermitteln, dauert es Millionen bis Milliarden oder sogar noch mehrere Jahre. Neue Technologien können diese Zeit extrem verkürzen. Viele Entwickler von Kryptowährungen sind sich dessen bewusst und haben dafür bereits Vorkehrungen getroffen. Sie können z.B. die Verschlüsselungstechnologien austauschen oder verwenden Technologien die auch bei Quantenrechner noch sicher sind.

## Transaktionen auf falsche Adressen
Die Empfangsadressen sind meist lange Zeichenketten. Wird diese Zeichenkette falsch kopiert, beim Abtippen verändert oder durch einen Hack verändert, gehen die Überweisungen entweder auf ein falsches Konto oder ins Nirgendwo. Deshalb diese Adressen doppelt überprüfen.

## ICOs
Mögliche [ICOs (Initial Coin Offerings)](https://de.wikipedia.org/wiki/Initial_Coin_Offering) für neue Währungen können darauf aus sein, Geld abzugreifen, bieten aber keine solide Kryptowährung als Gegenwert.

## Dritte Plattformen
Sind Kryptowährungen nicht in der eigenen Wallet, sondern bei einem Online-Betreiber (Börsen, Online-Wallets, etc.) gelagert, entstehen hierbei zusätzliche Risiken:

- Dein Account kann gehackt werden (möglicher Schutz: [Two-Factor Authentication](/version1/two-factor-authentication-multi-factor-authentication-tfa-2fa-mfa-totp/))
- Die Plattform kann gehackt werden.
- Die Betreiber stellen den Service ein und zahlen ihre Kunden nicht aus.
- Gesetzliche Änderungen schränken die Betreiber ein.

<br />

## Deshalb gilt
> Investiere immer nur so viel, wie du auch bereit bist zu verlieren. 

Das Spekulieren mit Kryptowährungen ist steuerpflichtig. Spreche daher mit deiner:m Steuerberater:in, was zu beachten ist. 

*Cover-Foto von [Nicolas Cool](https://stocksnap.io/author/33662)*

```##Tags##: risiko handel kryptowährung trading krypto```
