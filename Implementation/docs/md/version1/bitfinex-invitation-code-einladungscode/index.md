# Bitfinex Invitation Code (Einladungscode)

`##Description##: Bitfinex Ref-Link für vergünstigte Gebühren`
`##Type##: article`
`##Locale##: de-DE`

![bitfinex referral affiliate einladungscode](/wp-content/uploads/2017/12/bitfinex-invitation-code-1.png)

Verwende für die Registrierung folgenden Link für eine Rückvergütung von 6%:<br />[BITFINEX_URL](BITFINEX_URL)

![bitfinex rückvergütung anmeldung referral](bitfinex-rebate-reward-rueckverguetung-fuer-anmeldung.png)

Wie du selbst einen Affiliate-Link erzeugst, kannst du in dieser Anleitung nachlesen:<br />[Anleitung für neues Bitfinex Affiliate/Referral-System](/version1/anleitung-fuer-neues-bitfinex-affiliate-referral-system/)

`##Include##: _crypto-disclaimer-de`

```##Tags##: bitfinex invitation einladung affiliate referral krypto```
