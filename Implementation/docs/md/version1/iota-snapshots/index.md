# IOTA Snapshots

`##Description##: Was sind IOTA-Snapshots und was ist hierfür zu tun`
`##Type##: article`
`##Locale##: de-DE`

![IOTA Snapshots](/wp-content/uploads/2018/02/iota-snapshots.png)

`Veröffentlicht 2018-02-04, Autor: Tom Riedl`

Das IOTA-Netzwerk (die Tangle) durchläuft immer wieder einen Prozess zur Performanceoptimierung. Hierbei werden alte Transaktionen entfernt und nur Adressen mit einer positiven Balance weitergeführt. Dieser Vorgang verhindert, dass die Tangle zu groß wird, was wiederum die Nodes dahingehend unterstützt, dass sie weniger Rechenleistung und Speicher benötigen. Die Erstellung der Snapshots ist momentan noch einer manueller Vorgang, der vom IOTA-Team angestoßen wird. Später soll dieser voll automatisch ausgeführt werden.

Verwendet man eine Light-Wallet sind nach einem Snapshot alle Transaktionen verschwunden. Dadurch hat die Wallet keine Information darüber, welche Adressen bereits verwendet wurden und/oder welche Adressen zur Seed gehören. Erkennbar ist das daran, dass die Balance 0 anzeigt. Die IOTA sind deshalb nicht verloren, sondern nur nicht sichtbar.

Um den korrekten Kontostand anzuzeigen, ist daher nichts weiter nötig, als so viele Adressen zu generieren, wie vor dem Snapshots in der Wallet sichtbar waren. Die neu erzeugten Adressen sind komplett identisch mit den Adressen, die auch vor der Snapshots genutzt wurden.

Es ist hilfreich, die Anzahl der zugehörigen Adressen einer Seed möglichst gering zu halten. Dadurch müssen nach einem Snapshot nicht zu viele Adressen generiert werden. Es bietet sich an, immer mal wieder die IOTA nach vielen Transaktionen auf eine neue Adresse einer komplett neuen Seed zu transferieren.

Weiterführende Informationen im [HelloIOTA-Forum](https://forum.helloiota.com/4659/How-do-I-prepare-for-a-snapshot).

Bei Fragen zu IOTA findest du hier eine [Hilfeseite](/version1/iota-erste-hilfe-fragen-und-antworten-faq/).

`##Include##: _crypto-disclaimer-de`

```##Tags##: iota snapshot krypto```
