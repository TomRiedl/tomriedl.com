# Spamreduzierung durch Catch-All E-Mails für alle Mitarbeiter

`##Description##: Konzept zur Spam-Reduzierung durch Catch-All E-Mails`
`##Type##: article`
`##Locale##: de-DE`

![Spamreduzierung durch Catch-All E-Mails für alle Mitarbeiter](/wp-content/uploads/2014/09/20H1.jpg)

## Problemstellung
Die aktuellen Spam-Filter arbeiten größtenteils sehr zuverlässig. Dennoch ist eine regelmäßige Kontrolle des Spam-Ordners immer noch nötig, um False-Positives, also falsch erkannte und vielleicht sogar wichtige E-Mail nicht zu übersehen. Dies geht mit zusätzlichem Arbeitsaufwand und unnötiger Verzögerung der Arbeitsprozesse einher.

## Warum erhalten wir Spam-Mails
Man benötigt mittlerweile unzählige Accounts für viele verschiedene Services. Jeder Service-Provider erhält somit viele E-Mail-Adressen. Nun gibt es verschiedene Gründe, wie die E-Mails von diesen Providern zu den Spam-Verteilern geraten:
- Der Anbieter verkauft diese E-Mails
- Hacker erlangen Zugriff auf die Datenbank mit den User-Accounts
- Der Anbieter selbst versendet Spam
- Die E-Mails werden nicht über HTTPS übertragen und die Kommunikation wird abgefangen ([MITM-Angriff](https://de.wikipedia.org/wiki/Man-in-the-Middle-Angriff))

Im Zeitalter der Apps entsteht eine weitere Gefahr. Viele Apps haben Zugriff auf das Telefonbuch der Handys und erlangen so alle Kontaktdaten, die sie unbemerkt an ihre eigenen Server senden können. Das sind nicht selten Spiele und andere Apps, von denen man es nie erwarten würde. Aber auch Apps, die die Adressliste benötigen, sollten genauer unter die Lupe genommen werden. Wie man sich hiergegen schützen kann, werden wir in einem späteren Artikel näher erläutern.

## Was ist Catch-All und Subdomain-Stripping
Eine Catch-All E-Mailadresse ist eine Weiterleitung von E-Mails mit einer beliebigen Zeichenfolge im ersten Teil der Adresse. Dies wäre technisch dargestellt *@example.com (* = beliebige für E-Mails gültige Zeichenfolge). Jede Catch-All Weiterleitung benötigt ein Zielpostfach. In unserem Beispiel mail@example.com. Nun werden E-Mails wie support@example.com, info@example.com, vorname.nachname@example.com immer an die Adresse mail@example.com weitergeleitet.

Subdomain-Stripping ist ein Prozess, bei dem die Subdomains aus der Domain der E-Mails entfernt werden, damit diese E-Mails direkt an die Hauptdomain gesendet werden können. Die Domain bei den E-Mails ist alles rechts des @-Zeichens. Aus der E-Mail info@subdomain.hauptdomain.de wird info@hauptdomain.de. In der Wildcard-Schreibweise wird aus *@*.example.com die E-Mail *@example.com.

![Subdomain-Stripping Catch-All E-Mails](/wp-content/uploads/2014/09/CatchAllSubdomainStrippingCombinedBranded.png)

## Wie hilft Catch-All gegen Spam
Mit der Catch-All E-Mail kann man sich bei verschiedenen Services jeweils mit einer eigenen E-Mail registrieren. Bei Google mit der Adresse google@example.com, bei Facebook mit facebook@example.com, usw. Nun erhält man weiterhin wie gewohnt die E-Mails der Service-Anbieter auf die Hauptadresse.

Wirft man nun einen Blick in den Spam-Ordner, kann man schnell erkennen, an welche Adresse die Spam-Mail gesendet wurde und welcher Service die E-Mailadresse ‚verloren‘ oder weitergegeben hat. Nehmen wir an, wir erhalten eine Spam-Mail an abc@example.com. Nun gibt es zwei Möglichkeiten:
- Bei ABC einloggen und Account löschen
- Bei ABC einloggen und E-Mail-Adresse auf abc-1@example.com ändern

Nachdem dies erledigt ist, sollte die Adresse abc@example.com auf ein spezielles Postfach, wie spam@example.com umgeleitet werden. Hierzu erstellt man am besten einen Alias abc@example.com für spam@example.com. Das Postfach spam@example.com darf dabei einfach ignoriert und muss nicht abgerufen werden. Es werden somit keine Spam-Mails mehr empfangen, die an abc@example.com gesendet werden.

## Wie erhalten alle Mitarbeiter eine Catch-All-Adresse
Hierbei wird das Prinzip von Catch-All und Subdomain-Stripping kombiniert. Jeder Mitarbeiter von example.com erhält eine eigene Subdomain und die zugehörige Catch-All E-Mail. Als Beispiel: *@mustermann.example.com – Diese E-Mails werden dann an die Mitarbeiter-Adresse mustermann@example.com weitergeleitet.

## Technische Voraussetzung
- Verwaltung der Subdomains für die Domain
- Verwaltung der MX Resource Records der Domain
- E-Mail-Service muss Catch-All und Subdomain-Stripping unterstützen

## Technische Umsetzung
**E-Mail-Server vorbereiten:**
1. Catch-All-Postfach anlegen, z.B. catchall@example.com
2. Catch-All auf catchall@example.com verweisen
3. Subdomain-Stripping aktivieren
4. spam@example.com-Postfach erstellen

## Regelwerk für die korrekte Zustellung
Die E-Mails, die im Postfach catchall@example.com landen, müssen an den richtigen Mitarbeiter weitergeleitet werden. Hierzu werden für jeden Mitarbeiter Regeln definiert:

- Bedingung: ‚To‘ enthält ‚@mitarbeitername.example.com‘<br />
Aktion 1: Leite E-Mail an ‚mitarbeitername@example.com‘<br />
Aktion 2: Lösche E-Mail 


## Weiterführende Infos
- Die mir bekannten Provider mit Subdomain-Stripping sind Zoho und Google Apps for Business.
- Alternativ kann ein Catch-All für eine Subdomain eingerichtet werden; wird z.B. von [Serverprofis](SERVERPROFIS_URL) unterstützt
- [Subdomain-Stripping bei Zoho](https://adminconsole.wiki.zoho.com/set-up/Subdomain-Stripping.html)
- [Catch-All bei Wikipedia](https://de.wikipedia.org/wiki/Catch-All)


*Coverbild von [Ryan McGuire](http://www.mcguiremade.com/)*

```##Tags##: subdomain email catch-all subdomain-stripping spam sicherheit```
