# 币安BNB收益池 – BNB 权益质押 

`##Description##: 关于币安BNB收益池的介绍和信息`
`##Type##: article`
`##Locale##: zh-Hans`

`##Picture##:1067:427:%E5%B0%81%E9%9D%A2-BNB-%E6%9D%83%E7%9B%8A%E8%B4%A8%E6%8A%BC:png:币安BNB收益池 – BNB 权益质押 `

[币安](BINANCE_ZH_URL) BNB收益池于2020年11月3日开始启用，并赋予了币安用家们非常多的优势。我会在以下的文章与大家分享使用币安BNB收益池的优势、如何存入BNB并解答一些常见的问题。

假如您尚未启用币安账户，您可以 [点击这里开始申请](BINANCE_ZH_URL) - 经此连结开启账户的话，您可以获得加入VIP群组的资格并取得额外支援。请 [点击这里](/en/support/vip-group/)以取得更多资料。

- [概述](#概述)
- [优势](#优势)
- [教学](#教学)
  - [将BNB存入BNB收益池(APP使用教学)](#将bnb存入bnb收益池app使用教学)
  - [从BNB收益池提取BNB(APP使用教学)](#从bnb收益池提取bnbapp使用教学)
- [常见问题与解答](#常见问题与解答)
  - [我是否可以随时从BNB收益池提取BNB代币？](#我是否可以随时从bnb收益池提取bnb代币)
  - [在BNB收益池的BNB和在Earn-钱包内的BNB有甚么区别?](#在bnb收益池的bnb和在earn-钱包内的bnb有甚么区别)
  - [我在甚么时候开始收到权益质押的收益?](#我在甚么时候开始收到权益质押的收益)
  - [取得的收入会存放在到哪里？](#取得的收入会存放在到哪里)
  - [我可否将BNB自动存入BNB收益池？](#我可否将bnb自动存入bnb收益池)
  - [我有哪些税务相关的地方需要留意？](#我有哪些税务相关的地方需要留意)

## 概述
BNB是[币安](BINANCE_ZH_URL)链上的原生代币，于2017年首次通过[ICO](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#ico)发售。BNB拥有者可以在交易时享有折扣优势(用BNB进行支付时可以享受２５％的费用折扣)及其他按使用功能获取的奖金。假如你想长期持有BNB并不打算于日内交易中使用，你可以将你的BNB放在BNB收益池里，从而赚取收益和[其他奖金](#vorteile)。


## 优势
于BNB收益池存放你的BNB有以下的优势︰
- 你可以从你存入的BNB赚取收益(约每年5%，截至2021年1月30日)。<br />`##Picture##:713:176:BNB%E6%94%B6%E7%9B%8A:webp:BNB收益`
- 假如你使用[币安信用卡](/krypto/binance/binance-kreditkarte-anleitung-vorstellung-und-test/)，你在金库中的BNB也将加入等级计算。
- 参与[Airdrops](/krypto/abkuerzungen-definitionen-und-begriffe-der-kryptowelt/#airdrop)及新代币的新币挖矿。<br />`##Picture##:639:862:%E5%B8%81%E5%AE%89-%E6%96%B0%E5%B8%81%E6%8C%96%E7%9F%BF:webp:新币挖矿`

## 教学

### 将BNB存入BNB收益池(APP使用教学)
1. 开启[币安](BINANCE_ZH_URL)APP。
2. 于主页点击**更多**键。<br />`##Picture##:706:327:%E5%B8%81%E5%AE%89-APP-%E4%B8%BB%E9%A1%B5-%E6%9B%B4%E5%A4%9A:webp:币安APP主页更多键`
3. 于**金融业务**板面选按**BNB收益池**。<br />`##Picture##:667:420:BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-%E5%B8%81%E5%AE%89-APP:webp:BNB收益池APP`
4. 于BNB收益池主页点击**存入BNB**键。<br />`##Picture##:641:390:BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-%E5%AD%98%E5%85%A5BNB-%E9%94%AE:webp:BNB收益池存入BNB键`
5. 输入希望存入的BNB数量。<br />`##Picture##:688:308:%E8%BE%93%E5%85%A5-%E5%AD%98%E5%85%A5%E7%9A%84BNB%E6%95%B0%E9%87%8F:webp:输入存入的BNB数量`
6. 阅读并勾选同意币安服务协议。<br />`##Picture##:686:166:%E5%B8%81%E5%AE%89-%E6%9C%8D%E5%8A%A1%E5%8D%8F%E8%AE%AE:webp:币安服务协议复选框`
7. 按**确认**键以确认存入。<br />`##Picture##:694:155:%E5%AD%98%E5%85%A5-%E7%A1%AE%E8%AE%A4:webp:存入确认`
8. 成功存入BNB收益池。<br />`##Picture##:644:186:%E6%88%90%E5%8A%9F-%E5%AD%98%E5%85%A5:webp:成功存入`
9. 假如你想系统自动将BNB存入BNB收益池，请激活**自动转入**，这样系统会自动将在现货钱包内的BNB于UTC时间00:00转入至BNB收益池。<br />`##Picture##:643:115:BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-%E8%87%AA%E5%8A%A8-%E8%BD%AC%E5%85%A5:webp:BNB收益池自动转入`


### 从BNB收益池提取BNB(APP使用教学)
1. 开启[币安](BINANCE_ZH_URL)APP。
2. 于主页点击「更多」键。<br />`##Picture##:706:327:%E5%B8%81%E5%AE%89-APP-%E4%B8%BB%E9%A1%B5-%E6%9B%B4%E5%A4%9A:webp:币安APP主页更多键`
3. 于**金融业务**板面选按**BNB收益池**。<br />`##Picture##:667:420:BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-%E5%B8%81%E5%AE%89-APP:webp:BNB收益池APP`
4. 按**取出BNB**键。<br />`##Picture##:635:218:BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-%E5%8F%96%E5%87%BABNB:webp:BNB收益池取出BNB键`
5. 输入希望取出的BNB数量。<br />`##Picture##:699:315:%E8%BE%93%E5%85%A5-%E5%8F%96%E5%87%BA%E7%9A%84BNB%E6%95%B0%E9%87%8F:webp:输入取出的BNB数量`
6. 选择赎回方式。<br />&bull; **快速赎回**︰赎回的BNB会立即转入现货钱包，即日起你将不会赚取任何收益。<br />&bull; **标准赎回**︰赎回的BNB将于明天发放，你仍然可以获取今天的收益。<br />`##Picture##:657:512:%E8%B5%8E%E5%9B%9E-%E6%96%B9%E5%BC%8F:webp:赎回方式`
7. 阅读并勾选同意币安服务协议。<br />`##Picture##:697:161:%E6%9C%8D%E5%8A%A1%E5%8D%8F%E8%AE%AE-%E5%8F%96%E5%87%BA:webp:取出服务协议`
8. 按**确认**键以确认取出。<br />`##Picture##:694:155:%E5%AD%98%E5%85%A5-%E7%A1%AE%E8%AE%A4:webp:取出确认`


## 常见问题与解答

### 我是否可以随时从BNB收益池提取BNB代币？
你可以即时提取总量不多于100,000,000的BNB，并于下一天提取更大数量的BNB。由于100,000,000BNB占已发行的BNB总量的一半，所以简单来说，答案应该是︰「对的。」

### 在BNB收益池的BNB和在Earn-钱包内的BNB有甚么区别?
没有区别，你在你的Earn-投资组合内也可以见到你存入到BNB收益池的BNB。

### 我在甚么时候开始收到权益质押的收益?
存款从第二天的00:00 (以UTC时间计算)开始会被加入到收入计算，收入会从第三天开始被分配。

### 取得的收入会存放在到哪里？
收入每天都会被转入现货钱包。

### 我可否将BNB自动存入BNB收益池？
可以，为此你需要激活「自动转入」功能。激活这个功能后，系统会自动将在现货钱包内的BNB于UTC时间02:00转入BNB收益池。<br />`##Picture##:643:115:BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-%E8%87%AA%E5%8A%A8-%E8%BD%AC%E5%85%A5:webp:BNB收益池自动转入`

### 我有哪些税务相关的地方需要留意？
由于我不是税务顾问，所以抱歉未能够在此提供任何信息。请向你的税务顾问清楚说明你的状况，并进一步了解你的投资会否及在多大程度上适用于税务相关法律。


[➡️ 更多与币安相关的文章](/tag/binance/)


<!-- ##Tags##: 币安 bnb 收益池 权益质押 权益 -->

<ul class="LanguageSelection">
    <li><a href="/krypto/binance/binance-bnb-vault-bnb-staking/">Deutsch</a></li>
</ul>

`##Translation##:de:https://##Domain##/krypto/binance/binance-bnb-vault-bnb-staking/`
`##Translation##:zh-Hans:https://##Domain##/zh/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81/%E5%B8%81%E5%AE%89/%E5%B8%81%E5%AE%89-BNB-%E6%94%B6%E7%9B%8A%E6%B1%A0-BNB-%E6%9D%83%E7%9B%8A%E8%B4%A8%E6%8A%BC/`

