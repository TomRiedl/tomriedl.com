# 币安推荐代码 – 征集链接

`##Description##: 赚取20%返佣，生成币安推荐链接/征集代码`
`##Type##: article`
`##Locale##: zh-Hans`

`##Picture##:1600:907:/wp-content/uploads/2017/12/binance-referral-code:png:生成币安推荐代码征集链接`

在[币安](BINANCE_ZH_URL)，你可以透过推荐他人获得20%的佣金。与[Bitfinex](BITFINEX_URL) ([教学](/version1/anleitung-fuer-neues-bitfinex-affiliate-referral-system/))相似，推荐人能够从被推荐人的交易中获得相应货币的回佣，你亦可以为被推荐人设定返佣比例。

假如你还未有币安账户，并通过我的链接注册开户，你将可以获得参与[VIP群组](/support/vip-gruppe/)的资格。([请点击这里了解更多](/en/support/vip-group/)):<br />[BINANCE_ZH_URL](BINANCE_ZH_URL)

## 教学

1. 登入[币安](BINANCE_ZH_URL)。
2. 于菜单右上方点选账户图像然后点击**返佣**键。<br />`##Picture##:378:465:%E6%95%99%E5%AD%A6-%E5%B8%81%E5%AE%89-%E6%8E%A8%E8%8D%90-%E9%93%BE%E6%8E%A5-%E8%8F%9C%E5%8D%95:webp:币安推荐链接征集计划`
3. 于返佣页面点击**生成推荐链接**键。<br />`##Picture##:498:208:%E7%94%9F%E6%88%90-%E6%8E%A8%E8%8D%90-%E9%93%BE%E6%8E%A5:webp:生成推荐链接`
4. 在出现的对话框中，设置返佣比例并生成新的推荐链接。<br />`##Picture##:399:493:%E5%B8%81%E5%AE%89-%E5%BE%81%E9%9B%86-%E8%AE%A1%E5%88%92-%E9%93%BE%E6%8E%A5-%E7%94%9F%E6%88%90:webp:币安征集链接伙伴朋友`
5. 将链接分享给朋友。

<!-- ##Tags##: 征集 币安 代码 计划 推荐 征集-系统 朋友 注册 -->

<ul class="LanguageSelection">
    <li><a href="/en/crypto/binance/binance-referral-code-affiliate-link/">English</a></li>
    <li><a href="/version1/binance-referral-code-affiliate-link-wo-finde-ich-ihn/">Deutsch</a></li>
</ul>

`##Translation##:de:https://##Domain##/version1/binance-referral-code-affiliate-link-wo-finde-ich-ihn/`
`##Translation##:en:https://##Domain##/en/crypto/binance/binance-referral-code-affiliate-link/`
`##Translation##:zh-Hans:https://##Domain##/zh/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81/%E5%B8%81%E5%AE%89/%E5%B8%81%E5%AE%89-%E6%8E%A8%E8%8D%90-%E4%BB%A3%E7%A0%81-%E5%BE%81%E9%9B%86-%E9%93%BE%E6%8E%A5/`
