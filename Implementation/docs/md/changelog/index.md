# Changelog

`##Description##: Alle Änderungen der Website tomriedl.com`
`##Type##: website`
`##Locale##: de-DE`

Hier siehst du alle Änderungen der Website.<br />
Die Quelltexte des Projekts liegen unter <https://gitlab.com/TomRiedl/tomriedl.com>.

Offene Issues ansehen oder neue eintragen kannst du unter <https://gitlab.com/TomRiedl/tomriedl.com/-/issues>

`##Tags##: changelog`