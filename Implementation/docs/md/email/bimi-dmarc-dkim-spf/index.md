# BIMI, DMARC, DKIM und SPF für E-Mails - Anleitung zum Einrichten und Testen

`##Description##: Absicherung des eigenen E-Mail-Verkehrs und zur Spam-Reduzierung`
`##Type##: article`
`##Locale##: de-DE`

`##Style##:table { min-width:initial !important; width: 100%; }`
`##Style##:table td { vertical-align: top; width: auto; }`
`##Style##:table th:nth-child(1) { width: 15% }`
`##Style##:table th:nth-child(2) { width: 40% }`
`##Style##:table th:nth-child(3) { width: 44% }`

`##Picture##:700:700:cover-bimi-dmarc-dkim-spf:webp,png:BIMI, DMARC, DKIM und SPF - Anleitung zum Einrichten und Testen`

E-Mail ist eine alte Technologie, hat aber über die Jahre viele Erweiterungen erhalten. Dennoch gibt es immer wieder die Frage "Ist die E-Mail, die ich gerade erhalten habe wirklich von einem vertrauenswürdigen Absender oder nicht?".
BIMI, DMARC, DKIM, SPF und auch GPG oder S/MIME sind dabei gute Wege, um zumindest seine eigene Domain, sei es privat oder geschäftlich vor Misbrauch zu schützen. Ich empfehle wirklich jeder Firma und auch jeder Privatperson mit eigener Domain, die vier wichtigsten Absicherungen umzusetzen. Wie das funktioniert, erkläre ich dir in diesem Artikel am Beispiel meiner Domain [tomriedl.com](https://tomriedl.com/).

Weißt du schon, was BIMI, DMARC, DKIM und SPF ist, kannst du gleich [mit der Einrichtung fortfahren](#spf-einrichten). Die empfohlene Reihenfolge ist dabei:
1. [SPF einrichten](#spf-einrichten)
2. [DKIM einrichten](#dkim-einrichten)
3. [DMARC einrichten](#dmarc-einrichten)
4. [BIMI einrichten](#bimi-einrichten)

Inhalt des Artikels:
- [Allgemeine Informationen und Hintergrundwissen](#allgemeine-informationen-und-hintergrundwissen)
  - [Was ist SPF (Sender Policy Framework)](#was-ist-spf-sender-policy-framework)
  - [Was ist DKIM (Domain Keys Identified Mail)](#was-ist-dkim-domain-keys-identified-mail)
  - [Was ist DMARC (Domain-based Message Authentication, Reporting and Conformance)](#was-ist-dmarc-domain-based-message-authentication-reporting-and-conformance)
  - [Was ist BIMI (Brand Indicators for Message Identification)](#was-ist-bimi-brand-indicators-for-message-identification)
- [SPF einrichten](#spf-einrichten)
- [DKIM einrichten](#dkim-einrichten)
  - [Schritt 1: Prüfen, ob DKIM bereits aktiviert ist](#schritt-1-prüfen-ob-dkim-bereits-aktiviert-ist)
  - [Schritt 2: DKIM konfigurieren](#schritt-2-dkim-konfigurieren)
- [DMARC einrichten](#dmarc-einrichten)
- [BIMI einrichten](#bimi-einrichten)
  - [BIMI-Logo vorbereiten](#bimi-logo-vorbereiten)
- [Signierung der E-Mails](#signierung-der-e-mails)
- [Überprüfung der Zustellbarkeit](#überprüfung-der-zustellbarkeit)
  - [Überprüfung durch MxToolBox](#überprüfung-durch-mxtoolbox)
  - [Überprüfung durch Google](#überprüfung-durch-google)

## Allgemeine Informationen und Hintergrundwissen
Den Versand von Spam kann man leider nicht verhindern, jedoch kann man den empfangenden E-Mailservern Informationen geben, ob die empfangene E-Mail wirklich von einem selbst ist oder von jemand anderem. Wurde erkannt, dass die E-Mail von einem Spammer, Scammer oder Phisher kommt, sollte diese entweder gleich gelöscht oder in den Spam-Ordner verschoben werden. Die hier beschriebenen Technologien helfen dabei, den Empfangsservern Informationen zu geben, ob die E-Mail von dir oder jemand anderen ist.

Dabei haben die Technologien folgende Aufgaben:
- SPF legt fest, wer E-Mails versenden darf.
- DKIM stellt sicher, dass die E-Mail nachweislich und unverändert vom richtigen Server gesendet wurde.
- DMARC legt fest, was mit E-Mails passieren soll, die durch die SPF- und DKIM-Prüfung fallen.
- BIMI hilft mit der Darstellung des eigenen Logos zu visualisieren, dass es sich um einen valide und unveränderte E-Mail handelt.

Idealerweise solltest du alle vier Konfigurationen vornehmen.

### Was ist SPF (Sender Policy Framework)
Das Sender Policy Framework, kurz SPF, legt fest, welche Server bzw. Domains E-Mails versenden dürfen. Hierbei wird lediglich ein TXT-Record zur Domain hinzugefügt. [Zur Anleitung](#spf-einrichten)

Weiterführende Informationen:
- [Wikipedia "Sender Policy Framework"](https://de.wikipedia.org/wiki/Sender_Policy_Framework)
- [IETF RFC 7208: Sender Policy Framework (SPF) for Authorizing Use of Domains in Email](https://datatracker.ietf.org/doc/html/rfc7208)

### Was ist DKIM (Domain Keys Identified Mail)
Bei DKIM signiert der sendende E-Mail-Server die E-Mail mit einem nur ihm bekannten privaten Schlüssel. Der empfangende Server hat nun die Möglichkeit, die Signatur zu überprüfen. Der öffentliche Schlüssel zur Überprüfung wird dabei bei der Domain als TXT-Eintrag hinterlegt und ist somit zur Überprüfung für jeden E-Mailserver zugänglich.

Weiterführende Informationen:
- [Wikipedia "DomainKeys"](https://de.wikipedia.org/wiki/DomainKeys)
- [IETF RFC 4871: DomainKeys Identified Mail (DKIM) Signatures](https://datatracker.ietf.org/doc/html/rfc4871)

### Was ist DMARC (Domain-based Message Authentication, Reporting and Conformance)
DMARC legt fest, wie E-Mails, die durch die SPF- und DKIM-Prüfung fallen behandelt werden sollen. Weiterhin können Empfangsserver Berichte erstellen und diese an die angegebene E-Mail-Adressen senden. Es kann zwischen `none`, `quarantine` und `reject` unterschieden werden. Bei `none` werden die E-Mails zugestellt und dient lediglich der Berichterstellung, bei `quarantine` sollen die ungültigen Mails in die Quarantäne/Spam-Ordner verschoben werden und bei `reject` direkt abgelehnt werden.

Weiterführende Informationen:
- [Wikipedia "DMARC"](https://de.wikipedia.org/wiki/DMARC)
- [IETF RFC 7489: Domain-based Message Authentication, Reporting, and Conformance (DMARC)](https://datatracker.ietf.org/doc/html/rfc7489)

### Was ist BIMI (Brand Indicators for Message Identification)
BIMI ist eine Spezifikation, die von Mailgrößen wie Google, mailchimp, SendGrid und einigen anderen ins Leben gerufen wurde. Durch die Konfiguration von BIMI wird im Empfängerpostfach dein Logo neben deiner E-Mail angezeigt. Dies jedoch nur, wenn die SPF- und DKIM-Prüfung korrekt war und DMARC auf `quarantine` oder `reject` gesetzt ist. Damit kann der Empfänger sicher sein, dass die E-Mail aus einer vertrauenswürdigen Quelle stammt.

Weiterführende Informationen:
- [BIMI Group](https://bimigroup.org/)
- [Wikipedia "BIMI" (Englisch)](https://en.wikipedia.org/wiki/Brand_Indicators_for_Message_Identification)

## SPF einrichten
*Voraussetzung: Du musst für deine Domain einen TXT Resource Record anlegen können. Mittlerweile unterstützt das fast jeder Hoster, suche hierzu einfach nach "TXT Record DerNameDesHosters" in einer Suchmaschine deiner Wahl.*

1. Prüfe zuerst, ob deine Domain nicht bereits für SPF konfiguriert ist. Manche Hoster übernehmen das automatisch, wie auch mein Hoster [Serverprofis](SERVERPROFIS_URL). Sollte es bereits konfiguriert sein, fahre mit der [Einrichtung von DKIM](#dkim-einrichten) fort. Überprüfen kannst du das auf dieser Seite: [MxToolBox SPF Record Check](https://mxtoolbox.com/spf.aspx)
1. Logge dich bei deinem Hoster ein.
2. Öffne die DNS bzw. Resource-Records-Einstellungen für deine Domain.
3. Füge einen neuen TXT-Eintrag hinzu.<br />`##Picture##:381:419:txt-record-hinzufuegen:webp:Neuen TXT RR-Eintrag hinzufügen`
4. Trage nun folgende Daten ein:<br />**Zonenname:** Bleibt leer, bitte nichts eintragen<br />**TTL:** beliebig, Standardwert z.B. `14400`<br />**Typ:** `TXT`<br />**Wert:** Der SPF-Eintrag, genauere Details im folgenden Text.

Der einfachste Weg zur korrekten Erzeugung des SPF-Wertes ist ein Generator. Empfehlen kann ich dabei den [SPF-Generator von MxToolBox](https://mxtoolbox.com/SPFRecordGenerator.aspx):

1. Deine Domain eintragen und auf **Check SPF record** klicken<br />`##Picture##:511:159:spf-record-generator-domain:webp:Domain eintragen und überprüfen`
2. Fülle nun die relevanten Punkte aus:<br />`##Picture##:807:425:spf-wizard-formular:webp:SPF-Generator Formular`

**1) Do you send email from your webserver?** Wähle hier **Yes**, wenn dein Webserver z.B. über ein Formular E-Mails versendet. Häufig ist das bei PHP-Formularen der Fall. Idealerweise solltest du alle Formulare so konfigurieren, dass sie Mails nur über SMTP versenden und nicht über den Webserver. Ist das bei dir der Fall, wähle hier **No**.<br /><br />
**2) Do you send email from the same server in your MX records?** Wähle hier **Yes**, wenn die Server in den MX-Records ebenfalls E-Mails versenden. Wenn du hier nicht sicher bist, wähle einfach **Yes**.<br /><br />
**3) Enter any other server hostname or domain that delivers email for your domain.** Hier kannst du optional andere Domains von Servern eintragen, welche E-Mails für dich versenden.<br /><br />
**4) Enter your domain's IPv4 Addresses / CIDR Ranges.** Trage hier die IP des Servers ein, auf den die Domain zeigt. Sind das mehrere Server, trägst du diese mit Komma getrennt ein.<br /><br />
**5) How strict should the SPF Policy be?** Kommt eine E-Mail von einem Server, der nicht in der erlaubten Liste ist, greift die letzte Regel. Hier kannst du zwischen folgenden Optionen wählen:<br />
&bull; Strict: Alle Mails von Servern, die nicht im SPF-Eintrag erlaubt sind sollen abgelehnt werden.<br />
&bull; Soft Fail: Ist ein Sender nicht im SPF-Eintrag erlaubt, soll die E-Mail zwar akzeptiert aber markiert werden.<br />
&bull; Neutral: E-Mail wird akzeptiert, da keine genaueren Informationen vorliegen.<br />
Für den ersten Start empfehle ich **Soft Fail**, um zu überprüfen, ob alles richtig konfiguriert ist. Funktioniert alles, sollte der Eintrag auf **Strict** gesetzt werden.

Unten wird nun der erzeugte Wert für den TXT-Record angezeigt. Kopiere und speichere diesen wie oben beschrieben als TXT-Eintrag deiner Domain.

Der Eintrag für tomriedl.com sieht z.B. folgendermaßen aus:<br />`v=spf1 ip4:195.30.85.80 include:mailout1.sp-server.net +a +mx -all`

Überprüfe den Eintrag über den [MxToolBox SPF Record Check](https://mxtoolbox.com/spf.aspx). Lies dir auch weitere Überprüfungsmethoden unter [Überprüfung der Zustellbarkeit](#überprüfung-der-zustellbarkeit) durch.

Weiterführende Informationen:
- [Wikipedia "Sender Policy Framework"](https://de.wikipedia.org/wiki/Sender_Policy_Framework)
- [IETF RFC 7208: Sender Policy Framework (SPF) for Authorizing Use of Domains in Email](https://datatracker.ietf.org/doc/html/rfc7208)

## DKIM einrichten
*Voraussetzung: Dein E-Mailserver muss DKIM unterstützen und du musst für deine Domain einen TXT Resource Record anlegen können.*

Die Einrichtung von DKIM ist im Vergleich zu [SPF](#spf-einrichten) deutlich einfacher, denn der E-Mailserver bzw. das Konfigurationspanel generiert den DKIM-Eintrag für dich. Der private Schlüssel zur Signierung wird hierbei auf dem Server selbst erzeugt und verlässt diesen auch nicht. Der öffentliche Schlüssel, der dann als Resource-Record verwendet wird, wird dagegen im Panel angezeigt.

Im folgenden die Beschreibung und Screenshots von cPanel beim Hoster [Serverprofis](SERVERPROFIS_URL). Das Prinzip ist bei den anderen Konfigurationspanels ähnlich und diese Anleitung sollte somit übertragbar sein.

### Schritt 1: Prüfen, ob DKIM bereits aktiviert ist
Prüfe zuerst, ob DKIM bei dir bereits eingerichtet ist. Hierzu gehst du folgendermaßen vor:
1. Sende eine E-Mail an eine E-Mailadresse einer anderen Domain auf die du Zugriff hast (z.B. auf dein Privat- oder Firmenkonto). Eine andere Domain deshalb, da bei gleicher Domain E-Mails nur intern erzeugt werden und häufig nicht signiert werden.
2. Öffne den Quelltext der E-Mail bzw. lasse dir die Header anzeigen.<br />`##Picture##:334:319:e-mail-quelltext-anzeigen:webp:Quelltext E-Mail anzeigen`
3. Suche nach dem Header-Eintrag **DKIM-Signature**. Ist dieser nicht vorhanden, [fahre mit Schritt 2 fort](#schritt-2-dkim-konfigurieren).
4. Kopiere den Selector-Wert bei `s=`. Steht dort z.B. `s=email`, kopiere `email`.<br />`##Picture##:622:108:dkim-header-e-mail-quelltext:webp:DKIM-Header in E-Mail Quelltext`
5. Öffne das [DKIM-Werkzeug der MxToolBox](https://mxtoolbox.com/dkim.aspx).
6. Trage deine Domain, sowie den kopierten Selector ein und klicke auf **DKIM Lookup**.<br />`##Picture##:854:164:mxtoolbox-dkim-record-lookup:webp:DKIM-Lookup bei MXToolBox`
7. Ist alles richtig konfiguriert, wird dies mit drei grünen Haken bestätigt (DKIM Record Published, DKIM Syntax Check, DKIM Public Key Check) und du kannst mit der [DMARC-Konfiguration fortfahren](#dmarc-einrichten). Werden Fehler angezeigt, gehe weiter zu [DKIM Schritt 2](#schritt-2-dkim-konfigurieren).<br />`##Picture##:1082:559:erfolgreiche-dkim-konfiguration:webp:Erfolgreiche DKIM-Konfiguration`

### Schritt 2: DKIM konfigurieren
3. Beim Hoster oder E-Mail-Dashboard einloggen.
4. Die Domain auswählen und den Bereich DKIM/E-Mail-Signierung/E-Mail-Zustellbarkeit/etc. öffnen.<br />`##Picture##:503:395:cpanel-email-zustellbarkeit:webp:Menüpunkt E-Mail Zustellbarkeit`
5. Den DKIM-Eintrag kopieren bzw. überprüfe, ob dieser bereits korrekt gesetzt ist (viele Hoster setzen diesen Eintrag bereits automatisch, siehe auch [Schritt 1](#schritt-1-prüfen-ob-dkim-bereits-aktiviert-ist))<br />`##Picture##:642:260:dkim-eintrag-kopieren:webp:DKIM-Eintrag kopieren`
6. Öffne die DNS/Resource-Record-Konfiguration deiner Domainverwaltung.
7. Füge einen neuen TXT-Eintrag hinzu.<br />`##Picture##:381:419:txt-record-hinzufuegen:webp:Neuen TXT RR-Eintrag hinzufügen`
8. Trage nun folgende Daten ein:<br />**Zonenname:** Der Selektorname mit der Endung `._domainkey`<br />**TTL:** beliebig, Standardwert z.B. *14400*<br />**Typ:** TXT<br />**Wert:** Der DKIM-Eintrag, den du zuvor kopiert hast. Dieser hat das Format<br />`v=DKIM1; k=rsa; p=<public-key>`
9. Speichere den neuen TXT-Eintrag.
10. Gehe zu [Schritt 1](#schritt-1-prüfen-ob-dkim-bereits-aktiviert-ist) und überprüfe, ob DKIM richtig konfiguriert ist.

Weiterführende Informationen:
- [Wikipedia "DomainKeys"](https://de.wikipedia.org/wiki/DomainKeys)
- [IETF RFC 4871: DomainKeys Identified Mail (DKIM) Signatures](https://datatracker.ietf.org/doc/html/rfc4871)

## DMARC einrichten
*Voraussetzung: [SPF](#spf-einrichten) und [DKIM](#dkim-einrichten) müssen zuvor eingerichtet sein und du musst für deine Domain einen TXT Resource Record anlegen können.*

Viele Hoster bieten für die Domains bereits einen DMARC-Assistenten, wie auch mein Hoster [Serverprofis](SERVERPROFIS_URL), im folgenden beschreibe ich die manuelle Konfiguration, falls es einen solchen Assistenten nicht gibt:

1. Beim Hoster oder Domain-Panel einloggen.
2. Die Domain auswählen und zur DNS- bzw. Resource-Records-Einstellungen wechseln.
3. Füge einen neuen TXT-Eintrag hinzu.<br />`##Picture##:381:419:txt-record-hinzufuegen:webp:Neuen TXT RR-Eintrag hinzufügen`
4. Trage nun folgende Daten ein:<br />**Zonenname:** `_dmarc`<br />**TTL:** beliebig, Standardwert z.B. `14400`<br />**Typ:** `TXT`<br />**Wert:** Der DMARC-Eintrag, genauere Details im folgenden Text oder verwende einen [Assistenten](https://mxtoolbox.com/DMARCRecordGenerator.aspx).<br />`##Picture##:963:100:dmarc-tomriedl:webp:DMARC bei tomriedl.com`
5. Speichere den neuen TXT-Eintrag.

Wenn du einen Generator für den TXT-Eintrag verwenden willst, kann ich dir [MxToolBox DMARC Record Generator](https://mxtoolbox.com/DMARCRecordGenerator.aspx) empfehlen. Für die manuelle Einrichtung verwende folgende Konfigurationstabelle.

Der Eintrag hat folgendes Format: `v=DMARC1; p=1; sp=2; pct=3; adkim=4; aspf=5; fo=6; ruf=7; rua=8; ri=9`. Die Werte für 1-9 werden in der folgenden Tabelle genauer beschrieben:

| Nr. (Key) | Wert | Beschreibung |
| :--- | :--- | :--- |
| 1 (p) | &bull; `none`<br />&bull; `quarantine`<br />&bull; `reject` | Pflichtfeld: Vorschrift, wie bei einer ungültigen E-Mail der Hauptdomain vorgegangen werden soll.<br />&bull; none: Hier wird nichts unternommen, ist lediglich für eine Berichterstellung.<br />&bull; quarantine: Ungültige E-Mail wird in die Quarantäne/Spam-Ordner verschoben.<br />&bull; reject: E-Mail wird sofort abgewiesen und erreicht das Zielpostfach nicht (empfohlen). |
| 2 (sp) | &bull; `none`<br />&bull; `quarantine`<br />&bull; `reject` | Optional: Vorschrift, wie bei einer ungültigen E-Mail der Subdomain vorgegangen werden soll. |
| 3 (pct) | `1` - `100`<br />Standard: `100` | Optional: Prozentwert, bei wievielen E-Mails die DMARC-Regel angewendet werden sollen. |
| 4 (adkim) | &bull; `r` (relaxed)<br />&bull;`s` (strict)<br />Standard: `r` | Optional: Alignment für DKIM, welcher festlegt, ob die Domain inkl. Subdomain übereinstimmen muss. |
| 5 (aspf) | &bull; `r` (relaxed)<br />&bull;`s` (strict)<br />Standard: `r` | Optional: Alignment für SPF, welcher festlegt, ob die Domain inkl. Subdomain übereinstimmen muss. |
| 6 (fo) | &bull; `0` (Bericht, wenn SPF+DKIM fehlschlagen)<br />&bull;`1` (Bericht, wenn SPF oder DKIM fehlschlagen)<br />&bull;`s` (Bericht, wenn SPF fehlschlägt)<br />&bull;`d` (Bericht, wenn DKIM fehlschlägt) | Optional: Hier kannst du festlegen, in welcher Situation Daten für einen Bericht gesammelt werden sollen. |
| 7 (ruf) | `mailto:mail@example.com` | Optional: E-Mail-Adresse, an den der forensische Bericht gesendet werden soll. |
| 8 (rua) | `mailto:mail@example.com` | Optional: E-Mail-Adresse, an den der aggregierte Bericht gesendet werden soll. |
| 9 (ri) | Häufigkeit der Berichte in Sekunden. Standard: `86400` (ein Tag) | Optional: Die Häufigkeit, mit der die Berichte gesendet werden. Kleinere Werte als einmal am Tag werden eventuell von den Mailanbietern ignoriert. |

Um sich selbst vor Spam zu schützen, sollte man für `ruf` und `rua` keine E-Mail verwenden, die man im Alltag verwendet. Entweder man richtet ein eigenes Postfach ein, verwendet Services, wie [EasyDMARC](https://easydmarc.com/) (kostenloser Tarif vorhanden) oder [MxToolBox Delivery Center](https://mxtoolbox.com/c/products/deliverycenter) (ab $99 pro Monat) oder nutzt OpenSource-Lösungen, wie [dmarc-visualizer](https://github.com/debricked/dmarc-visualizer) oder [andere](https://github.com/topics/dmarc-reports).

Nachdem du DMARC eingerichtet hast, solltest du zwingend die Zustellung testen. Siehe hierzu den Abschnitt [Überprüfung der Zustellbarkeit](#überprüfung-der-zustellbarkeit).

Weiterführende Informationen:
- [Wikipedia "DMARC"](https://de.wikipedia.org/wiki/DMARC)
- [IETF RFC 7489: Domain-based Message Authentication, Reporting, and Conformance (DMARC)](https://datatracker.ietf.org/doc/html/rfc7489)

## BIMI einrichten
*Voraussetzung: [SPF](#spf-einrichten) und [DKIM](#dkim-einrichten) müssen zuvor eingerichtet sein, [DMARC](#dmarc-einrichten) auf quarantine/reject gesetzt sein, du musst für deine Domain einen TXT Resource Record anlegen können und du benötigst dein Logo als SVG.*

Die BIMI-Einrichtung erfordert drei Schritte, 1. Vorbereitung des Logos, 2. Upload des Logos und 3. Konfiguration des TXT-Records.

### BIMI-Logo vorbereiten
1. Das BIMI-Logo muss ein [SVG](https://de.wikipedia.org/wiki/Scalable_Vector_Graphics) sein. Je nachdem in welchem Format es verfügbar ist, kannst du es meist aus der zugehörigen Software exportieren, Voraussetzung ist jedoch, dass es in einem Vektorenformat vorliegt. Ist dein Logo schon als .svg oder .pdf, .ai oder .eps verfügbar, empfiehlt es sich, die Datei als *Plain SVG*  mit [Inkscape](https://inkscape.org/de/) zu speichern.<br />`##Picture##:856:634:svg-export-bimi:webp:Plain SVG Export Logo`
2. Nachdem du ein *Plain SVG* vorliegen hast, solltest du es mit dem Online-Service [SVGOMG](https://jakearchibald.github.io/svgomg/) optimieren. Die Standardeinstellungen sind hierbei ausreichend.<br />`##Picture##:718:513:svgomg-optimierung:webp:Optimierung mit SVGOMG`
3. Im nächsten Schritt muss das SVG so angepasst werden, dass es den BIMI-Spezifikationen entspricht (SVG Tiny Portable/Secure-Format). Am einfachsten funktioniert dies über den [EasyDMARC BIMI Logo Converter](https://easydmarc.com/tools/bimi-logo-converter). Bei mir hat dieser Konverter allerdings nicht gut funktioniert und habe es deshalb händisch vornehmen müssen (Erklärung ab Schritt 4). 
4. Die folgenden Schritte musst du nur vornehmen, falls der Online-Konverter nicht richtig funktioniert hat (weiter bei 9. falls der Konverter funktioniert hat):
5. Öffne das SVG in einem Text-Editor und füge im svg-Tag `version="1.2"` und `baseProfile="tiny-ps"` hinzu.<br />`##Picture##:1166:136:svg-version-baseprofile-bimi:webp:SVG Version und Base-Profile`
6. Füge direkt nach dem öffnenden svg-Tag das title-Tag hinzu: `<title>Dein Name bzw. dein Firmenname</title>`<br />`##Picture##:651:125:svg-titel-bimi:webp:Titel-Tag für BIMI-Logo`
7. Als nächstes muss das SVG gegen die BIMI-Spezifikation geprüft werden. Hierzu nutzt du am besten [Docker](https://www.docker.com/products/docker-desktop). Führe den folgenden Befehl im gleichen Verzeichnis aus, in dem die SVG-Datei liegt. Achte darauf, dass in diesem Verzeichnis nur eine .svg-Datei liegt:
   1. **Linux:** `docker run -it -v "$(pwd):/wd" -w /wd alpine:latest /bin/sh -c "apk add curl openjdk11 python3 py3-pip && pip3 install jingtrang && curl https://bimigroup.org/resources/SVG_PS-latest.rnc.txt > /tmp/spec.txt && pyjing -c /tmp/spec.txt *.svg"`
   1. **Windows:** `docker run -it -v "$(pwd):/wd" -w /wd alpine:latest /bin/sh -c "apk add curl openjdk11 python3 py3-pip && pip3 install jingtrang && curl https://bimigroup.org/resources/SVG_PS-latest.rnc.txt > /tmp/spec.txt && pyjing -c /tmp/spec.txt *.svg"`
   1. **MacOS:** `docker run -it -v "$(pwd):/wd" -w /wd alpine:latest /bin/sh -c "apk add curl openjdk11 python3 py3-pip && pip3 install jingtrang && curl https://bimigroup.org/resources/SVG_PS-latest.rnc.txt > /tmp/spec.txt && pyjing -c /tmp/spec.txt *.svg"`
8. Sollten bei der SVG-Prüfung Fehler angezeigt werden korrigiere diese.
9. Lade die SVG-Datei auf deinen Webserver hoch. Die Datei sollte dabei über die gleiche Domain, wie deine E-Mails, erreichbar sein. Bei mir ist das <https://tomriedl.com/logo-tom-riedl-bimi.svg>
10. Als nächstes muss der BIMI-RR-Eintrag deiner Domain angelegt werden. Logge dich hierzu beim Hoster oder Domain-Panel ein.
11. Wähle deine Domain aus und wechsle zu den DNS- bzw. Resource-Records-Einstellungen.
12. Füge einen neuen TXT-Eintrag hinzu.<br />`##Picture##:381:419:txt-record-hinzufuegen:webp:Neuen TXT RR-Eintrag hinzufügen`
13. Trage nun folgende Daten ein:<br />**Zonenname:** `default._bimi`<br />**TTL:** beliebig, Standardwert z.B. `14400`<br />**Typ:** `TXT`<br />**Wert:** `v=BIMI1; l=https://example.com/pfad/zu/deiner/svg-datei.svg;`<br />`##Picture##:953:118:bimi-txt-eintrag:webp:BIMI TXT-Eintrag`
14. Speichere den neuen TXT-Eintrag.
15. Prüfe den BIMI-Eintrag mit dem [BIMI Inspector](https://bimigroup.org/bimi-generator/) oder dem [MxToolBox BIMI Check Tool](https://mxtoolbox.com/bimi.aspx).
16. Ist alles richtig eingerichtet, wird dein Logo bei den unterstützen E-Mail-Clients angezeigt (besonders zu empfehlen ist hier [FairEmail](https://email.faircode.eu/)):<br />`##Picture##:652:434:fairemail-bimi-screenshot:webp:Screenshot BIMI FairEmail`

Weiterführende Informationen:
- [BIMI Group](https://bimigroup.org/)
- [Wikipedia "BIMI" (Englisch)](https://en.wikipedia.org/wiki/Brand_Indicators_for_Message_Identification)

## Signierung der E-Mails
E-Mails sollten nicht nur vom Server über DKIM, sondern auch auf dem Gerät, auf dem du die E-Mail schreibst mit deinem persönlichen Schlüssel signiert werden. Hierfür gibt es:
- [OpenPGP](https://www.openpgp.org/) (kostenlos)
- [S/MIME](https://de.wikipedia.org/wiki/S/MIME) (meist kostenpflichtig)

Weiterhin hast du mit OpenPGP und S/MIME die Möglichkeit E-Mails zu verschlüsseln.

Die beiden Arten der Signierung/Verschlüsselung würden allerdings den Rahmen dieses Artikels sprengen. Ich plane deshalb einen weiteren Artikel, um auch hier eine Anleitung zur Verfügung zu stellen.

## Überprüfung der Zustellbarkeit

### Überprüfung durch MxToolBox
Der einfachste Weg um die Zustellbarkeit zu überprüfen ist, eine E-Mail mit beliebigem Betreff und Text an [ping@tools.mxtoolbox.com](mailto:ping@tools.mxtoolbox.com) zu senden. Innerhalb weniger Minuten bekommst du dann das Prüfergebnis:<br />`##Picture##:571:175:mxtoolbox-dmarc-ping-email:webp:MxToolBox DMARC Pink E-Mail`

### Überprüfung durch Google
Zur Überprüfung über Google sendest du von deinem E-Mailkonto eine E-Mail an deine Gmail-Adresse:
1. Sende eine E-Mail an deine *@gmail.com-Adresse.
2. Öffne die E-Mail in Gmail.
3. Klicke auf das Symbol **&#8942;** rechts neben dem Datum der Mail.<br />`##Picture##:355:297:email-optionen-gmail:webp:Burger-Menü Google Mail`
4. Wähle den Menüpunkt **Original anzeigen**<br />`##Picture##:358:461:gmail-originalnachricht-anzeigen:webp:Originalnachricht anzeigen`
5. Überprüfe, ob bei den Punkten SPF, DKIM und DMARC jeweils **PASS** steht.<br />`##Picture##:865:452:gmail-test-spf-dkim-dmarc:webp:Testergebnis Gmail für SPF, DKIM und DMARC`

`##Tags##: email bimi dmarc dkim spf gpg spam phishing scam`
