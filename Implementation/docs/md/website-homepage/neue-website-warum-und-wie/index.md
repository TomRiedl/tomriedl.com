# Neue Website - Warum und wie

`##Description##: Eine technische Übersicht, wie die Website tomriedl.com erzeugt wurde`
`##Type##: article`
`##Locale##: de-DE`

<img src="/logo-200.png" style="float:left;margin-right:1em;width:100px;height:100px;" alt="Tom Riedl Logo klein" />

Die alte Version war mit WordPress aufgesetzt. In Zeiten von Progressive Web Apps (PWA), Headless-CMS und REST-APIs gehört WordPress wohl eher zu einem Relikt vergangener Zeiten. Den Umbau hatte ich also schon lange vor und habe ihn nun endlich umgesetzt. Wie die aktuelle Website aufgesetzt ist, erkläre ich in diesem Artikel.

## Kurzzusammenfassung

| Kategorie | Umsetzung |
| :--- | :--- |
| Quelltexte | [gitlab.com/TomRiedl/tomriedl.com](https://gitlab.com/TomRiedl/tomriedl.com) |
| Tickets | [gitlab.com/TomRiedl/tomriedl.com/-/issues](https://gitlab.com/TomRiedl/tomriedl.com/-/issues) |
| Hosting | [Serverprofis](SERVERPROFIS_URL) |
| TLS | [Let's Encrypt](https://letsencrypt.org/) |
| CMS/Framework | [Codedoc](https://codedoc.cc/) |
| Tools | [Visual Studio Code](https://code.visualstudio.com/), [GitKraken](https://www.gitkraken.com/invite/8dEAeKHi), [GitLab Web-IDE](https://docs.gitlab.com/ee/user/project/web_ide/) |

## Vorher - Nachher

**Screenshot der alten Version**

![Screenshot tomriedl.com alte Version](screenshot-tomriedl.com-alt-h300.png)

**Screenshots der aktuellen Version**

![Screenshot tomriedl.com neue Version dunkel](screenshot-tomriedl.com-neu-dark-h300.png) ![Screenshot tomriedl.com neue Version hell](screenshot-tomriedl.com-neu-light-h300.png)

| Kategorie | Vorher | Nachher |
| :--- | :--- | :--- |
| Website-Technologie | &bull; WordPress, PHP | &bull; Statische Website ohne server-side Rendering<br />&bull; [Codedoc](https://codedoc.cc/) |
| Verwaltung der Inhalte | &bull; Admin-Panel von WordPress | &bull; Alle Artikel/Seiten liegen als Markdown-Dateien in einem [Repository bei GitLab](https://gitlab.com/TomRiedl/tomriedl.com)<br />&bull; Änderungen entweder lokal und Git Commit/Push oder [GitLab-Web-IDE](https://docs.gitlab.com/ee/user/project/web_ide/) |
| Versionierung | &bull; keine | &bull; [Semantic Versioning](https://semver.org/lang/de/) |
| Nachverfolgbarkeit der Änderungen | &bull; keine | &bull; [Changelog](/changelog/)<br />&bull; [Versionshistorie](https://gitlab.com/TomRiedl/tomriedl.com/-/releases) |
| Progressive Web-App | &bull; keine | &bull; in Arbeit |


**Lighthouse-Tests**

Die Tests mit [Lighthouse](https://developers.google.com/web/tools/lighthouse) zeigen eine deutlichere Verbesserung. Mit der Optimierung bin ich allerdings noch nicht ganz fertig, da kann ich sicherlich noch etwas mehr optimieren.

| Kategorie | Vorher | Nachher |
| :--- | :--- | :--- |
| Mobile | ![Lighthouse Mobil vorher](lighthouse-old-mobile.png) | ![Lighthouse Mobil nachher](lighthouse-new-mobile.png) |
| Desktop | ![Lighthouse Desktop vorher](lighthouse-old-desktop.png) | ![Lighthouse Desktop nachher](lighthouse-new-desktop.png) |


## Design
Design kann man es wohl nicht nennen; für den designlosen und schlichten Ansatz habe ich mich bewusst entschieden. Diese Website soll Informationen liefern, da ist ein hübsches Design zwar nett aber unnötig. Jedoch: Die Website unterstützt einen Dark- und Light-Mode (kann durch einen Klick rechts oben geändert werden).

## Hosting
Die Homepage wird bei [Serverprofis](SERVERPROFIS_URL) gehostet. Nachdem ich schon viele Hoster durchprobiert habe, wie All-Inkl, 1&1, HostEurope, DomainFactory und Strato, bin ich nun schon seit einigen Jahren zufrieden bei [Serverprofis](SERVERPROFIS_URL) und [Hetzner](https://www.hetzner.com/de/). Websites hoste ich bei Serverprofis, da deren Preis-Leistung sehr gut ist, der Support schnell und kompetent ist und sie kostenlos die Homepages mit TLS-Zertifikaten von Let's Encrypt versehen (leider ist das bei vielen anderen Hostern noch kein Standard und man muss für die Zertifikate noch extra zahlen). Für Docker-Hosting und andere Serveranwendungen verwende ich die [Hetzner Cloud](https://www.hetzner.com/de/cloud) (wird aber für tomriedl.com nicht benötigt).


## Cookies und Analytics
Warum kommt kein Cookie-Banner? Weil keine Cookies gespeichert werden. Ich mag es nicht, wenn ich im Internet überall getrackt werde und deshalb gibt es auf meiner Homepage keine Cookies, kein Google-Analytics und auch keine Server-Logs. Weiterhin werden keine Scripte von anderen Domains geladen. Alle Scripte, Designs, Fonts werden direkt von tomriedl.com gehostet.


## Warum Codedoc
[Codedoc](https://codedoc.cc/) erzeugt aus Markdown-Dateien HTML-Dateien. Somit kann man aus einer Sammlung von Markdown-Dokumenten eine Website erzeugen. Der Fokus von Codedoc liegt in der Erstellung von Dokumentationen für Software bzw. für Quelltexte.

Codedoc ist ein statischer Webseitengenerator; davon gibt es [unglaublich viele](https://jamstack.org/generators/). Für Codedoc habe ich mich aus folgenden Gründen entscheiden:
- Basisdateien sind [Markdown (.md)](https://de.wikipedia.org/wiki/Markdown).
- Verwendet [TypeScript](https://www.typescriptlang.org/) statt JavaScript als Primärsprache.
- [Erweiterbar durch Plugins](https://codedoc.cc/docs/customization/overview).
- [Gute Darstellung von Quelltext](https://codedoc.cc/docs/code/overview).
- Live-Reload - Beim Live-Reload kann man die Website lokal ausführen und bei einer Änderung in einer Markdown-Datei wird sofort die Seite in HTML übersetzt und der Browser automatisch aktualisiert.
- Funktioniert ohne JavaScript - Öffnet man die Seite mit einem JavaScript-Blocker, wie [NoScript](https://noscript.net/) oder uMatrix ([Firefox](https://addons.mozilla.org/en-US/firefox/addon/umatrix/)/[Chrome](https://chrome.google.com/webstore/detail/umatrix/ogfcmafjalglgifnmanfmnieipoejdcf)) funktioniert diese auch bei geblockter Code-Ausführung.
- Dark/Light-Theme-Unterstützung.
- Verwendet keine Cookies.


## Static Website
Durch das Erzeugen von HTML-Dateien aus Markdown wird auf Serverseite nur ein File-Server benötigt, da kein server-side Rendering durchgeführt werden muss. Das hat folgende Vorteile:
- Die Website wird deutlich schneller
- Es gibt weniger Angriffspunkte für Hacks
- Ein Umzug zu einem anderen Hoster ist einfacher
- Man wird unabhängig von Serversoftware (PHP-Versionen, etc.)
- Progressive Web-Apps sind mit Static Websites leichter umzusetzen


## Warum Open-Source
Die Quelltexte, sowie die Deploy-Skripte sind öffentlich in folgendem Repository einsehbar:<br /><https://gitlab.com/TomRiedl/tomriedl.com/>

Den Code der Website habe ich aus folgenden Gründen öffentlich verfügbar gemacht:
- Die Website wird dadurch komplett nachverfolgbar. Jedes Release und jede Änderung bleibt für immer im Repository einsehbar. Siehe [Changelog-Seite](/changelog/).
- Die Scripte und Deploy-Prozesse sind eventuell für andere Personen/Firmen interessant.
- Andere Personen können Fehler korrigieren oder selbst eigene Artikel schreiben.

Wenn dir also die Struktur und die Technik der Website gefällt, kannst du einfach das Repository kopieren und auf dieser Basis eine eigene Website aufsetzen, ohne großen Initialaufwand.

## Deploy

**Mirroring**<br />
Das Repository [gitlab.com/TomRiedl/tomriedl.com](https://gitlab.com/TomRiedl/tomriedl.com) wird über die [GitLab-Bordmittel](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html) automatisch zu einer GitLab-Instanz unserer Firma gepusht. <br />
![GitLab-Mirror auf Firmeninstanz](gitlab-mirror.png)<br />
Der Grund hierfür ist, dass ich die SFTP-Zugangsdaten für den Webserver (das spätere Deploy-Ziel) nicht direkt auf gitlab.com speichern möchte. Die Zugangsdaten sind als [CI-Variablen gespeichert](https://docs.gitlab.com/ee/ci/variables/).

**Build-Pipeline**<br />
Mit jeder Änderung des Quelltextes wird eine [Build-Pipeline angestoßen](https://gitlab.com/TomRiedl/tomriedl.com/-/pipelines). Die Pipeline selbst durchläuft folgende Stages:<br />
![GitLab Deploy Stages](gitlab-deploy-stages.png)<br />
- [Preparation - Meta](https://gitlab.com/TomRiedl/tomriedl.com/-/blob/release/Pipeline/Preparation.yml)<br />Hier werden aus dem Repository [Meta-Informationen ausgelesen](https://gitlab.com/TomRiedl/tomriedl.com/-/blob/release/Pipeline/Meta.sh), diese werden primär für die Versionierung verwendet.
- [Build](https://gitlab.com/TomRiedl/tomriedl.com/-/blob/release/Pipeline/Build.yml)<br />Das Erzeugen der HTML-Dateien aus den Markdown-Dateien. Weiterhin werden in diesem Schritt die [sitemap.xml](https://gitlab.com/TomRiedl/tomriedl.com/-/blob/release/Pipeline/CreateSitemap.sh) und die [Tag-Pages](https://gitlab.com/TomRiedl/tomriedl.com/-/blob/release/Pipeline/CreateTagPages.sh) erzeugt.
- [Test](https://gitlab.com/TomRiedl/tomriedl.com/-/blob/release/Pipeline/Test.yml)<br />Beim Test wird ein [lokaler Webserver gestartet](https://www.npmjs.com/package/http-server), und die Website auf [ungültige Links überprüft](https://www.npmjs.com/package/broken-link-checker).
- [Deploy](https://gitlab.com/TomRiedl/tomriedl.com/-/blob/release/Pipeline/Deploy.yml)<br />Dieser Job lädt die fertige und geprüfte Website auf den Webserver hoch. Der Job wird nur auf der Firmen-Instanz ausgeführt (siehe Mirroring oben).

**Versionierung**<br />
Nur getaggte Versionen des `release`-Branches werden auf tomriedl.com deployed und vor dem Upload mit einer [semantischen Versionierung](https://semver.org/lang/de/) versehen. Die aktuelle Version kannst du im Hauptmenü sehen:
![Version der Website](website-version.png)<br />
Die [Versionshistorie](https://gitlab.com/TomRiedl/tomriedl.com/-/releases) findest du [bei GitLab](https://gitlab.com/TomRiedl/tomriedl.com/-/releases).

## Online-Editor
Für gewöhnlich editiere ich die Seite über [Visual Studio Code](https://code.visualstudio.com/) und verwalte das Repository über [GitKraken](https://www.gitkraken.com/invite/8dEAeKHi). GitLab verfügt jedoch über eine mehr als ausreichende [Web-IDE](https://docs.gitlab.com/ee/user/project/web_ide/) über die es möglich ist, die gesamte Website über den Browser zu bearbeiten.<br />
Somit kann ich auch schnell unterwegs oder auf meinem Handy einen Artikel editieren.<br />
![GitLab Web-IDE](gitlab-web-ide.png)

## Website lokal öffnen
Zum lokalen Entwickeln kann über Codedoc ein Web-Server mit Live-Reload gestartet werden. Damit man Codedoc und die nötigen Abhängigkeiten nicht auf seinem Rechner installieren muss, habe ich hierfür einen Docker-Image erstellt: <https://hub.docker.com/repository/docker/cyancor/codedoc> ([Quelltext](https://gitlab.com/cyancor/docker-codedoc/-/blob/production/Dockerfile)). Zum Starten reicht es, das Script [serve.cmd](https://gitlab.com/TomRiedl/tomriedl.com/-/blob/release/serve.cmd) auszuführen. Das Script ist so geschrieben, dass es auf Linux, Windows und MacOS lauffähig ist.


## Progressive Web-App
Die Website als Progressive Web-App ist noch nicht umgesetzt. Sobald ich diese Seite zu einer PWA umbaue, schreibe ich dazu einen eigenen Artikel mit Anleitung.


## Mitgestaltung und Feedback
Zur Mitgestaltung der Website hast du folgende Möglichkeiten:
- **Ticket erstellen**: Alle offenen Tickets siehst du hier: <https://gitlab.com/TomRiedl/tomriedl.com/-/issues><br />Solltest du einen Fehler gefunden haben oder dir einen Artikel zu einem bestimmten Thema wünschen, kannst du jederzeit ein [neues Ticket erstellen](https://gitlab.com/TomRiedl/tomriedl.com/-/issues/new) (hierfür benötigst du einen gitlab.com-Account).
- **Quelltext editieren**: Du kannst jederzeit Inhalte korrigieren und sogar eigene Artikel als Gastbeitrag erstellen. Nach einem Review der Änderungen können diese dann in den Release-Branch gemerged werden. Hierzu gehst du folgendermaßen vor:<br />1. Einen Fork des Repositories erstellen: <https://gitlab.com/TomRiedl/tomriedl.com/-/forks/new><br />2. Die Änderung in deinem Repository durchführen<br />3. Einen Merge-Request an mich stellen

```##Tags##: homepage website codedoc hosting pwa progressive-web-app gitlab```
