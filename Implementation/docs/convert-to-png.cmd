echo off

goto(){
    ScriptPath="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
    RepositoryPath="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." >/dev/null 2>&1 && pwd)"
    DockerImage=$(cat "$RepositoryPath/Pipeline/Environment.yml" | grep BuildDocker | grep -o -P '(?<=\").*(?=\")')

    dockerNeedsSudo=$(docker ps > /dev/null 2>&1; echo $?)
    if [ "${dockerNeedsSudo}" == "0" ]; then
      sudoCmd="";
    else
      sudoCmd="sudo";
      sudo echo;
    fi

    $sudoCmd docker run -it \
        -w /wd \
        -v "$ScriptPath:/wd" \
        "$DockerImage" \
        /bin/bash -c "/wd/convert-to-png.sh $@"
}

goto "$@"
exit

:(){
    set ScriptPath=%~dp0
    set RepositoryPath=%~dp0\..\..
    for /F "Tokens=*" %%I in ('findstr "BuildDocker: " %RepositoryPath%\Pipeline\Environment.yml') Do Set DockerFile=%%I
    for /F delims^=^"^ tokens^=2 %%a in ("%DockerFile%") do Set DockerImage=%%a

    docker run -it ^
        -w /wd ^
        -v "%ScriptPath%:/wd" ^
        "%DockerImage%" ^
        /bin/bash -c "/wd/convert-to-png.sh %*"
