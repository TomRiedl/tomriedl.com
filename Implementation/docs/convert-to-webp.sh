 #!/bin/bash

Path=$1
if [ "$Path" == "" ]; then echo "Please specify path."; exit 1; fi

echo "Converting files in /wd/$Path..."
cd /wd/$Path
pwd

for inputFile in $(find /wd/$Path -name '*.png'); do
    echo "Converting $inputFile..."
    outputFile=${inputFile/.png/.webp}
    convert "$inputFile" -strip -define webp:lossless=true -define webp:alpha-compression=1 -define webp:method=6 "$outputFile"
    if [[ "$inputFile" != *"cover"* ]]; then
        rm "$inputFile"
    fi
done

