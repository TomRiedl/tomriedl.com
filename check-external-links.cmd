echo off

goto(){
    # Linux/Mac
    RepositoryPath="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
    DockerImage=$(cat "$RepositoryPath/Pipeline/Environment.yml" | grep BuildDocker | grep -o -P '(?<=\").*(?=\")')

    docker run -it \
        -w /wd \
        -v "$RepositoryPath:/wd" \
        "$DockerImage" \
        /bin/bash -c "./Scripts/check-links.sh --exclude-internal --get"
}

goto "$@"
exit


:(){
    rem Windows
    set RepositoryPath=%~dp0
    for /F "Tokens=*" %%I in ('findstr "BuildDocker: " %RepositoryPath%\Pipeline\Environment.yml') Do Set DockerFile=%%I
    for /F delims^=^"^ tokens^=2 %%a in ("%DockerFile%") do Set DockerImage=%%a

    docker run -it ^
        -w /wd ^
        -v "%RepositoryPath%\Implementation:/wd" ^
        "%DockerImage%" ^
        /bin/bash -c "./Scripts/check-links.sh --exclude-internal --get"
