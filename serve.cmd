echo off

goto(){
    # Linux/Mac
    RepositoryPath="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
    DockerImage=$(cat "$RepositoryPath/Pipeline/Environment.yml" | grep BuildDocker | grep -o -P '(?<=\").*(?=\")')

    dockerNeedsSudo=$(docker ps > /dev/null 2>&1; echo $?)
    if [ "${dockerNeedsSudo}" == "0" ]; then
      sudoCmd="";
    else
      sudoCmd="sudo";
      sudo echo;
    fi

    $sudoCmd docker run -it --init \
        -p 3000:3000 \
        -w /wd \
        -v "$RepositoryPath/Implementation:/wd" \
        "$DockerImage" \
        /bin/bash -c "rm -rf /wd/dist/*; codedoc serve"
}

goto "$@"
exit


:(){
    rem Windows
    set RepositoryPath=%~dp0
    for /F "Tokens=*" %%I in ('findstr "BuildDocker: " %RepositoryPath%\Pipeline\Environment.yml') Do Set DockerFile=%%I
    for /F delims^=^"^ tokens^=2 %%a in ("%DockerFile%") do Set DockerImage=%%a

    docker run -it ^
        -p 3000:3000 ^
        -w /wd ^
        -v "%RepositoryPath%\Implementation:/wd" ^
        "%DockerImage%" ^
        /bin/bash -c "rm -rf /wd/dist/*; codedoc serve"
