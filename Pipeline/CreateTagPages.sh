#!/bin/bash

echo "Creating tag pages..."

if [ -d "docs/md/tag" ]; then rm -rf docs/md/tag/*; fi

TagIntro=$(cat docs/md/_tag.md)

TagIndexFile="docs/md/tag/all-tags"

for f in $(find dist -name '*.html' ! -name '_*' ! -name '404*'); do
    echo "Reading tags from $f..."
    Tags=$(cat $f | grep "\"TagLink\"" | grep "href=\"/tag/")
    echo "$Tags" | while read Line
    do
        Tag=$(echo $Line | cut -d "\"" -f 2)
        Tag=${Tag/\/tag\//}
        if [ "$Tag" == "" ]; then break; fi

        if [ ! -d "docs/md/tag/${Tag}" ]; then
            mkdir -p "docs/md/tag/${Tag}"
            echo "$Tag" >> $TagIndexFile
        fi

        UrlToHere=${f/dist\//\/}
        UrlToHere=${UrlToHere/\/index.html/\/}

        Title=$(cat $f | grep "<title>" | grep "</title>" | cut -d "|" -f 1)
        Title=${Title/<title>/}
        Icon=$(cat $f | grep "<!-- Icon:" | cut -d ":" -f 2 | cut -d " " -f 1)
        if [ "${Icon}" == "" ]; then Icon="link"; fi

        if [ ! -f "docs/md/tag/${Tag}/index.md" ]; then
            Intro=${TagIntro/\$TAG/$Tag}
            echo "$Intro" > "docs/md/tag/${Tag}/index.md"
            printf "\n\n" >> "docs/md/tag/${Tag}/index.md"
        fi

        printf "<div class=\"tagLink\"><a href=\"%s\"><img src=\"/images/${Icon}.svg\" alt=\"${Icon}\" />&nbsp;%s</a></div>\n" "${UrlToHere}" "${Title}"  >> "docs/md/tag/${Tag}/index.md"
    done
done

sort $TagIndexFile | uniq -u > "$TagIndexFile.sorted"
TagIndex=$(cat $TagIndexFile.sorted)
cp "docs/md/_tag-index.md" "docs/md/tag/index.md"
printf "\n\n" >> "docs/md/tag/index.md"
echo "$TagIndex" | while read Tag
do
    printf "%s [${Tag}](/tag/${Tag})\n" "-" >> "docs/md/tag/index.md"
done

rm "$TagIndexFile" "$TagIndexFile.sorted"
