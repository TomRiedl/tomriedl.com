#!/bin/bash

Branch="$1"

FooterContent=$(cat docs/md/_footer.md)
for f in $(find docs -name '*.md' ! -name '_*' ! -name '404*' ! -wholename 'docs/md/changelog/*'); do
    echo "Processing $f..."
    printf "\n\n" >> $f
    Footer="${FooterContent/\$FILEPATH/$f}"
    Footer="${Footer/\$BRANCH/$Branch}"
    echo "$Footer" >> $f
    printf "\n" >> $f
    Changelog=$(git log --pretty=format:'- %ad ([%h](https://gitlab.com/TomRiedl/tomriedl.com/-/commit/%H)): %s' --date=short -- $f | grep -v 'Merge branch' | grep -v 'Merge remote' | head -n 10)
    if [ "$Changelog" != "" ]; then
        printf "\n\n## Changelog\n" >> $f
        echo "$Changelog" >> $f
        echo "- [Vollständiger Changelog dieser Seite](https://gitlab.com/TomRiedl/tomriedl.com/-/commits/${Branch}/Implementation/${f})" >> $f
        echo "- [Changelog der gesamten Website](/changelog/)" >> $f
        printf "\n" >> $f
    fi
done
