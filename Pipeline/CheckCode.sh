#!/bin/bash

ScriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
RepositoryPath="$ScriptPath/.."
ImplementationPath="$RepositoryPath/Implementation"
CallSource="$1"

cd "$RepositoryPath"

if [ "$SkipLint" != "true" ]; then
    InvalidCasings="$(find . | perl -ne 's!([^/]+)$!lc $1!e; print if 1 == $seen{$_}++')"
    if [ "$InvalidCasings" != "" ]; then
        printf "\033[0;31mFound duplicate filenames with different casing:\n$InvalidCasings\n\n\033[0m";
        exit 1;
    fi

    grep -nr --color=always -P "\t|if\(|for\(|while\(|\}else|\} else|else\{|else \{|\) \{$|\)\{$| $|( )+\,|console.log" \
        --include=*.{ts,tsx,js,json,html,scss} \
        --exclude-dir="node_modules" \
        Implementation/docs/* | tr '\t' '>' | tr ' ' '_'
    if [ "${PIPESTATUS[0]}" != "1" ]; then printf "\033[0;31mFound illegal code or tab characters.\n\033[0m"; exit 1; fi;
    grep -nr --color=always -P "\t|if\(|for\(|while\(|\}else|\} else|else\{|else \{|\) \{$|\)\{$| $|( )+\," \
        --include=*.{sh,conf,json,html,bat,plist,robot,yml} \
        --exclude="CheckCode.sh" \
        --exclude-dir="dist" \
        --exclude-dir="node_modules" \
        . | tr '\t' '>' | tr ' ' '_'
    if [ "${PIPESTATUS[0]}" != "1" ]; then printf "\033[0;31mFound illegal code or tab characters.\n\033[0m"; exit 1; fi;
    grep -nr --color=always -P "\t" \
        --include=*.{yml} \
        --exclude-dir="dist" \
        --exclude-dir="node_modules" \
        . | tr '\t' '>' | tr ' ' '_'
    if [ "${PIPESTATUS[0]}" != "1" ]; then printf "\033[0;31mFound illegal code or tab characters.\n\033[0m"; exit 1; fi;
fi

cd "$ImplementationPath"


