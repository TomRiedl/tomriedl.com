#!/bin/bash

SitemapFile="docs/root/sitemap.xml"

printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n" > $SitemapFile

# TODO: Implement a better way to exclude certain pages
for f in $(find docs -name '*.md' ! -name '_*' ! -name '404*' ! -wholename 'docs/md/tag/*' ! -wholename 'docs/md/version1/iota-wallet-wiederherstellen-iota-recovery-tool-reclaim/*'); do
    echo "Adding $f to sitemap..."
    LastModified="$(git log -1 --pretty="format:%cs" $f)"
    Uri="${f/\/index.md/\/}"
    Uri="${Uri/.md/.html}"
    Uri="${Uri/\/version1\//\/}"
    Uri="${Uri/docs\/md\//}"
    if [ "$LastModified" != "" ]; then
        printf "<url><loc>https://tomriedl.com/${Uri}</loc><lastmod>${LastModified}</lastmod></url>\n" >> $SitemapFile
    else
        printf "<url><loc>https://tomriedl.com/${Uri}</loc></url>\n" >> $SitemapFile
    fi
done

printf "</urlset>\n" >> $SitemapFile
