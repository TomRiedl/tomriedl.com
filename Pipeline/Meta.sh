#!/bin/bash

echo "Reading meta information..."

if [ "$ReleaseBranchName" == "" ]; then
    # For projects with "master" as the production branch set the CI variable "ReleaseBranchName" to "master"
    ReleaseBranchName="release"
fi
if [ "$ReleaseCandidateBranchName" == "" ]; then
    ReleaseCandidateBranchName="rc"
fi
if [ "$DevelopBranchName" == "" ]; then
    DevelopBranchName="develop"
fi

RemoteName=$(git remote)
echo "Remote: $RemoteName"
echo "Release branch name: $ReleaseBranchName"
echo "RC branch name:      $ReleaseCandidateBranchName"
echo "Develop branch name: $DevelopBranchName"

git config --global --add safe.directory '*'

# Update repository to contain all necessary information
git fetch "$RemoteName" "$ReleaseBranchName" --tags

BuildTime=$(date --utc --iso-8601=seconds)
BuildTime=$(echo ${BuildTime/+0000/Z})
BuildTime=$(echo ${BuildTime/+00:00/Z})
BuildDate=$(date --utc --iso-8601)
BuildDateNumbers=$(echo ${BuildDate//-/})
Branch=$CI_COMMIT_REF_NAME
BranchMasked=${Branch//\//\\\/}
BranchNoSlash=$(echo "$Branch" | tr / -)
BranchNoSlashLower=$(echo "$BranchNoSlash" | tr '[:upper:]' '[:lower:]')
Tag=$(git tag --contains $CI_COMMIT_SHA)
LastTag=$(git describe --tags --abbrev=0 --match '[0-9]*\.[0-9]*\.[0-9]*' $(git merge-base $RemoteName/$ReleaseBranchName HEAD))
CommitHash=$CI_COMMIT_SHA
CommitShortHash=$CI_COMMIT_SHORT_SHA
BuildNumber=$CI_PIPELINE_IID

if [[ "$Branch" == "$ReleaseBranchName/"* ]]; then
    echo "Detected $Branch branch."
    echo "The release branch $ReleaseBranchName cannot be used as folder. \"$ReleaseBranchName/*\" is not allowed."
    exit 1
fi

if [[ $Branch == "$ReleaseBranchName" && $Tag == "" ]]; then
    echo "Detected $Branch branch."
    echo "Tried to build the release branch without a tag."
    exit 1
fi

if [[ $Branch == "$DevelopBranchName" ]]; then
    HashRelease=$(git log -n 1 --pretty='format:%C(auto)%H' $RemoteName/$ReleaseBranchName)
    HashDevelop=$(git log -n 1 --pretty='format:%C(auto)%H' $RemoteName/$DevelopBranchName)
    HashCommon=$(git merge-base $HashRelease $HashDevelop)
    if [ "$HashCommon" != "$HashRelease" ]; then
        echo "The latest release has not been merged into $DevelopBranchName."
        exit 1
    fi
fi

if [[ "$Branch" == "$ReleaseBranchName" && "$Tag" =~ ^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$ ]]; then
    echo "Detected $Branch branch."
    Category="release"
    CategoryUpper="Release"
    DockerTag="$Tag"
    DockerTagLatest="latest"
    Domain="$DomainProduction"
    DomainNoHttps="$DomainProductionNoHttps"
    Version="$Tag"
    VersionNumeric="$Tag"
    VersionPreRelease=""
    DeployPath="$ReleaseBranchName/$Version"
    DeployUrl="$DeployUrlProduction"
    DeployPathWithoutPrefix=$(echo "$DeployPath" | awk -F "$ReleaseBranchName/" '{print $2}')
fi

if [[ "$Branch" == "$ReleaseCandidateBranchName/"* ]]; then
    echo "Detected release-candidate branch."
    Category="rc"
    CategoryUpper="Rc"
    IFS='/' read -ra Version <<< "$Branch"
    VersionPreRelease="rc+$BuildNumber.$CommitShortHash.$BuildDateNumbers"
    Version="${Version[1]}-$VersionPreRelease"
    VersionNumeric="${Version[1]}"

    DockerTag="rc-$Version-$BuildNumber"
    DockerTagLatest="rc-$Version"
    Domain="$DomainRc"
    DeployPath="release-candidate/$VersionNumeric"
    DeployUrl="$DeployUrlRc"
    DeployPathWithoutPrefix=$(echo "$DeployPath" | awk -F "release-candidate/" '{print $2}')
fi

if [[ "$Branch" == "$DevelopBranchName" ]]; then
    echo "Detected $DevelopBranchName branch."
    Category="develop"
    CategoryUpper="Develop"
    DockerTag="$DevelopBranchName-$BuildNumber"
    DockerTagLatest="$DevelopBranchName"
    Domain="$DomainDevelop"
    VersionPreRelease="$DevelopBranchName+$BuildNumber.$CommitShortHash.$BuildDateNumbers"
    Version="$LastTag-$VersionPreRelease"
    VersionNumeric="$LastTag"
    DeployPath="$DevelopBranchName"
    DeployUrl="$DeployUrlDevelop"
    DeployPathWithoutPrefix="$DeployPath"
fi

if [[ "$Category" == "" ]]; then
    echo "No specific branch found, selecting feature."
    Category="feature"
    CategoryUpper="Feature"
    BranchCleaned=$(echo "$Branch" | sed -r 's/[\/]+/-/g')
    DockerTag="$BranchCleaned-$BuildNumber"
    DockerTagLatest="$BranchCleaned-latest"
    DockerTagFeatureLatest="feature-latest"
    Domain="$DomainFeature"
    VersionPreRelease="$BuildNumber.$BranchNoSlashLower"
    Version="$LastTag-$VersionPreRelease"
    VersionNumeric="$LastTag"
    DeployPath="$Branch"
    DeployUrl="$DeployUrlFeature"
    DeployPathWithoutPrefix=$(echo "$DeployPath" | awk -F "feature/" '{print $2}')
fi


IFS='.' read -ra VersionParts <<< "$VersionNumeric"
VersionMajor="${VersionParts[0]}"
VersionMinor="${VersionParts[1]}"
VersionPatch="${VersionParts[2]}"

Version=${Version//#/}
VersionPreRelease=${VersionPreRelease//#/}
DeployPath=${DeployPath//#/}
DeployPathWithoutPrefix=${DeployPathWithoutPrefix//#/}
DockerTag=${DockerTag//#/}
DockerTagLatest=${DockerTagLatest//#/}
DockerTagFeatureLatest=${DockerTagFeatureLatest//#/}

mkdir -p Meta

echo "$BuildTime" > "Meta/BuildTime"
echo "$BuildDate" > "Meta/BuildDate"
echo "$BuildDateNumbers" > "Meta/BuildDateNumbers"
echo "$Domain" > "Meta/Domain"
echo "$DomainNoHttps" > "Meta/DomainNoHttps"
echo "$Category" > "Meta/Category"
echo "$CategoryUpper" > "Meta/CategoryUpper"
echo "$Version" > "Meta/Version"
echo "$VersionNumeric" > "Meta/VersionNumeric"
echo "$VersionMajor" > "Meta/VersionMajor"
echo "$VersionMinor" > "Meta/VersionMinor"
echo "$VersionPatch" > "Meta/VersionPatch"
echo "$VersionPreRelease" > "Meta/VersionPreRelease"
echo "$Branch" > "Meta/Branch"
echo "$BranchMasked" > "Meta/BranchMasked"
echo "$BranchNoSlash" > "Meta/BranchNoSlash"
echo "$DeployPath" > "Meta/DeployPath"
echo "$Tag" > "Meta/Tag"
echo "$CommitHash" > "Meta/CommitHash"
echo "$CommitShortHash" > "Meta/CommitShortHash"
echo "$Hash" > "Meta/Hash"
echo "$BuildNumber" > "Meta/BuildNumber"
echo "$DockerTag" > "Meta/DockerTag"
echo "$DockerTagLatest" > "Meta/DockerTagLatest"
echo "$DockerTagFeatureLatest" > "Meta/DockerTagFeatureLatest"
echo "$DeployUrl" > "Meta/DeployUrl"
echo "$DeployPathWithoutPrefix" > "Meta/DeployPathWithoutPrefix"


echo "BuildTime:               $BuildTime"
echo "BuildDate:               $BuildDate"
echo "BuildDateNumbers:        $BuildDateNumbers"
echo "Tag:                     $Tag"
echo "CommitHash:              $CommitHash"
echo "CommitShortHash:         $CommitShortHash"
echo "BuildNumber:             $BuildNumber"
echo "Version:                 $Version"
echo "VersionNumeric:          $VersionNumeric"
echo "VersionMajor:            $VersionMajor"
echo "VersionMinor:            $VersionMinor"
echo "VersionPatch:            $VersionPatch"
echo "VersionPreRelease:       $VersionPreRelease"
echo "Branch:                  $Branch"
echo "BranchMasked:            $BranchMasked"
echo "BranchNoSlash:           $BranchNoSlash"
echo "DeployPath:              $DeployPath"
echo "Category:                $Category"
echo "CategoryUpper:           $CategoryUpper"
echo "Domain:                  $Domain"
echo "DomainNoHttps:           $DomainNoHttps"
echo "DockerTag:               $DockerTag"
echo "DockerTagLatest:         $DockerTagLatest"
echo "DockerTagFeatureLatest:  $DockerTagFeatureLatest"
echo "DeployUrl:               $DeployUrl"
echo "DeployPathWithoutPrefix: $DeployPathWithoutPrefix"

